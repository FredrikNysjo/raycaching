/// @file    02_cube.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgTypes.h"
#include "cgLogging.h"
#include "cgUtils.h"
#include "cgGL.h"
#include "cgRenderer.h"
#include "cgMesh.h"

#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <cstdlib>
#include <iostream>

// Struct for application context
struct Context {
    int width;
    int height;
    float aspect;
    GLFWwindow *window;
    GLuint defaultVAO;
    GLuint cubeVAO;
    GLuint cubeVBO;
    GLuint cubeIBO;
    GLuint cubeProgram;
    cg::CgDrawable cubeDrawable;
    float elapsedTime;
};

// Return the absolute path to the shader directory
std::string shaderDir(void)
{
    std::string rootDir = cg::cgGetEnvSafe("CG_ENGINE_ROOT");
    if (rootDir.empty()) {
        rootDir = "..";
    }
    return rootDir + "/examples/02_cube/src/shaders/";
}

void createCube(GLuint *vao, GLuint *vbo, GLuint *ibo)
{
    glm::vec3 positions[] = {
        glm::vec3(0.5f, -0.5f, -0.5f),
        glm::vec3(0.5f, -0.5f, 0.5f),
        glm::vec3(-0.5f, -0.5f, 0.5f),
        glm::vec3(-0.5f, -0.5f, -0.5f),
        glm::vec3(0.5f, 0.5f, -0.5f),
        glm::vec3(0.5f, 0.5f, 0.5f),
        glm::vec3(-0.5f, 0.5f, 0.5f),
        glm::vec3(-0.5f, 0.5f, -0.5f),
    };

    std::uint16_t indices[] = {
        0, 1, 3, 4, 7, 5,
        0, 4, 1, 1, 5, 2,
        2, 6, 3, 4, 0, 7,
        1, 2, 3, 7, 6, 5,
        4, 5, 1, 5, 6, 2,
        6, 7, 3, 0, 3, 7,
    };

    // Save GL state
    GLint prevVertexArrayBinding;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVertexArrayBinding);

    // Create VAO
    glGenVertexArrays(1, vao);
    glBindVertexArray(*vao);

    // Create GL buffer for positions
    glGenBuffers(1, vbo);
    glBindBuffer(GL_ARRAY_BUFFER, *vbo);
    size_t positionsNBytes = sizeof(positions);
    glBufferData(GL_ARRAY_BUFFER, positionsNBytes, &positions[0], GL_STATIC_DRAW);

    // Create GL buffer for indices
    glGenBuffers(1, ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *ibo);
    size_t indicesNBytes = sizeof(indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesNBytes, &indices[0], GL_STATIC_DRAW);

    // Set vertex format
    glEnableVertexAttribArray(cg::CG_POSITION);
    glVertexAttribPointer(cg::CG_POSITION, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    // Restore GL state
    glBindVertexArray(prevVertexArrayBinding);
}

void init(Context &ctx)
{
    // Initialize default GL pipeline state setters
    cg::cgInitRenderStateSetters();

    // Load cube shader program
    ctx.cubeProgram = cg::cgLoadShaderProgram(shaderDir() + "cube.vert",
                                              shaderDir() + "cube.frag");

    // Create cube (GL buffers and VAO)
    createCube(&ctx.cubeVAO, &ctx.cubeVBO, &ctx.cubeIBO);

    // Create cube drawable
    ctx.cubeDrawable.program = ctx.cubeProgram;
    ctx.cubeDrawable.vao = ctx.cubeVAO;
    ctx.cubeDrawable.drawInfo.indexCount = 36;
    ctx.cubeDrawable.drawInfo.indexType = GL_UNSIGNED_SHORT;
    ctx.cubeDrawable.drawInfo.primitiveType = GL_TRIANGLES;
}

void update(Context &ctx)
{
    // Animate cube rotation
    ctx.cubeDrawable.modelMatrix = (
        glm::rotate(glm::mat4(), ctx.elapsedTime, glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::rotate(glm::mat4(), ctx.elapsedTime, glm::vec3(1.0f, 0.0f, 0.0f)));
}

void render(Context &ctx)
{
    // Create pipeline state for cube drawable
    cg::CgRenderStateInfo pipelineStateInfo;
    pipelineStateInfo[GL_VIEWPORT] = glm::ivec4(0, 0, ctx.width, ctx.height);
    pipelineStateInfo[GL_CULL_FACE] = GL_TRUE;

    // Clear buffers
    const float clearColor[] = { 0.2f, 0.2f, 0.2f, 0.0f };
    glClearBufferfv(GL_COLOR, 0, &clearColor[0]);
    glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

    // Render cube
    cg::cgRendererDraw(cg::CgCamera(), &ctx.cubeDrawable, 1, ctx.elapsedTime, &pipelineStateInfo);
}

void errorCallback(int error, const char* description)
{
    std::cerr << description << std::endl;
}

void resizeCallback(GLFWwindow* window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->width = width;
    ctx->height = height;
    ctx->aspect = float(width) / float(height);
}

int main(void)
{
    // Set logging level
    cg::cgSetLoggingLevel(cg::CG_INFO);

    // Create application context
    Context ctx;

    // Create GLFW window and GL context
    glfwSetErrorCallback(errorCallback);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    ctx.width = 512;
    ctx.height = 512;
    ctx.aspect = float(ctx.width) / float(ctx.height);
    ctx.window = glfwCreateWindow(ctx.width, ctx.height, "example 02 - cube", nullptr, nullptr);
    if (!ctx.window) {
        std::exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(ctx.window);
    glfwSetWindowUserPointer(ctx.window, &ctx);
    glfwSetFramebufferSizeCallback(ctx.window, resizeCallback);

    // Load GL API functions
    if (cg::cgLoadGL() != cg::CgStatus::Success) {
        std::exit(EXIT_FAILURE);
    }
    cg::cgLogInfo("GL version: %s\n", glGetString(GL_VERSION));

    // Create default VAO and set initial pipeline state
    glGenVertexArrays(1, &ctx.defaultVAO);
    glBindVertexArray(ctx.defaultVAO);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    // Initialize application
    init(ctx);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window)) {
        glfwPollEvents();
        ctx.elapsedTime = glfwGetTime();
        update(ctx);
        render(ctx);
        glfwSwapBuffers(ctx.window);
    }

    // Shutdown
    glfwDestroyWindow(ctx.window);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
