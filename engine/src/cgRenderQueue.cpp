/// @file    cgRenderQueue.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgRenderQueue.h"

#include <mutex>
#include <thread>

namespace cg {

// Thread safe singleton initialization
static CgRenderQueue *s_instance = CgRenderQueue::instance();

static std::mutex s_mutex;  // Mutex for locking render queue

/// Pre-sort drawables to minimize GL driver state changes
static void _cgRenderQueuePreSortDrawables(CgRenderQueue *renderQueue)
{
    // STUB
}

CgRenderQueue *CgRenderQueue::instance()
{
    static CgRenderQueue instance;
    return &instance;
}

void cgRenderQueueSubmitDrawable(CgRenderQueue *renderQueue,
                                 const CgDrawable &drawable)
{
    assert(renderQueue);

    std::lock_guard<std::mutex> lock(s_mutex);

    // Push shallow copy of drawable onto render queue
    renderQueue->drawables.push_back(drawable);
}

void cgRenderQueueGenerateDrawLists(CgRenderQueue *renderQueue)
{
    assert(renderQueue);

    std::lock_guard<std::mutex> lock(s_mutex);

    // Clear old draw lists
    for (unsigned i = 0; i < renderQueue->drawLists.size(); ++i) {
        renderQueue->drawLists[i].clear();
    }

    // Pre-sort drawables
    _cgRenderQueuePreSortDrawables(renderQueue);
 
    // Generate new draw lists
    for (const CgDrawable &drawable : renderQueue->drawables) {
        // Push shallow copy of drawable onto draw list for enabled render pass
        for (unsigned i = 0; i < renderQueue->drawLists.size(); ++i) {
            std::uint32_t renderPassIndex = i;
            if (drawable.renderMask & (1uLL << renderPassIndex)) {
                renderQueue->drawLists[renderPassIndex].push_back(drawable);
            }
        }
    }
}

void cgRenderQueueGetDrawList(const CgRenderQueue *renderQueue,
                              const std::uint32_t renderPassIndex,
                              const CgDrawable **drawables,
                              size_t *drawablesCount)
{
    assert(renderQueue);
    assert(renderPassIndex < renderQueue->drawLists.size());
    assert(drawables);
    assert(drawablesCount);

    if ((renderPassIndex < renderQueue->drawLists.size() &&
         renderQueue->drawLists[renderPassIndex].size())) {
        // Return draw list for render pass index
        const auto &drawList = renderQueue->drawLists[renderPassIndex];
        *drawables = (const CgDrawable *)&drawList[0];
        *drawablesCount = drawList.size();
    }
    else {
        // Indicate empty draw list or invalid render pass index
        *drawables = nullptr;
        *drawablesCount = 0;
    }
}

void cgRenderQueueClear(CgRenderQueue *renderQueue)
{
    assert(renderQueue);

    std::lock_guard<std::mutex> lock(s_mutex);
    
    // Clear drawables
    renderQueue->drawables.clear();
}

} // namespace cg
