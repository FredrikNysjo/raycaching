#version 450
#extension GL_ARB_gpu_shader_int64 : enable
#extension GL_NV_shader_atomic_int64 : enable

#define RAYCACHE_MODE_RAYCAST 0
#define RAYCACHE_MODE_UPDATE 1
#define RAYCACHE_MODE_CLEAR 2
#define RAYCACHE_MODE_INSERT 3
#define RAYCACHE_MODE_PROCESS 4

#define BRICK_SIZE 5
#define TILE_SIZE_X 16
#define TILE_SIZE_Y 16
#define NUM_TILES (TILE_SIZE_X * TILE_SIZE_Y)
#define NUM_SHADOW_STEPS 100
#define NUM_AO_STEPS 20
#define MAX_ACCUM_FRAMES 8

//#define USE_UINT64_CLIPMAP_VOXELS  // Requires support for 64-bits atomics
#ifdef USE_UINT64_CLIPMAP_VOXELS
#define CLIPMAP_VALUE_TYPE uint64_t
#else // USE_UINT64_CLIPMAP_VOXELS
#define CLIPMAP_VALUE_TYPE uint
#endif // USE_UINT64_CLIPMAP_VOXELS

layout(binding = 4, std140) uniform RaycastUniforms {
    mat4 projection[2];
    mat4 view[2];
    mat4 view_inv[2];
    int num_occlusion_rays;
    int num_shadow_rays;
    int max_steps;
    int raycasting_enabled;
    int num_importance_rays;
    float step_length;
    int use_normal_refinement;
    int num_rays_per_tile;
    int frame;
} u_raycast;

layout(binding = 5, std140) uniform VolumeUniforms {
    mat4 world_from_image;
    mat4 image_from_world;
    vec4 spacing;
    vec4 clip[2];
    float threshold;
} u_volume;

layout(binding = 6, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(location = 1) uniform int u_mode;
layout(location = 2) uniform int u_importance_samples = 0;

layout(binding = 0) uniform sampler3D u_volume_texture;
layout(binding = 2) uniform samplerBuffer u_halton;

layout(std430, binding = 2) restrict buffer ResultBuffer {
    int count;
    int clear_count;
    int pad[2];
    uvec4 data[];
} u_result_buffer;

layout(std430, binding = 3) restrict buffer ClipmapBuffer {
    CLIPMAP_VALUE_TYPE data[];
} u_clipmap_buffer;

layout(std430, binding = 4) restrict buffer BrickCache {
    int start;
    int count;
    int capacity;
    int pad;
    ivec4 cmd[2];  // DrawArraysInstancedCommand * 2
    uvec4 data[];
} u_bricks;

layout(std430, binding = 5) restrict buffer TimestampBuffer {
    uint data[];
} u_timestamp_buffer;

layout(std430, binding = 6) restrict buffer TileBuffer {
    int data[];
} u_tile_buffer;

uvec4 g_data[BRICK_SIZE];
float g_coverage;
vec3 g_pos;
vec3 g_normal;

float imageLinear(vec3 p)
{
    return textureLod(u_volume_texture, p, 0.0).r;
}

vec3 imageGradient(vec3 p, vec3 delta)
{
    vec3 grad = vec3(0.0);
    grad.x -= imageLinear(p - vec3(delta.x, 0.0, 0.0));
    grad.x += imageLinear(p + vec3(delta.x, 0.0, 0.0));
    grad.y -= imageLinear(p - vec3(0.0, delta.y, 0.0));
    grad.y += imageLinear(p + vec3(0.0, delta.y, 0.0));
    grad.z -= imageLinear(p - vec3(0.0, 0.0, delta.z));
    grad.z += imageLinear(p + vec3(0.0, 0.0, delta.z));
    grad *= 0.5;

    return grad;
}

vec3 imageNormal(vec3 p, vec3 spacing)
{
    vec3 delta = 1.0 / vec3(textureSize(u_volume_texture, 0));
    delta *= min(spacing.x, min(spacing.y, spacing.z)) / spacing;
    vec3 grad = imageGradient(p, delta);
    float grad_mag = max(1e-5, length(grad));

    return -(grad / grad_mag);
}

vec4 imageRayCast(vec3 ray_origin, vec3 ray_dir, float step,
                  int max_steps, float threshold, int num_refinements)
{
    vec3 p = ray_origin;
    float value = imageLinear(p);
    for (int i = 0; i < max_steps; ++i) {
        value = imageLinear(p);
        if (value >= threshold) { break; }
        p += (step * ray_dir);
    }

    if (value >= threshold) {
        vec3 q = p - (step * ray_dir);
        for (int i = 0; i < num_refinements; ++i) {
            vec3 midpoint = (q + p) * 0.5;
            value = imageLinear(midpoint);
            if (value >= threshold) { p = midpoint; }
            if (value < threshold) { q = midpoint; }
        }
        value = imageLinear(p);
    }

    return vec4(p, value);
}

float imageRayCastVisibility(vec3 ray_origin, vec3 ray_dir,
                             float step_length, int max_steps)
{
    float threshold = max(u_volume.threshold, imageLinear(ray_origin));
    vec4 t = imageRayCast(ray_origin, ray_dir, step_length, max_steps, threshold, 0);
    float visibility = length(t.xyz - ray_origin) / (step_length * max_steps);
    return visibility;
}

// Intersect image-space ray with 1-unit AABB centered at vec3(0.5), using
// method from Chapter 5.3 in "Real-Time Collision Detection" book
vec2 imageRayBoxIntersect(vec3 ray_origin, vec3 ray_dir)
{
    float tmin = 0.0;
    float tmax = 9999.0;
    for (int i = 0; i < 3; ++i) {
        float odd = 1.0 / (abs(ray_dir[i]) > 1e-5 ? ray_dir[i] : 1e-5);
        vec2 t = (vec2(0.0, 1.0) - ray_origin[i]) * odd;
        tmin = max(tmin, (t[0] > t[1] ? t[1] : t[0]));
        tmax = min(tmax, (t[0] > t[1] ? t[0] : t[1]));
    }
    if (tmin > tmax) { tmin = tmax = -1.0; }

    return vec2(tmin, tmax);
}

vec3 reconstructViewPos(vec3 ndc_pos, mat4 projection)
{
    float a = projection[0][0];
    float b = projection[1][1];
    float c = projection[2][2];
    float d = projection[3][2];
    float e = projection[2][0];
    float f = projection[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);

    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

vec4 clipmapFromWorld(vec3 p, int bias)
{
    vec3 q = (p - u_clipmap.center.xyz) / u_clipmap.radius;
    float lod = ceil(log2(max(1.0, max(abs(q.x), max(abs(q.y), abs(q.z)))))) + bias;
    vec3 pos = u_clipmap.dimensions[0] * ((q / exp2(lod)) * 0.5 + 0.5);

    return vec4(pos, lod);
}

vec4 worldFromClipmap(vec4 p)
{
    vec3 q = ((p.xyz / u_clipmap.dimensions[0]) - 0.5) * (exp2(p.w) / 0.5);
    vec3 pos = (q * u_clipmap.radius) + u_clipmap.center.xyz;

    return vec4(pos, 1.0);
}

int clipmapVoxelIndex(vec4 clipmap_pos, ivec4 clipmap_size)
{
    ivec4 p = ivec4(clipmap_pos);
    return clipmap_size.x * (clipmap_size.y * (clipmap_size.z * p.w + p.z) + p.y) + p.x;
}

CLIPMAP_VALUE_TYPE clipmapSubvoxelBit(vec4 clipmap_pos)
{
    vec3 ndc_pos = (clipmap_pos.xyz - (0.5 * u_clipmap.dimensions[0])) / (0.5 * u_clipmap.dimensions[0]);
    float dist = max(abs(ndc_pos.x), max(abs(ndc_pos.y), abs(ndc_pos.z)));

    CLIPMAP_VALUE_TYPE mask = 0;
    int sz = u_clipmap.subres;
    vec3 subvoxel = floor(sz * fract(clipmap_pos.xyz));
    mask = CLIPMAP_VALUE_TYPE(1) << uint(int(subvoxel.z) * (sz * sz) + int(subvoxel.y) * sz + int(subvoxel.x));

    return mask;
}

float clipmapSubvoxelRadius(vec4 clipmap_pos)
{
    return (0.5 / u_clipmap.subres) * exp2(clipmap_pos.w) * u_clipmap.spacing;
}

vec4 clipmapSubvoxelWorldPos(vec3 p, int bias)
{
    vec4 clipmap_pos = clipmapFromWorld(p, bias);
    int sz = u_clipmap.subres;
    vec4 clipmap_pos_new = vec4((floor(sz * clipmap_pos.xyz) + 0.5) / sz, clipmap_pos.w);

    return worldFromClipmap(clipmap_pos_new);
}

// Clear clipmap subvoxel using atomics
void clipmapClearSubvoxel(vec4 clipmap_pos)
{
    int voxel_idx = clipmapVoxelIndex(clipmap_pos, u_clipmap.dimensions);
    CLIPMAP_VALUE_TYPE subvoxel_bit = clipmapSubvoxelBit(clipmap_pos);
    atomicAnd(u_clipmap_buffer.data[voxel_idx], ~subvoxel_bit);
}

// Write and test clipmap subvoxel using atomics. Returns true if the test
// succeeds, false otherwise.
bool clipmapWriteAndTestSubvoxel(vec4 clipmap_pos)
{
     int voxel_idx = clipmapVoxelIndex(clipmap_pos, u_clipmap.dimensions);
     CLIPMAP_VALUE_TYPE subvoxel_bit = clipmapSubvoxelBit(clipmap_pos);
     CLIPMAP_VALUE_TYPE voxel_mask = atomicOr(u_clipmap_buffer.data[voxel_idx], subvoxel_bit);

     return (voxel_mask & subvoxel_bit) == CLIPMAP_VALUE_TYPE(0);
}

// Updates timestamp of clipmap voxel
void timestampUpdate(vec4 clipmap_pos, uint timestamp)
{
    ivec4 ts_pos = ivec4(clipmap_pos.xyz * 0.5, clipmap_pos.w);
    int ts_idx = 64 * (64 * (64 * ts_pos.w + ts_pos.z) + ts_pos.y) + ts_pos.x;

    u_timestamp_buffer.data[ts_idx] = timestamp;
}

// Fetches timestamp of clipmap voxel
uint timestampFetch(vec4 clipmap_pos)
{
    ivec4 ts_pos = ivec4(clipmap_pos.xyz * 0.5, clipmap_pos.w);
    int ts_idx = 64 * (64 * (64 * ts_pos.w + ts_pos.z) + ts_pos.y) + ts_pos.x;

    return u_timestamp_buffer.data[ts_idx];
}

// Convert Cartesian normal to spherical coordinate in range [0,1]
vec2 cartToSphere(vec3 c)
{
    return (vec2(atan(c.y, c.x), acos(c.z)) / 3.141592) * 0.5 + 0.5;
}

// Convert spherical coordinate in range [0,1] to Cartesian normal
vec3 sphereToCart(vec2 s)
{
    float theta = (s.x * 2.0 - 1.0) * 3.141592;
    float phi = (s.y * 2.0 - 1.0) * 3.141592;

    return vec3(cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi));
}

// Compute and update coverage, normal, and opacity for brick voxel
void updateBrickVoxel(vec3 world_pos, float subvox_radius, int sub_idx)
{
    vec3 image_pos = vec3(u_volume.image_from_world * vec4(world_pos, 1.0));
    vec3 image_radius = mat3(u_volume.image_from_world) * vec3(subvox_radius);

    vec3 sub_pos = vec3(sub_idx & 1, (sub_idx >> 1) & 1, sub_idx >> 2);
    vec3 p = image_pos + (sub_pos - 0.5) * image_radius;
    vec3 view_dir = normalize(world_pos - u_clipmap.center.xyz);

    vec4 pos = vec4(0.0);
    vec3 normal = vec3(view_dir * 1e-4);
    float extinction = 0.0;
    float opacity2 = 0.0;
    for (int z = 0; z < 2; ++z) {
        for (int y = 0; y < 2; ++y) {
            for (int x = 0; x < 2; ++x) {
                vec3 r = vec3(x, y, z) - 0.5;
                vec3 q = p + r * image_radius * 0.5;

                // Sample volume data
                float accum = 0.0;
                float taps[8];
                for (int i = 0; i < 8; ++i) {
                    vec3 corner = vec3(i & 1, (i >> 1) & 1, i >> 2) - 0.5;
                    taps[i] = imageLinear(q + corner * image_radius * 0.5);
                    accum += step(u_volume.threshold, taps[i]);
                }

                // Compute gradient from central difference
                vec3 grad = vec3(0.0);
                grad.x += (taps[1] + taps[3] + taps[5] + taps[7]);
                grad.x -= (taps[0] + taps[2] + taps[4] + taps[6]);
                grad.y += (taps[2] + taps[3] + taps[6] + taps[7]);
                grad.y -= (taps[0] + taps[1] + taps[4] + taps[5]);
                grad.z += (taps[4] + taps[5] + taps[6] + taps[7]);
                grad.z -= (taps[0] + taps[1] + taps[2] + taps[3]);
                float grad_mag = max(1e-5, length(grad));

                // Compute weight
                //float w1 = float(accum > 0.0 && accum < 8.0);
                //float w2 = float(grad_mag > 1e-5);
                //float w3 = clamp(dot(-view_dir, -(grad / grad_mag)) * 0.5 + 0.5, 0.0, 1.0);
                float w1 = 4.0 - abs(4.0 - accum);
                float w2 = float(grad_mag > 1e-5);
                float w3 = clamp(dot(-view_dir, -(grad / grad_mag)) * 0.5 + 0.5, 0.0, 1.0);

                // Accumulate attributes
                //pos += w1 * w2 * vec4(q, 1.0);
                //normal += w1 * w2 * w3 * -(grad / grad_mag);
                //extinction += -log(1.0 - 0.98 * w1 * w2 * w3);
                pos += (w1 / 4.0) * w2 * vec4(q, 1.0);
                normal += (w1 / 4.0) * w2 * w3 * -(grad / grad_mag);
                opacity2 += (accum / 8.0) * w2;
                extinction += w1 * w2;
            }
        }
    }
    pos.xyz /= pos.w;
    normal = normalize(normal);
    extinction /= 8.0;
    opacity2 /= 8.0;

    if (bool(u_raycast.use_normal_refinement)) {
        // Compare voxel ratio to see if a refined normal should be computed
        vec3 ratio = image_radius * vec3(textureSize(u_volume_texture, 0));
        if (max(ratio.x, max(ratio.y, ratio.z)) < 2.0) {
            normal = imageNormal(pos.xyz, u_volume.spacing.xyz);
        }
    }

    // Compute coverage and extinction based opacity
    float dist = dot((pos.xyz - p) / image_radius, normal);
    //float coverage = clamp(dist + 0.5, 0.05, 0.95) * step(1e-5, pos.w);
    float coverage = clamp(opacity2, 0.05, 0.95) * step(1e-5, pos.w);
    float opacity = clamp(1.0 - exp(-extinction * 2.0), 0.0, 1.0);

    // Store information for secondary rays
    g_coverage = coverage;
    g_normal = normal;
    g_pos = pos.xyz;

    // Pack and store point data
    uint point_data = packUnorm4x8(vec4(coverage, cartToSphere(normal), opacity));
    g_data[1 + (sub_idx >> 2)][sub_idx & 3] = point_data;
}

void updateShadows(int sub_idx, int accum_frame, float subvox_radius, float jitter)
{
    vec3 image_radius = mat3(u_volume.image_from_world) * vec3(subvox_radius);
    vec3 image_pos = g_pos;
    vec3 image_normal = g_normal;
    float threshold = max(u_volume.threshold, imageLinear(image_pos));

    float ao = 0.0;
    if (bool(u_raycast.num_occlusion_rays) && g_coverage > 0.0) {
        float step_length = u_raycast.step_length;

        for (int i = 0; i < 4; ++i) {
            vec3 rnd = fract(texelFetch(u_halton, i).rgb + jitter);
            vec3 ray_origin = image_pos;
            vec3 ray_dir = normalize(image_normal + normalize(rnd - 0.5));
            ray_origin += ray_dir * (1.0 + abs(jitter * 2.0 - 1.0)) * step_length;
            ao += 1.0 - imageRayCastVisibility(ray_origin, ray_dir, step_length, NUM_AO_STEPS);
        }
        ao /= 4.0;
    }

    float shadow = 0.0;
    if (bool(u_raycast.num_shadow_rays) && g_coverage > 0.0) {
        vec4 light_pos = u_volume.image_from_world * (u_raycast.view_inv[0] * vec4(1.0, 2.0, 3.0, 1.0));
        float step_length = u_raycast.step_length;

        vec3 ray_origin = image_pos;
        vec3 ray_dir = normalize(light_pos.xyz - ray_origin.xyz);
        ray_origin += ray_dir * (1.0 + abs(jitter * 2.0 - 1.0)) * step_length;
        shadow = 1.0 - imageRayCastVisibility(ray_origin, ray_dir, step_length, NUM_SHADOW_STEPS);
    }

    vec4 data = unpackUnorm4x8(g_data[3 + (sub_idx >> 2)][sub_idx & 3]);
    data.x = mix(data.x, shadow, 1.0 / (1.0 + min(3, accum_frame)));
    data.y = mix(data.y, ao, 1.0 / (1.0 + min(3, accum_frame)));
    g_data[3 + (sub_idx >> 2)][sub_idx & 3] = packUnorm4x8(data);
}

// Append current brick to end of result buffer
int emitBrick()
{
    int result_idx = atomicAdd(u_result_buffer.count, 1);
    for (int i = 0; i < BRICK_SIZE; ++i) {
        u_result_buffer.data[BRICK_SIZE * result_idx + i] = g_data[i];
    }

    return result_idx;
}

void main()
{
    // Fetch brick data
    int brick_idx = (u_bricks.start + gl_VertexID) % u_bricks.capacity;
    for (int i = 0; i < BRICK_SIZE; ++i) {
        g_data[i] = u_bricks.data[BRICK_SIZE * brick_idx + i];
        if (u_mode == RAYCACHE_MODE_RAYCAST) { g_data[i] = uvec4(0u); }
    }

    // Unpack brick data
    vec4 world_pos = vec4(uintBitsToFloat(g_data[0].xyz), 1.0);
    int lod = int(g_data[0].w & 0xf);
    int hidden = int(g_data[0].w >> 31u) & 1;
    int accum_frame = int(g_data[0].w >> 4) & 0xf;

    if (u_mode == RAYCACHE_MODE_RAYCAST) {
        if (gl_VertexID >= (u_bricks.capacity - u_bricks.count)) { return; }

        int eye_idx = gl_VertexID & 1;
        int tile_idx = gl_VertexID / u_raycast.num_rays_per_tile;
        hidden = int(((gl_VertexID + u_raycast.frame) % 5) == 0);

        // Compute UV for ray
        if (bool(u_importance_samples)) {
            int tile_idx2 = u_tile_buffer.data[3 * NUM_TILES + 1 + tile_idx];
            tile_idx = (tile_idx2 > 0) ? tile_idx2 : tile_idx;
        }
        vec2 tile = vec2(tile_idx % TILE_SIZE_X, tile_idx / TILE_SIZE_X);
        int sample_idx = atomicAdd(u_tile_buffer.data[tile_idx * 3 + eye_idx], 1);
        vec2 rnd = texelFetch(u_halton, sample_idx % 4096).rg;
        // Combine Halton with other sequence to amplify the number of values
        rnd = fract(rnd + 1.618034 * float(((sample_idx / 4096) % 1024) + eye_idx));
        vec2 uv = (tile + rnd) / vec2(TILE_SIZE_X, TILE_SIZE_Y);

        // Compute ray start and stop position
        vec3 ndc_pos = vec3(uv, 0.0) * 2.0 - 1.0;
        vec3 view_pos = reconstructViewPos(ndc_pos, u_raycast.projection[eye_idx]);
        mat4 image_from_view = u_volume.image_from_world * u_raycast.view_inv[eye_idx];
        vec3 image_pos = vec3(image_from_view * vec4(view_pos, 1.0));
        vec3 ray_dir = normalize(mat3(image_from_view) * view_pos);
        vec2 t = imageRayBoxIntersect(image_pos, ray_dir);
        vec3 ray_start = image_pos + ray_dir * t[0];
        vec3 ray_stop = image_pos + ray_dir * t[1];

        // Do raycasting
        float ray_length = length(ray_stop - ray_start);
        int num_steps = int(ray_length / u_raycast.step_length);
        float jitter = texelFetch(u_halton, gl_VertexID % 4096).b;
        ray_start -= (jitter * u_raycast.step_length) * ray_dir;
        vec4 hit = imageRayCast(ray_start, ray_dir, u_raycast.step_length,
                                num_steps, u_volume.threshold, 8);
        if (hit.w < u_volume.threshold) { return; }

        // Check that intersection is inside clipmap
        world_pos = u_volume.world_from_image * vec4(hit.xyz, 1.0);
        world_pos = clipmapSubvoxelWorldPos(world_pos.xyz, hidden);
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
        if (int(clipmap_pos.w) >= u_clipmap.dimensions.w) { return; }

        timestampUpdate(clipmap_pos, uint(u_raycast.frame));
        if (!clipmapWriteAndTestSubvoxel(clipmap_pos)) { return; }

        if (!bool(u_importance_samples) && jitter < 0.25) {
            if (atomicMax(u_tile_buffer.data[tile_idx * 3 + 2], u_raycast.frame) != u_raycast.frame) {
                int count = atomicAdd(u_tile_buffer.data[3 * NUM_TILES], 1);
                u_tile_buffer.data[3 * NUM_TILES + 1 + (count % NUM_TILES)] = tile_idx;
            }
        }

        // Write brick to result buffer
        g_data[0].xyz = floatBitsToUint(world_pos.xyz);
        g_data[0].w = uint(clipmap_pos.w) | uint(hidden << 31);
        emitBrick();
    }

    if (u_mode == RAYCACHE_MODE_UPDATE) {
        if (gl_VertexID >= u_bricks.count) { return; }
        if ((g_data[0].w & 0xff00u) == 0u) { return; }  // Empty brick (discard)

        // Snap brick to clipmap subvoxel
        world_pos = clipmapSubvoxelWorldPos(world_pos.xyz, hidden);
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);

        // Do clipmap test and timestamp check
        uint voxel_timestamp = timestampFetch(clipmap_pos);
        if ((uint(u_raycast.frame) - voxel_timestamp) > 60u) { return; }
        if (!clipmapWriteAndTestSubvoxel(clipmap_pos)) { return; }

        // Write brick to result buffer
        accum_frame = (accum_frame < (2 * MAX_ACCUM_FRAMES - 1)) ? accum_frame + 1 : MAX_ACCUM_FRAMES;
        g_data[0].xyz = floatBitsToUint(world_pos.xyz);
        g_data[0].w = uint(clipmap_pos.w) | uint(accum_frame << 4) | uint(hidden << 31);
        emitBrick();
    }

    if (u_mode == RAYCACHE_MODE_PROCESS) {
        if ((gl_VertexID >> 3) >= u_result_buffer.count) { return; }

        // Fetch data from result buffer instead of brick cache
        int brick_idx = (gl_VertexID >> 3);
        int sub_idx = (gl_VertexID & 7);
        for (int i = 0; i < BRICK_SIZE; ++i) {
            g_data[i] = u_result_buffer.data[BRICK_SIZE * brick_idx + i];
        }

        // Unpack brick data
        world_pos = vec4(uintBitsToFloat(g_data[0].xyz), 1.0);
        lod = int(g_data[0].w & 0xf);
        hidden = int(g_data[0].w >> 31u) & 1;
        accum_frame = int(g_data[0].w >> 4) & 0xf;

        // Update brick voxel
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
        float subvox_radius = clipmapSubvoxelRadius(clipmap_pos);
        vec3 sub_pos = vec3(sub_idx & 1, (sub_idx >> 1) & 1, sub_idx >> 2);
        vec3 grid_pos = floor(clipmap_pos.xyz * 2.0 * float(u_clipmap.subres)) + sub_pos;
        float jitter = fract(dot(grid_pos.xyz, vec3(sqrt(2.0), sqrt(3.0), sqrt(5.0))) + 1.61803 * float(accum_frame));
        updateBrickVoxel(world_pos.xyz, subvox_radius, sub_idx);
        updateShadows(sub_idx, accum_frame, subvox_radius, jitter);

        // Write brick voxel to result buffer
        int i = (sub_idx >> 2); int j = (sub_idx & 3);
        u_result_buffer.data[BRICK_SIZE * brick_idx + 1 + i][j] = g_data[1 + i][j];
        u_result_buffer.data[BRICK_SIZE * brick_idx + 3 + i][j] = g_data[3 + i][j];
    }

    if (u_mode == RAYCACHE_MODE_CLEAR) {
        if (gl_VertexID >= u_bricks.count) { return; }
        atomicAdd(u_result_buffer.clear_count, 1);

        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
        clipmapClearSubvoxel(clipmap_pos);

        // Save timestamp
        uint voxel_timestamp = timestampFetch(clipmap_pos);
        u_bricks.data[BRICK_SIZE * u_bricks.capacity + brick_idx][0] = voxel_timestamp;
    }

    if (u_mode == RAYCACHE_MODE_INSERT) {
        if (gl_VertexID >= u_bricks.count) { return; }

        // Update LOD
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
        int hidden_new = hidden;
        if (!bool(hidden) && (clipmap_pos.w > lod)) { return; }  // Discard if moving to higher LOD
        if (bool(hidden) && (clipmap_pos.w > lod)) { hidden_new = 0; }
        if (!bool(hidden) && (clipmap_pos.w < lod)) { hidden_new = 1; }
        bool scatter_brick = (clipmap_pos.w < lod && hidden == 0);  // Need to compare with old value before we update
        clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden_new);
        if (clipmap_pos.w >= u_clipmap.dimensions.w) { return; }

        // Restore timestamp
        uint voxel_timestamp = u_bricks.data[BRICK_SIZE * u_bricks.capacity + brick_idx][0];
        timestampUpdate(clipmap_pos, voxel_timestamp);

        if (clipmapWriteAndTestSubvoxel(clipmap_pos)) {
            // Write brick to result buffer
            g_data[0].xyz = floatBitsToUint(world_pos.xyz);
            g_data[0].w = uint(clipmap_pos.w) | uint(accum_frame << 4) | uint(hidden_new << 31);
            emitBrick();
        }

        if (scatter_brick) {
            uvec4 data_[BRICK_SIZE];
            for (int i = 0; i < BRICK_SIZE; ++i) {
                data_[i] = g_data[i];
                g_data[i] = uvec4(0u);
            }

            for (int i = 0; i < 8; ++i) {
                // Unpack sub-point data
                uint packed0 = data_[1 + (i >> 2)][i & 3];
                if ((packed0 & 0xffu) == 0u) { continue; }
                float coverage = unpackUnorm4x8(packed0).x;
                vec3 world_normal = sphereToCart(unpackUnorm4x8(packed0).yz);

                // Compute location of new brick
                vec3 displacement = (coverage - 0.5) * world_normal;
                vec3 brick_pos = vec3(i & 1, (i >> 1) & 1, i >> 2) - 0.5;
                float subvox_radius = (0.5 / u_clipmap.subres) * exp2(lod) * u_clipmap.spacing;
                vec3 world_pos_new = world_pos.xyz + (brick_pos + displacement) * subvox_radius;
                vec4 clipmap_pos_new = clipmapFromWorld(world_pos_new, 0);

                timestampUpdate(clipmap_pos_new, voxel_timestamp);
                if (!clipmapWriteAndTestSubvoxel(clipmap_pos_new)) { continue; }

                // Write brick to result buffer
                g_data[0].xyz = floatBitsToUint(world_pos_new);
                g_data[0].w = uint(clipmap_pos_new.w) | uint(accum_frame << 4);
                g_data[1][0] = 0xffu | packed0;  // Flag brick for rendering as single splat
                g_data[3] = g_data[4] = uvec4(data_[3 + (i >> 2)][i & 3]);
                emitBrick();
            }
        }
    }
}
