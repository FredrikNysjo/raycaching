/// @file    raycaching.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2017 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "halton3d.h"

#include <GL/glew.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

enum RaycacheMode {
    RAYCACHE_MODE_RAYCAST = 0,
    RAYCACHE_MODE_UPDATE = 1,
    RAYCACHE_MODE_CLEAR = 2,
    RAYCACHE_MODE_INSERT = 3,
    RAYCACHE_MODE_PROCESS = 4,
};

enum CopyMode {
    COPY_MODE_INSERT = 0,
    COPY_MODE_UPDATE = 1
};

enum SortMode {
    SORT_MODE_BIN = 0,
    SORT_MODE_PREFIX_SUM_SEQUENTIAL = 1,
    SORT_MODE_WRITE_INDEX = 2,
    SORT_MODE_CLEAR = 3,
    SORT_MODE_PREFIX_SUM_PARALLEL1 = 4,
    SORT_MODE_PREFIX_SUM_PARALLEL2 = 5,
};

enum SplatMode {
    SPLAT_MODE_SQUARE = 0,
    SPLAT_MODE_ROUND = 1,
    SPLAT_MODE_DISK = 2,
    SPLAT_MODE_RAYCAST = 3
};

struct BrickCacheInfo {
    int32_t start = 0;
    int32_t count = 0;
    int32_t capacity = 1000000;
    int32_t pad;
    struct DrawArraysInstancedCommand {
        int32_t count = 0;
        int32_t prim_count = 1;
        int32_t first = 0;
        int32_t reserved_must_be_zero = 0;
    } cmd[2];
};

struct Brick {
    glm::vec4 data[5];
};

struct SplatUniforms {
    glm::ivec2 resolution;
    float splat_size = 0.8f;
    int32_t splat_mode = SPLAT_MODE_DISK;
    int32_t show_clipmap_lod = true;
    int32_t show_normals = false;
};

struct RaycastUniforms {
    glm::mat4 projection[2];
    glm::mat4 view[2];
    glm::mat4 view_inv[2];
    int32_t num_occlusion_rays = 0;
    int32_t num_shadow_rays = 0;
    int32_t max_steps = 256;
    int32_t raycasting_enabled = true;
    int32_t importance_samples = 1;
    float step_length = 1.0f / 256.0f;
    int32_t use_normal_refinement = true;
    int32_t num_rays_per_tile = 16;
    int32_t frame = 0;
};

struct ClipmapUniforms {
    glm::mat4 world_from_clipmap;
    glm::mat4 clipmap_from_world;
    glm::vec4 center;
    glm::ivec4 dimensions = glm::ivec4(128, 128, 128, 8);
    float radius = 0.15625f;
    float spacing = 0.15625f / 64.0f;
    int32_t subres = 2;
};

struct VolumeUniforms {
    glm::mat4 world_from_image;
    glm::mat4 image_from_world;
    glm::vec4 spacing = glm::vec4(1.0f);
    glm::vec4 clip[2];
    float threshold = 0.008f;
};

void createClipmapBuffer(GLuint *buffer, GLuint *texture, const glm::ivec4 &dimensions);

void createBrickCache(GLuint *buffer, GLuint *texture, const BrickCacheInfo &buffer_info);

void createIndexBuffer(GLuint *buffer, GLuint *texture, const BrickCacheInfo &buffer_info);

void createResultBuffer(GLuint *buffer, const BrickCacheInfo &buffer_info);

void createTimestampBuffer(GLuint *buffer, const BrickCacheInfo &buffer_info);

void createTileBuffer(GLuint *buffer, const BrickCacheInfo &buffer_info);

void createHaltonTexture(GLuint *buffer, GLuint *texture);

glm::mat4 predictHeadPose(const glm::mat4 &prev, const glm::mat4 &current, float num_frames=10.0f);
