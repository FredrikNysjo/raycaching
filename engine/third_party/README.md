CG Engine Third-Party Dependencies
==================================

Overview
--------

Third-party dependencies for the CG Engine.

License
-------

cJSON, GLEW, GLFW, GLM, imgui, lodepng, and OpenVR are distributed under
their own respective licensens. See `LICENSES.md` for details.
