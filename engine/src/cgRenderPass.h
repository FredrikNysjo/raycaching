/// @file    cgRenderPass.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"
#include "cgRenderer.h"

namespace cg {

/// Default render pass indices for basic (common) top-level passes
enum CgRenderPassIndex {
    CG_DEFERRED_PASS = 0,
    CG_FORWARD_PASS = 1,
    CG_SHADOW_PASS = 2,
    CG_TRANSPARENCY_PASS = 3,
    CG_RENDER_PASS_INDEX_END = CG_TRANSPARENCY,
};

/// Render pass options
enum CgRenderPassOption {
    CG_RENDER_PASS_OPTION_END = 0,
};

/// Base struct for render pass data
struct CgRenderPassData {
    std::uint32_t type;
};

/// Struct for render passes
struct CgRenderPass {
    std::uint32_t index;  ///< 0-63 = Top-level pass; 64 or higher = Sub-pass
    CgRenderPass *next;  ///< Weak ref. to next render pass
    CgRenderPass *subPass;  ///< Weak ref. to first sub-pass
    void *preDraw;  // Function pointer
    void *postDraw;  // Function pointer
    CgRenderPassData *data;  ///< Weak ref. to render pass data

    CgRenderPass() :
        next(nullptr),
        subPass(nullptr),
        preDraw(nullptr),
        postDraw(nullptr),
        data(nullptr)
    {}
};

/// Draw top-level render pass and its sub-passes
void cgRenderPassDraw(const CgRenderQueue &renderQueue, CgRenderPass *next);

} // namespace cg
