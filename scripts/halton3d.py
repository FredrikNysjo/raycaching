import numpy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def halton(index, base):
    i = index
    b = base
    f = 1.0
    r = 0.0
    while i > 0:
        f = f / b
        r = r + f * (i % b)
        i = int(i / b)
    return r

n = 4096
x = [halton(i, 2) for i in range(1, n + 1)]
y = [halton(i, 3) for i in range(1, n + 1)]
z = [halton(i, 5) for i in range(1, n + 1)]
for v in zip(x, y, z):
    print("    %.5ff, %.5ff, %.5ff," % v)

plt.figure()
ax = plt.axes(projection='3d')
ax.plot(x[0:10], y[0:10], z[0:10], 'or')
ax.plot(x[10:100], y[10:100], z[10:100], 'ob')
ax.plot(x[100:256], y[100:256], z[100:256], 'og')
plt.axis('square')
plt.show()
