#version 450

#define BRICK_SIZE 5

struct Material {
    vec4 albedo;
    float gloss;
    float metallic;
    float wrap;
    //float pad[1];
};

struct HemisphereLight {
    vec4 up;
    vec4 ground_color;
    vec4 sky_color;
    float intensity;
    //float pad[3];
};

struct Light {
    vec4 position;
    vec4 color;
    float intensity;
    //float pad[3];
};

layout(binding = 1, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    float time;
    //float pad[3];
    HemisphereLight hemisphere;
    Light light;
} u_scene;

layout(binding = 3, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
    Material material;
} u_drawable;

layout(binding = 4, std140) uniform SplatUniforms {
    ivec2 resolution;
    float splat_size;
    int splat_mode;
    int show_clipmap_lod;
    int show_normals;
} u_splat;

layout(binding = 6, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(binding = 0) uniform usamplerBuffer u_bricks;

flat out vec4 v_color;
flat out vec3 v_disk_normal;
flat out float v_seed;

float diffuse_wrap(vec3 N, vec3 L, float wrap)
{
    return max(0.0, (dot(N, L) + wrap) / ((1.0 + wrap) * (1.0 + wrap)));
}

float specular(vec3 N, vec3 L, vec3 V, float gloss)
{
    vec3 H = normalize(L + V);
    float specular_power = exp2(10.0 * gloss);
    float normalization = (8.0 + specular_power) / 8.0;

    return normalization * pow(max(0.0, dot(N, H)), specular_power);
}

vec3 fresnel_schlick(vec3 F0, vec3 V, vec3 H)
{
    return F0 + (1.0 - F0) * pow(1.0 - clamp(dot(V, H), 0.0, 1.0), 5.0);
}

vec3 hemisphere_diffuse(vec3 N, in HemisphereLight hemisphere)
{
    float t = dot(N, hemisphere.up.xyz) * 0.5 + 0.5;
    return mix(hemisphere.ground_color.rgb, hemisphere.sky_color.rgb, t);
}

vec3 hemisphere_specular(vec3 N, vec3 V, float gloss, in HemisphereLight hemisphere)
{
    float g = min(0.975, gloss);
    float t = clamp(dot(reflect(-V, N), hemisphere.up.xyz) / (1.0 - g * g), 0.0, 1.0);

    return mix(hemisphere.ground_color.rgb, hemisphere.sky_color.rgb, t);
}

vec4 computeLighting(vec3 N, vec3 V, in Material material, float shadow, float ao)
{
    vec4 color = vec4(0.0, 0.0, 0.0, material.albedo.a);
    vec3 F0 = mix(vec3(0.05), material.albedo.rgb, material.metallic);
    vec3 Rd = mix(material.albedo.rgb, vec3(0.0), material.metallic);
    vec3 F = mix(F0, fresnel_schlick(F0, V, N), material.gloss);

    vec3 L = normalize(u_scene.light.position.xyz);
    vec3 light_intensity = u_scene.light.color.rgb * u_scene.light.intensity * shadow;
    color.rgb += (Rd * (1.0 - F0)) * diffuse_wrap(N, L, material.wrap) * light_intensity;
    color.rgb += F0 * specular(N, L, V, material.gloss) * light_intensity;

    HemisphereLight hemisphere = u_scene.hemisphere;
    //hemisphere.up.xyz = mat3(u_scene.view) * hemisphere.up.xyz;
    hemisphere.up.xyz = hemisphere.up.xyz;
    hemisphere.sky_color.rgb *= hemisphere.intensity * ao;
    hemisphere.ground_color.rgb *= hemisphere.intensity * ao;
    color.rgb += (Rd * (1.0 - clamp(F, 0.0, 1.0))) * hemisphere_diffuse(N, hemisphere);
    color.rgb += F * hemisphere_specular(N, V, material.gloss, hemisphere);

    return color;
}

// Convert normalized spherical XY to normalized Cartesian XYZ
vec3 sphereToCart(vec2 s)
{
    float theta = (s.x * 2.0 - 1.0) * 3.141592;
    float phi = (s.y * 2.0 - 1.0) * 3.141592;
    return vec3(cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi));
}

// Cosine based palette from http://www.iquilezles.org/www/articles/palettes/palettes.htm
vec3 palette(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
    return a + b * cos(6.28318 * (c * t + d));
}
#define PALETTE_RAINBOW(t) palette(t, vec3(0.5), vec3(0.5), vec3(1.0), vec3(0.0, 0.33, 0.67))

void main()
{
    // Fetch brick data
    int brick_ofs = 3 + BRICK_SIZE * (gl_VertexID >> 6);
    uvec4 brick_data = texelFetch(u_bricks, brick_ofs);
    vec4 local_pos = vec4(uintBitsToFloat(brick_data.xyz), 1.0);
    int lod = int(brick_data.w & 0xf);

    // Fetch point data
    int sub_idx = (gl_VertexID & 7);
    v_seed = float(gl_VertexID & 63) / 63.0;
    uint point_data0 = texelFetch(u_bricks, brick_ofs + 1 + (sub_idx >> 2))[sub_idx & 3];
    uint point_data1 = texelFetch(u_bricks, brick_ofs + 3 + (sub_idx >> 2))[sub_idx & 3];
    vec4 unpacked0 = unpackUnorm4x8(point_data0);
    vec4 unpacked1 = unpackUnorm4x8(point_data1);
    float coverage = unpacked0.x;
    vec3 local_normal = sphereToCart(unpacked0.yz);
    float opacity = unpacked0.w;
    float shadow = (1.0 - unpacked1.x);
    float ao = (1.0 - unpacked1.y);

    // Project point to isosurface
    float subvox_radius = (0.5 / u_clipmap.subres) * exp2(lod) * u_clipmap.spacing;
    vec3 sub_pos = vec3(sub_idx & 1, (sub_idx >> 1) & 1, sub_idx >> 2);
    vec3 displacement = ((sub_pos - 0.5) + 1.0 * (coverage - 0.5) * local_normal) * subvox_radius;
    if (coverage == 1.0) { subvox_radius *= 2.0; displacement = vec3(0.0); }  // HACK
    local_pos.xyz += displacement;

    // Compute view normal and output position
    vec3 view_pos = vec3(u_scene.view * local_pos);
    vec3 view_normal = mat3(u_scene.view * u_drawable.normal) * local_normal;
    gl_Position = u_scene.projection * vec4(view_pos, 1.0);
    if (dot(view_pos, view_normal) > 0.0) { gl_Position.z = gl_Position.w + 1.0; }

    // Compute output color from lighting
    v_color = vec4(1.0);
    if (bool(u_splat.show_normals)) { v_color.rgb = local_normal * 0.5 + 0.5; }
    if (bool(u_splat.show_clipmap_lod)) { v_color.rgb = PALETTE_RAINBOW(lod / 7.0); }
    Material material = u_drawable.material;
    material.albedo.rgba *= v_color.rgba;
    v_color = computeLighting(view_normal, -normalize(view_pos), material, shadow, ao);
    v_color.a *= opacity;

    // Compute output splat size and extra normal used for disk projection
    gl_PointSize = u_splat.splat_size * (-subvox_radius / view_pos.z) *
                   (u_scene.projection[1][1] * u_splat.resolution[1]);
    gl_PointSize *= 1.0 + (1.0 - normalize(-view_pos).z);
    gl_PointSize = max(1.0, gl_PointSize);
    float nz = dot(view_normal, -normalize(view_pos - view_normal / gl_PointSize));
    v_disk_normal = vec3(view_normal.xy, nz);
}
