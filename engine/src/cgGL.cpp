/// @file    cgGL.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgGL.h"
#include "cgLogging.h"

#include <cassert>
#include <cstring>
#include <algorithm>

namespace cg {

CgStatus cgLoadGL()
{
    glewExperimental = true;
    GLenum status = glewInit();
    if (status != GLEW_OK) {
        cgLogError("%s\n", glewGetErrorString(status));
        return CgStatus::Failure;
    }
    return CgStatus::Success;
}

// Struct for storing enum-string value pairs
struct EnumStringTuple {
    GLenum first;
    const char *second;
};

// Helper function
static const EnumStringTuple *binarySearch(EnumStringTuple *begin, EnumStringTuple *end, const char *name)
{
    static auto compare = [](const EnumStringTuple &a, const EnumStringTuple &b) -> bool
    {
        // Compare string part of pairs
        return std::strcmp(a.second, b.second) < 0;
    };

    // Perform binary search
    EnumStringTuple *found, value = { 0, name };
    if ((found = std::lower_bound(begin, end, value, compare)) != end) {
        if (std::strcmp(value.second, found->second) == 0) { return found; }
    }
    return nullptr;
}

// Tables below must be kept lexicographically sorted for the binary search to
// work. Remember to sort the lines when adding new enums! Using for example a
// macro to generate the enum-string pairs is also good idea, to avoid making
// errors...

GLenum cgGlBooleanFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_FALSE, "GL_FALSE" },
        { GL_TRUE, "GL_TRUE" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    cgLogError("Invalid enum string: %s\n", name);
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlFrontFaceFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_CCW, "GL_CCW" },
        { GL_CW, "GL_CW" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    cgLogError("Invalid enum string: %s\n", name);
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlShaderStageFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_FRAGMENT_SHADER, "GL_FRAGMENT_SHADER" },
        { GL_GEOMETRY_SHADER, "GL_GEOMETRY_SHADER" },
        { GL_VERTEX_SHADER, "GL_VERTEX_SHADER" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    cgLogError("Invalid enum string: %s\n", name);
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlPrimitiveTypeFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_LINES, "GL_LINES" },
        { GL_LINES_ADJACENCY, "GL_LINES_ADJACENCY" },
        { GL_LINE_STRIP, "GL_LINE_STRIP" },
        { GL_POINTS, "GL_POINTS" },
        { GL_TRIANGLES, "GL_TRIANGLES" },
        { GL_TRIANGLES_ADJACENCY, "GL_TRIANGLES_ADJACENCY" },
        { GL_TRIANGLE_FAN, "GL_TRIANGLE_FAN" },
        { GL_TRIANGLE_STRIP, "GL_TRIANGLE_STRIP" },
        { GL_TRIANGLE_STRIP_ADJACENCY, "GL_TRIANGLE_STRIP_ADJACENCY" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlBufferTargetFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_ARRAY_BUFFER, "GL_ARRAY_BUFFER" },
        { GL_COPY_READ_BUFFER, "GL_COPY_READ_BUFFER" },
        { GL_COPY_WRITE_BUFFER, "GL_COPY_WRITE_BUFFER" },
        { GL_ELEMENT_ARRAY_BUFFER, "GL_ELEMENT_ARRAY_BUFFER" },
        { GL_TEXTURE_BUFFER, "GL_TEXTURE_BUFFER" },
        { GL_TRANSFORM_FEEDBACK_BUFFER, "GL_TRANSFORM_FEEDBACK_BUFFER" },
        { GL_UNIFORM_BUFFER, "GL_UNIFORM_BUFFER" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlBufferUsageFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_DYNAMIC_DRAW, "GL_DYNAMIC_DRAW" },
        { GL_STATIC_DRAW, "GL_STATIC_DRAW" },
        { GL_STREAM_DRAW, "GL_STREAM_DRAW" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlFramebufferAttachmentFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_COLOR_ATTACHMENT0, "GL_COLOR_ATTACHMENT0" },
        { GL_COLOR_ATTACHMENT1, "GL_COLOR_ATTACHMENT1" },
        { GL_COLOR_ATTACHMENT2, "GL_COLOR_ATTACHMENT2" },
        { GL_COLOR_ATTACHMENT3, "GL_COLOR_ATTACHMENT3" },
        { GL_COLOR_ATTACHMENT4, "GL_COLOR_ATTACHMENT4" },
        { GL_COLOR_ATTACHMENT5, "GL_COLOR_ATTACHMENT5" },
        { GL_COLOR_ATTACHMENT6, "GL_COLOR_ATTACHMENT6" },
        { GL_COLOR_ATTACHMENT7, "GL_COLOR_ATTACHMENT7" },
        { GL_DEPTH_ATTACHMENT, "GL_DEPTH_ATTACHMENT" },
        { GL_DEPTH_STENCIL_ATTACHMENT, "GL_DEPTH_STENCIL_ATTACHMENT" },
        { GL_STENCIL_ATTACHMENT, "GL_STENCIL_ATTACHMENT" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlTextureTargetFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_TEXTURE_1D, "GL_TEXTURE_1D" },
        { GL_TEXTURE_1D_ARRAY, "GL_TEXTURE_1D_ARRAY" },
        { GL_TEXTURE_2D, "GL_TEXTURE_2D" },
        { GL_TEXTURE_2D_ARRAY, "GL_TEXTURE_2D_ARRAY" },
        { GL_TEXTURE_2D_MULTISAMPLE, "GL_TEXTURE_2D_MULTISAMPLE" },
        { GL_TEXTURE_2D_MULTISAMPLE_ARRAY, "GL_TEXTURE_2D_MULTISAMPLE_ARRAY" },
        { GL_TEXTURE_3D, "GL_TEXTURE_3D" },
        { GL_TEXTURE_BUFFER, "GL_TEXTURE_BUFFER" },
        { GL_TEXTURE_CUBE_MAP, "GL_TEXTURE_CUBE_MAP" },
        { GL_TEXTURE_RECTANGLE, "GL_TEXTURE_RECTANGLE" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlTextureParameterFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_CLAMP_TO_BORDER, "GL_CLAMP_TO_BORDER" },
        { GL_CLAMP_TO_EDGE, "GL_CLAMP_TO_EDGE" },
        { GL_LINEAR, "GL_LINEAR" },
        { GL_LINEAR_MIPMAP_LINEAR, "GL_LINEAR_MIPMAP_LINEAR" },
        { GL_LINEAR_MIPMAP_NEAREST, "GL_LINEAR_MIPMAP_NEAREST" },
        { GL_MIRRORED_REPEAT, "GL_MIRRORED_REPEAT" },
        { GL_NEAREST, "GL_NEAREST" },
        { GL_NEAREST_MIPMAP_LINEAR, "GL_NEAREST_MIPMAP_LINEAR" },
        { GL_NEAREST_MIPMAP_NEAREST, "GL_NEAREST_MIPMAP_NEAREST" },
        { GL_REPEAT, "GL_REPEAT" },
        { GL_TEXTURE_BORDER_COLOR, "GL_TEXTURE_BORDER_COLOR" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlTextureInternalFormatFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_DEPTH24_STENCIL8, "GL_DEPTH24_STENCIL8" },
        { GL_DEPTH32F_STENCIL8, "GL_DEPTH32F_STENCIL8" },
        { GL_DEPTH_COMPONENT16, "GL_DEPTH_COMPONENT16" },
        { GL_DEPTH_COMPONENT24, "GL_DEPTH_COMPONENT24" },
        { GL_DEPTH_COMPONENT32F, "GL_DEPTH_COMPONENT32F" },
        { GL_R11F_G11F_B10F, "GL_R11F_G11F_B10F" },
        { GL_R16, "GL_R16" },
        { GL_R16F, "GL_R16F" },
        { GL_R16I, "GL_R16I" },
        { GL_R16UI, "GL_R16UI" },
        { GL_R16_SNORM, "GL_R16_SNORM" },
        { GL_R32F, "GL_R32F" },
        { GL_R32I, "GL_R32I" },
        { GL_R32UI, "GL_R32UI" },
        { GL_R8, "GL_R8" },
        { GL_R8I, "GL_R8I" },
        { GL_R8UI, "GL_R8UI" },
        { GL_R8_SNORM, "GL_R8_SNORM" },
        { GL_RG16F, "GL_RG16F" },
        { GL_RG16I, "GL_RG16I" },
        { GL_RG16UI, "GL_RG16UI" },
        { GL_RG16_SNORM, "GL_RG16_SNORM" },
        { GL_RG32F, "GL_RG32F" },
        { GL_RG32I, "GL_RG32I" },
        { GL_RG32UI, "GL_RG32UI" },
        { GL_RG8, "GL_RG8" },
        { GL_RG8I, "GL_RG8I" },
        { GL_RG8UI, "GL_RG8UI" },
        { GL_RG8_SNORM, "GL_RG8_SNORM" },
        { GL_RGB10_A2, "GL_RGB10_A2" },
        { GL_RGB16F, "GL_RGB16F" },
        { GL_RGB16I, "GL_RGB16I" },
        { GL_RGB16UI, "GL_RGB16UI" },
        { GL_RGB16_SNORM, "GL_RGB16_SNORM" },
        { GL_RGB32F, "GL_RGB32F" },
        { GL_RGB32I, "GL_RGB32I" },
        { GL_RGB32UI, "GL_RGB32UI" },
        { GL_RGB8, "GL_RGB8" },
        { GL_RGB8I, "GL_RGB8I" },
        { GL_RGB8UI, "GL_RGB8UI" },
        { GL_RGB8_SNORM, "GL_RGB8_SNORM" },
        { GL_RGBA16, "GL_RGBA16" },
        { GL_RGBA16F, "GL_RGBA16F" },
        { GL_RGBA16I, "GL_RGBA16I" },
        { GL_RGBA16UI, "GL_RGBA16UI" },
        { GL_RGBA32F, "GL_RGBA32F" },
        { GL_RGBA32I, "GL_RGBA32I" },
        { GL_RGBA32UI, "GL_RGBA32UI" },
        { GL_RGBA8, "GL_RGBA8" },
        { GL_RGBA8I, "GL_RGBA8I" },
        { GL_RGBA8UI, "GL_RGBA8UI" },
        { GL_RGBA8_SNORM, "GL_RGBA8_SNORM" },
        { GL_SRGB8, "GL_SRGB8" },
        { GL_SRGB8_ALPHA8, "GL_SRGB8_ALPHA8" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlTextureFormatFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_BGR, "GL_BGR" },
        { GL_BGRA, "GL_BGRA" },
        { GL_DEPTH_COMPONENT, "GL_DEPTH_COMPONENT" },
        { GL_DEPTH_STENCIL, "GL_DEPTH_STENCIL" },
        { GL_RED, "GL_RED" },
        { GL_RG, "GL_RG" },
        { GL_RGB, "GL_RGB" },
        { GL_RGBA, "GL_RGBA" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlTextureTypeFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_BYTE, "GL_BYTE" },
        { GL_FLOAT, "GL_FLOAT" },
        { GL_HALF_FLOAT, "GL_HALF_FLOAT" },
        { GL_INT, "GL_INT" },
        { GL_SHORT, "GL_SHORT" },
        { GL_UNSIGNED_BYTE, "GL_UNSIGNED_BYTE" },
        { GL_UNSIGNED_INT, "GL_UNSIGNED_INT" },
        { GL_UNSIGNED_INT_10F_11F_11F_REV, "GL_UNSIGNED_INT_10F_11F_11F_REV" },
        { GL_UNSIGNED_INT_10_10_10_2, "GL_UNSIGNED_INT_10_10_10_2" },
        { GL_UNSIGNED_INT_24_8, "GL_UNSIGNED_INT_24_8" },
        { GL_UNSIGNED_INT_2_10_10_10_REV, "GL_UNSIGNED_INT_2_10_10_10_REV" },
        { GL_UNSIGNED_INT_8_8_8_8, "GL_UNSIGNED_INT_8_8_8_8" },
        { GL_UNSIGNED_INT_8_8_8_8_REV, "GL_UNSIGNED_INT_8_8_8_8_REV" },
        { GL_UNSIGNED_SHORT, "GL_UNSIGNED_SHORT" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlLogicOpModeFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_AND, "GL_AND" },
        { GL_AND_INVERTED, "GL_AND_INVERTED" },
        { GL_AND_REVERSE, "GL_AND_REVERSE" },
        { GL_CLEAR, "GL_CLEAR" },
        { GL_COPY, "GL_COPY" },
        { GL_COPY_INVERTED, "GL_COPY_INVERTED" },
        { GL_EQUIV, "GL_EQUIV" },
        { GL_INVERT, "GL_INVERT" },
        { GL_NAND, "GL_NAND" },
        { GL_NOOP, "GL_NOOP" },
        { GL_OR, "GL_OR" },
        { GL_OR_INVERTED, "GL_OR_INVERTED" },
        { GL_OR_REVERSE, "GL_OR_REVERSE" },
        { GL_SET, "GL_SET" },
        { GL_XOR, "GL_XOR" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlBlendModeFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_FUNC_ADD, "GL_FUNC_ADD" },
        { GL_FUNC_REVERSE_SUBTRACT, "GL_FUNC_REVERSE_SUBTRACT" },
        { GL_FUNC_SUBTRACT, "GL_FUNC_SUBTRACT" },
        { GL_MAX, "GL_MAX" },
        { GL_MIN, "GL_MIN" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlBlendFuncFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_CONSTANT_ALPHA, "GL_CONSTANT_ALPHA" },
        { GL_CONSTANT_COLOR, "GL_CONSTANT_COLOR" },
        { GL_DST_ALPHA, "GL_DST_ALPHA" },
        { GL_DST_COLOR, "GL_DST_COLOR" },
        { GL_ONE, "GL_ONE" },
        { GL_ONE_MINUS_CONSTANT_ALPHA, "GL_ONE_MINUS_CONSTANT_ALPHA" },
        { GL_ONE_MINUS_CONSTANT_COLOR, "GL_ONE_MINUS_CONSTANT_COLOR" },
        { GL_ONE_MINUS_DST_ALPHA, "GL_ONE_MINUS_DST_ALPHA" },
        { GL_ONE_MINUS_DST_COLOR, "GL_ONE_MINUS_DST_COLOR" },
        { GL_ONE_MINUS_SRC_ALPHA, "GL_ONE_MINUS_SRC_ALPHA" },
        { GL_ONE_MINUS_SRC_COLOR, "GL_ONE_MINUS_SRC_COLOR" },
        { GL_SRC_ALPHA, "GL_SRC_ALPHA" },
        { GL_SRC_COLOR, "GL_SRC_COLOR" },
        { GL_ZERO, "GL_ZERO" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}

GLenum cgGlStateFromString(const char *name)
{
    static EnumStringTuple table[] = {
        { GL_BLEND, "GL_BLEND" },
        { GL_BLEND_COLOR, "GL_BLEND_COLOR" },
        { GL_BLEND_DST_ALPHA, "GL_BLEND_DST_ALPHA" },
        { GL_BLEND_DST_RGB, "GL_BLEND_DST_RGB" },
        { GL_BLEND_EQUATION_ALPHA, "GL_BLEND_EQUATION_ALPHA" },
        { GL_BLEND_EQUATION_RGB, "GL_BLEND_EQUATION_RGB" },
        { GL_BLEND_SRC_ALPHA, "GL_BLEND_SRC_ALPHA" },
        { GL_BLEND_SRC_RGB, "GL_BLEND_SRC_RGB" },
        { GL_COLOR_WRITEMASK, "GL_COLOR_WRITEMASK" },
        { GL_CULL_FACE, "GL_CULL_FACE" },
        { GL_CULL_FACE_MODE, "GL_CULL_FACE_MODE" },
        { GL_DEPTH_CLAMP, "GL_DEPTH_CLAMP" },
        { GL_DEPTH_FUNC, "GL_DEPTH_FUNC" },
        { GL_DEPTH_RANGE, "GL_DEPTH_RANGE" },
        { GL_DEPTH_TEST, "GL_DEPTH_TEST" },
        { GL_DEPTH_WRITEMASK, "GL_DEPTH_WRITEMASK" },
        { GL_FRONT_FACE, "GL_FRONT_FACE" },
        { GL_LINE_WIDTH, "GL_LINE_WIDTH" },
        { GL_MIN_SAMPLE_SHADING_VALUE, "GL_MIN_SAMPLE_SHADING_VALUE" },
        { GL_MULTISAMPLE, "GL_MULTISAMPLE" },
        { GL_POLYGON_MODE, "GL_POLYGON_MODE" },
        { GL_POLYGON_OFFSET_FACTOR, "GL_POLYGON_OFFSET_FACTOR" },
        { GL_POLYGON_OFFSET_FILL, "GL_POLYGON_OFFSET_FILL" },
        { GL_POLYGON_OFFSET_LINE, "GL_POLYGON_OFFSET_LINE" },
        { GL_POLYGON_OFFSET_POINT, "GL_POLYGON_OFFSET_POINT" },
        { GL_POLYGON_OFFSET_UNITS, "GL_POLYGON_OFFSET_UNITS" },
        { GL_RASTERIZER_DISCARD, "GL_RASTERIZER_DISCARD" },
        { GL_SAMPLE_ALPHA_TO_COVERAGE, "GL_SAMPLE_ALPHA_TO_COVERAGE" },
        { GL_SAMPLE_ALPHA_TO_ONE, "GL_SAMPLE_ALPHA_TO_ONE" },
        { GL_SAMPLE_MASK, "GL_SAMPLE_MASK" },
        { GL_SAMPLE_MASK_VALUE, "GL_SAMPLE_MASK_VALUE" },
        { GL_SAMPLE_SHADING, "GL_SAMPLE_SHADING" },
        { GL_SCISSOR_BOX, "GL_SCISSOR_BOX" },
        { GL_SCISSOR_TEST, "GL_SCISSOR_TEST" },
        { GL_STENCIL_BACK_FAIL, "GL_STENCIL_BACK_FAIL" },
        { GL_STENCIL_BACK_FUNC, "GL_STENCIL_BACK_FUNC" },
        { GL_STENCIL_BACK_PASS_DEPTH_FAIL, "GL_STENCIL_BACK_PASS_DEPTH_FAIL" },
        { GL_STENCIL_BACK_PASS_DEPTH_PASS, "GL_STENCIL_BACK_PASS_DEPTH_PASS" },
        { GL_STENCIL_BACK_REF, "GL_STENCIL_BACK_REF" },
        { GL_STENCIL_BACK_VALUE_MASK, "GL_STENCIL_BACK_VALUE_MASK" },
        { GL_STENCIL_BACK_WRITEMASK, "GL_STENCIL_BACK_WRITEMASK" },
        { GL_STENCIL_FAIL, "GL_STENCIL_FAIL" },
        { GL_STENCIL_FUNC, "GL_STENCIL_FUNC" },
        { GL_STENCIL_PASS_DEPTH_FAIL, "GL_STENCIL_PASS_DEPTH_FAIL" },
        { GL_STENCIL_PASS_DEPTH_PASS, "GL_STENCIL_PASS_DEPTH_PASS" },
        { GL_STENCIL_REF, "GL_STENCIL_REF" },
        { GL_STENCIL_TEST, "GL_STENCIL_TEST" },
        { GL_STENCIL_VALUE_MASK, "GL_STENCIL_VALUE_MASK" },
        { GL_STENCIL_WRITEMASK, "GL_STENCIL_WRITEMASK" },
        { GL_VIEWPORT, "GL_VIEWPORT" },
    };

    size_t n = sizeof(table) / sizeof(table[0]);
    if (auto *found = binarySearch(&table[0], &table[0] + n, name)) {
        return found->first;
    }
    assert(0);
    return GL_INVALID_ENUM;
}


void cgGlSetViewport(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glViewport(ptr[0], ptr[1], ptr[2], ptr[3]);
}

void cgGlSetScissorTest(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_SCISSOR_TEST); } else { glDisable(GL_SCISSOR_TEST); }
}

void cgGlSetScissorBox(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glScissor(ptr[0], ptr[1], ptr[2], ptr[3]);
}

void cgGlSetDepthClamp(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_DEPTH_CLAMP); } else { glDisable(GL_DEPTH_CLAMP); }
}

void cgGlSetRasterizerDiscard(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_RASTERIZER_DISCARD); } else { glDisable(GL_RASTERIZER_DISCARD); }
}

void cgGlSetPolygonMode(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glPolygonMode(GL_FRONT_AND_BACK, ptr[0]);
}

void cgGlSetCullFace(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_CULL_FACE); } else { glDisable(GL_CULL_FACE); }
}

void cgGlSetCullFaceMode(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glCullFace(ptr[0]);
}

void cgGlSetFrontFace(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glFrontFace(ptr[0]);
}

void cgGlSetPolygonOffsetPoint(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_POLYGON_OFFSET_POINT); } else { glDisable(GL_POLYGON_OFFSET_POINT); }
}

void cgGlSetPolygonOffsetLine(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_POLYGON_OFFSET_LINE); } else { glDisable(GL_POLYGON_OFFSET_LINE); }
}

void cgGlSetPolygonOffsetFill(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_POLYGON_OFFSET_FILL); } else { glDisable(GL_POLYGON_OFFSET_FILL); }
}

void cgGlSetPolygonOffsetFactor(const void *value)
{
    GLfloat current[1];
    glGetFloatv(GL_POLYGON_OFFSET_UNITS, &current[0]);
    const GLfloat *ptr = static_cast<const GLfloat *>(value);
    glPolygonOffset(ptr[0], current[0]);
}

void cgGlSetPolygonOffsetUnits(const void *value)
{
    GLfloat current[1];
    glGetFloatv(GL_POLYGON_OFFSET_FACTOR, &current[0]);
    const GLfloat *ptr = static_cast<const GLfloat *>(value);
    glPolygonOffset(current[0], ptr[0]);
}

void cgGlSetLineWidth(const void *value)
{
    const GLfloat *ptr = static_cast<const GLfloat *>(value);
    glLineWidth(ptr[0]);
}

void cgGlSetMultisample(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_MULTISAMPLE); } else { glDisable(GL_MULTISAMPLE); }
}

void cgGlSetSampleShading(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_SAMPLE_SHADING); } else { glDisable(GL_SAMPLE_SHADING); }
}

void cgGlSetMinSampleShadingValue(const void *value)
{
    const GLfloat *ptr = static_cast<const GLfloat *>(value);
    glMinSampleShading(ptr[0]);
}

void cgGlSetSampleMask(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_SAMPLE_MASK); } else { glDisable(GL_SAMPLE_MASK); }
}

void cgGlSetSampleMaskValue(const void *value)
{
    const GLbitfield *ptr = static_cast<const GLbitfield *>(value);
    glSampleMaski(0, ptr[0]);  // Only set lower 32-bits
}

void cgGlSetSampleAlphaToCoverage(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE); } else { glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE); }
}

void cgGlSetSampleAlphaToOne(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_SAMPLE_ALPHA_TO_ONE); } else { glDisable(GL_SAMPLE_ALPHA_TO_ONE); }
}

void cgGlSetDepthTest(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_DEPTH_TEST); } else { glDisable(GL_DEPTH_TEST); }
}

void cgGlSetDepthWritemask(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glDepthMask(ptr[0]);
}

void cgGlSetDepthFunc(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glDepthFunc(ptr[0]);
}

void cgGlSetDepthRange(const void *value)
{
    const GLfloat *ptr = static_cast<const GLfloat *>(value);
    glDepthRange(ptr[0], ptr[1]);
}

void cgGlSetStencilTest(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_STENCIL_TEST); } else { glDisable(GL_STENCIL_TEST); }
}

void cgGlSetStencilFail(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_FAIL, &current[0]);
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_PASS, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilOpSeparate(GL_FRONT, ptr[0], current[0], current[1]);
}

void cgGlSetStencilPassDepthFail(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_FAIL, &current[0]);
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_PASS, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilOpSeparate(GL_FRONT, current[0], ptr[0], current[1]);
}

void cgGlSetStencilPassDepthPass(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_FAIL, &current[0]);
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_FAIL, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilOpSeparate(GL_FRONT, current[0], current[1], ptr[0]);
}

void cgGlSetStencilFunc(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_REF, &current[0]);
    glGetIntegerv(GL_STENCIL_VALUE_MASK, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilFuncSeparate(GL_FRONT, ptr[0], current[0], current[1]);
}

void cgGlSetStencilRef(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_FUNC, &current[0]);
    glGetIntegerv(GL_STENCIL_VALUE_MASK, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilFuncSeparate(GL_FRONT, current[0], ptr[0], current[1]);
}

void cgGlSetStencilWritemask(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilMaskSeparate(GL_FRONT, ptr[0]);
}

void cgGlSetStencilValueMask(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_FUNC, &current[0]);
    glGetIntegerv(GL_STENCIL_REF, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilFuncSeparate(GL_FRONT, current[0], current[1], ptr[0]);
}

void cgGlSetStencilBackFail(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_BACK_PASS_DEPTH_FAIL, &current[0]);
    glGetIntegerv(GL_STENCIL_BACK_PASS_DEPTH_PASS, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilOpSeparate(GL_BACK, ptr[0], current[0], current[1]);
}

void cgGlSetStencilBackPassDepthFail(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_BACK_FAIL, &current[0]);
    glGetIntegerv(GL_STENCIL_BACK_PASS_DEPTH_PASS, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilOpSeparate(GL_BACK, current[0], ptr[0], current[1]);
}

void cgGlSetStencilBackPassDepthPass(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_BACK_FAIL, &current[0]);
    glGetIntegerv(GL_STENCIL_BACK_PASS_DEPTH_FAIL, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilOpSeparate(GL_BACK, current[0], current[1], ptr[0]);
}

void cgGlSetStencilBackFunc(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_BACK_REF, &current[0]);
    glGetIntegerv(GL_STENCIL_BACK_VALUE_MASK, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilFuncSeparate(GL_BACK, ptr[0], current[0], current[1]);
}

void cgGlSetStencilBackRef(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_BACK_FUNC, &current[0]);
    glGetIntegerv(GL_STENCIL_BACK_VALUE_MASK, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilFuncSeparate(GL_BACK, current[0], ptr[0], current[1]);
}

void cgGlSetStencilBackWritemask(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilMaskSeparate(GL_BACK, ptr[0]);
}

void cgGlSetStencilBackValueMask(const void *value)
{
    GLint current[2];
    glGetIntegerv(GL_STENCIL_BACK_FUNC, &current[0]);
    glGetIntegerv(GL_STENCIL_BACK_REF, &current[1]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glStencilFuncSeparate(GL_BACK, current[0], current[1], ptr[0]);
}

void cgGlSetColorLogicOp(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_COLOR_LOGIC_OP); } else { glDisable(GL_COLOR_LOGIC_OP); }
}

void cgGlSetLogicOpMode(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glLogicOp(ptr[0]);
}

void cgGlSetBlend(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    if (ptr[0]) { glEnable(GL_BLEND); } else { glDisable(GL_BLEND); }
}

void cgGlSetBlendSrcRGB(const void *value)
{
    GLint current[3];
    glGetIntegerv(GL_BLEND_DST_RGB, &current[0]);
    glGetIntegerv(GL_BLEND_SRC_ALPHA, &current[1]);
    glGetIntegerv(GL_BLEND_DST_ALPHA, &current[2]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glBlendFuncSeparate(ptr[0], current[0], current[1], current[2]);
}

void cgGlSetBlendDstRGB(const void *value)
{
    GLint current[3];
    glGetIntegerv(GL_BLEND_SRC_RGB, &current[0]);
    glGetIntegerv(GL_BLEND_SRC_ALPHA, &current[1]);
    glGetIntegerv(GL_BLEND_DST_ALPHA, &current[2]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glBlendFuncSeparate(current[0], ptr[0], current[1], current[2]);
}

void cgGlSetBlendEquationRGB(const void *value)
{
    GLint current[1];
    glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &current[0]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glBlendEquationSeparate(ptr[0], current[0]);
}

void cgGlSetBlendSrcAlpha(const void *value)
{
    GLint current[3];
    glGetIntegerv(GL_BLEND_SRC_RGB, &current[0]);
    glGetIntegerv(GL_BLEND_DST_RGB, &current[1]);
    glGetIntegerv(GL_BLEND_DST_ALPHA, &current[2]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glBlendFuncSeparate(current[0], current[1], ptr[0], current[2]);
}

void cgGlSetBlendDstAlpha(const void *value)
{
    GLint current[3];
    glGetIntegerv(GL_BLEND_SRC_RGB, &current[0]);
    glGetIntegerv(GL_BLEND_DST_RGB, &current[1]);
    glGetIntegerv(GL_BLEND_SRC_ALPHA, &current[2]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glBlendFuncSeparate(current[0], current[1], current[2], ptr[0]);
}

void cgGlSetBlendEquationAlpha(const void *value)
{
    GLint current[1];
    glGetIntegerv(GL_BLEND_EQUATION_RGB, &current[0]);
    const GLint *ptr = static_cast<const GLint *>(value);
    glBlendEquationSeparate(current[0], ptr[0]);
}

void cgGlSetColorWritemask(const void *value)
{
    const GLint *ptr = static_cast<const GLint *>(value);
    glColorMask(ptr[0], ptr[1], ptr[2], ptr[3]);
}

void cgGlSetBlendColor(const void *value)
{
    const GLfloat *ptr = static_cast<const GLfloat *>(value);
    glBlendColor(ptr[0], ptr[1], ptr[2], ptr[3]);
}

} // namespace cg
