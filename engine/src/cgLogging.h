/// @file    cgLogging.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"

namespace cg {

/// Logging levels (same as in Python's `logging` module)
enum CgLogLevel {
    CG_NOT_SET = 0,
    CG_DEBUG = 10,
    CG_INFO = 20,
    CG_WARNING = 30,
    CG_ERROR = 40,
    CG_CRITICAL = 50,
    CG_LOG_LEVEL_END = CG_CRITICAL,
};

/// Set global logging level. Any logging below this level will be suppressed.
/// Initial value is Level::NotSet (no logging).
///
/// @remark Thread-safe
void cgSetLoggingLevel(const CgLogLevel level);

/// Set global logging file. Initially, no file is set and log output is written
/// to std::cerr. Pass nullptr to revert logging to std::cerr.
///
/// @remark Thread-safe
void cgSetLogFilename(const char *filename);

/// @name Logging functions
///
/// @remark Thread-safe
///@{
void cgLog(const CgLogLevel &level, const char *format, ...);
void cgLogDebug(const char *format, ...);
void cgLogInfo(const char *format, ...);
void cgLogWarning(const char *format, ...);
void cgLogError(const char *format, ...);
void cgLogCritical(const char *format, ...);
///@}

} // namespace cg
