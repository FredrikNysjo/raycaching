/// @file    hello_triangle.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgTypes.h"
#include "cgLogging.h"
#include "cgUtils.h"
#include "cgGL.h"
#include "cgRenderer.h"

#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

#include <cstdlib>
#include <iostream>

// Struct for application context
struct Context {
    int width;
    int height;
    float aspect;
    GLFWwindow *window;
    GLuint defaultVAO;
    GLuint triangleProgram;
    cg::CgDrawable triangleDrawable;
    float elapsedTime;
};

// Return the absolute path to the shader directory
std::string shaderDir(void)
{
    std::string rootDir = cg::cgGetEnvSafe("CG_ENGINE_ROOT");
    if (rootDir.empty()) {
        rootDir = "..";
    }
    return rootDir + "/examples/01_triangle/src/shaders/";
}

void init(Context &ctx)
{
    // Initialize default GL pipeline state setters
    cg::cgInitRenderStateSetters();

    // Load triangle shader program
    ctx.triangleProgram = cg::cgLoadShaderProgram(shaderDir() + "triangle.vert",
                                                  shaderDir() + "triangle.frag");

    // Create triangle drawable using default VAO for bufferless rendering
    ctx.triangleDrawable.program = ctx.triangleProgram;
    ctx.triangleDrawable.vao = ctx.defaultVAO;
    ctx.triangleDrawable.drawInfo.vertexCount = 3;
    ctx.triangleDrawable.drawInfo.indexCount = 0;
    ctx.triangleDrawable.drawInfo.primitiveType = GL_TRIANGLES;
}

void render(Context &ctx)
{
    // Create pipeline state for triangle drawable
    cg::CgRenderStateInfo pipelineStateInfo;
    pipelineStateInfo[GL_VIEWPORT] = glm::ivec4(0, 0, ctx.width, ctx.height);

    // Clear buffers
    const float clearColor[] = { 0.2f, 0.2f, 0.2f, 0.0f };
    glClearBufferfv(GL_COLOR, 0, &clearColor[0]);
    glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

    // Render triangle
    cg::cgRendererDraw(cg::CgCamera(), &ctx.triangleDrawable, 1, ctx.elapsedTime, &pipelineStateInfo);
}

void errorCallback(int error, const char* description)
{
    std::cerr << description << std::endl;
}

void resizeCallback(GLFWwindow* window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->width = width;
    ctx->height = height;
    ctx->aspect = float(width) / float(height);
}

int main(void)
{
    // Set logging level
    cg::cgSetLoggingLevel(cg::CG_INFO);

    // Create application context
    Context ctx;

    // Create GLFW window and GL context
    glfwSetErrorCallback(errorCallback);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    ctx.width = 512;
    ctx.height = 512;
    ctx.aspect = float(ctx.width) / float(ctx.height);
    ctx.window = glfwCreateWindow(ctx.width, ctx.height, "example 01 - triangle", nullptr, nullptr);
    if (!ctx.window) {
        std::exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(ctx.window);
    glfwSetWindowUserPointer(ctx.window, &ctx);
    glfwSetFramebufferSizeCallback(ctx.window, resizeCallback);

    // Load GL API functions
    if (cg::cgLoadGL() != cg::CgStatus::Success) {
        std::exit(EXIT_FAILURE);
    }
    cg::cgLogInfo("GL version: %s\n", glGetString(GL_VERSION));

    // Create default VAO and set initial pipeline state
    glGenVertexArrays(1, &ctx.defaultVAO);
    glBindVertexArray(ctx.defaultVAO);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    // Initialize application
    init(ctx);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window)) {
        glfwPollEvents();
        ctx.elapsedTime = glfwGetTime();
        render(ctx);
        glfwSwapBuffers(ctx.window);
    }

    // Shutdown
    glfwDestroyWindow(ctx.window);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
