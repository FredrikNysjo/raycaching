#!/bin/bash

if [ -z "$CG_ENGINE_ROOT" ]; then
    echo "CG_ENGINE_ROOT is not set"
    exit 1
fi

if [ -z "$CG_GENERATOR" ]; then
    CG_GENERATOR="Unix Makefiles"
    if [[ "$OSTYPE" == "msys" ]]; then
        CG_GENERATOR="Visual Studio 16 2019"
    fi
fi

# Generate build files
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -G "$CG_GENERATOR" ../ -DCMAKE_INSTALL_PREFIX=../ && \

# Build and install the program
cmake --build . --config RelWithDebInfo --target install && \

# Run the program
cd ../bin && \
./raycaching
