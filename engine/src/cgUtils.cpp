/// @file    cgUtils.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgUtils.h"

#include <lodepng.h>

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>

namespace cg {

const char *cgGetEnvSafe(const char *name)
{
    char const *value = std::getenv(name);
    return (value != nullptr) ? value : "";
}

std::string cgReadShaderSource(const std::string &filename)
{
    std::ifstream file(filename);
    std::stringstream stream;
    stream << file.rdbuf();

    return stream.str();
}

void cgShowShaderInfoLog(GLuint shader)
{
    GLint infoLogLength = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
    std::vector<char> infoLog(infoLogLength);
    glGetShaderInfoLog(shader, infoLogLength, &infoLogLength, &infoLog[0]);
    std::string infoLogStr(infoLog.begin(), infoLog.end());
    std::cerr << infoLogStr << std::endl;
}

void cgShowProgramInfoLog(GLuint program)
{
    GLint infoLogLength = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
    std::vector<char> infoLog(infoLogLength);
    glGetProgramInfoLog(program, infoLogLength, &infoLogLength, &infoLog[0]);
    std::string infoLogStr(infoLog.begin(), infoLog.end());
    std::cerr << infoLogStr << std::endl;
}

GLuint cgLoadShader(const std::string &filename, GLenum shaderType)
{
    // Load shader source
    std::string source = cgReadShaderSource(filename);

    // Create and compile shader
    GLuint shader = glCreateShader(shaderType);
    const char *sourcePtr = source.c_str();
    glShaderSource(shader, 1, &sourcePtr, nullptr);
    glCompileShader(shader);

    // Check compilation status
    GLint compiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
        std::cerr << "Shader compilation failed:" << std::endl;
        cgShowShaderInfoLog(shader);
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

GLuint cgCreateShaderProgram(const CgShaderProgramInfo &programInfo)
{
    // Create program
    GLuint program = glCreateProgram();
    for (GLuint shader : programInfo.shaders) {
        glAttachShader(program, shader);
    }

    const CgTransformFeedbackInfo &xfInfo = programInfo.xfInfo;
    if (xfInfo.varyings.size()) {
        // Define transform feedback outputs before linking
        glTransformFeedbackVaryings(program, xfInfo.varyings.size(),
                                    (const char **)&xfInfo.varyings[0],
                                    xfInfo.bufferMode);
    }

    // Link program
    glLinkProgram(program);

    // Check linking status
    GLint linked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        std::cerr << "Linking failed:" << std::endl;
        cgShowProgramInfoLog(program);
        glDeleteProgram(program);
        for (GLuint shader : programInfo.shaders) { glDeleteShader(shader); }
        return 0;
    }

    // Mark shaders for deletion after program is deleted
    for (GLuint shader : programInfo.shaders) { glDeleteShader(shader); }

    return program;
}

GLuint cgLoadShaderProgram(const std::string &vsFilename,
                           const std::string &fsFilename)
{
    CgShaderProgramInfo programInfo;
    programInfo.shaders = {
        cgLoadShader(vsFilename, GL_VERTEX_SHADER),
        cgLoadShader(fsFilename, GL_FRAGMENT_SHADER),
    };

    GLuint program = cgCreateShaderProgram(programInfo);

    return program;
}

GLuint cgLoad2DTexture(const std::string &filename)
{
    std::vector<unsigned char> data;
    unsigned width, height;
    unsigned error = lodepng::decode(data, width, height, filename);
    if (error != 0) {
        std::cout << "Error: " << lodepng_error_text(error) << std::endl;
        std::exit(EXIT_FAILURE);
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, &(data[0]));
    glBindTexture(GL_TEXTURE_2D, 0);

    return texture;
}

// Load cubemap texture and let OpenGL generate a mipmap chain
GLuint cgLoadCubemap(const std::string &dirname)
{
    const char *filenames[] = { "posx.png", "negx.png", "posy.png", "negy.png", "posz.png", "negz.png" };
    const GLenum targets[] = {
        GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };
    const unsigned num_sides = 6; 

    std::vector<unsigned char> data[num_sides];
    unsigned width;
    unsigned height;
    for (unsigned i = 0; i < num_sides; ++i) {
        std::string filename = dirname + "/" + filenames[i];
        unsigned error = lodepng::decode(data[i], width, height, filename);
        if (error != 0) {
            std::cout << "Error: " << lodepng_error_text(error) << std::endl;
            std::exit(EXIT_FAILURE);
        }
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    for (unsigned i = 0; i < num_sides; ++i) {
        glTexImage2D(targets[i], 0, GL_SRGB8_ALPHA8, width, height,
                     0, GL_RGBA, GL_UNSIGNED_BYTE, &(data[i][0]));
    }
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return texture;
}

// Load cubemap with pre-computed mipmap chain
GLuint cgLoadCubemapMipmap(const std::string &dirname)
{
    const char *levels[] = { "2048", "512", "128", "32", "8", "2", "0.5", "0.125" };
    const char *filenames[] = { "posx.png", "negx.png", "posy.png", "negy.png", "posz.png", "negz.png" };
    const GLenum targets[] = {
        GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };

    const unsigned num_levels = sizeof(levels) / sizeof(levels[0]);
    const unsigned num_sides = 6; 

    std::vector<unsigned char> data[num_levels][num_sides];
    unsigned width[num_levels];
    unsigned height[num_levels];
    for (unsigned i = 0; i < num_levels; ++i) {
        for (unsigned j = 0; j < num_sides; ++j) {
            std::string filename = dirname + "/" + levels[i] + "/" + filenames[j];
            unsigned error = lodepng::decode(data[i][j], width[i], height[i], filename);
            if (error != 0) {
                std::cout << "Error: " << lodepng_error_text(error) << std::endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    for (unsigned i = 0; i < num_levels; ++i) {
        for (unsigned j = 0; j < num_sides; ++j) {
            glTexImage2D(targets[j], i, GL_SRGB8_ALPHA8, width[i], height[i],
                         0, GL_RGBA, GL_UNSIGNED_BYTE, &(data[i][j][0]));
        }
    }
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return texture;
}

// Helper functions
namespace {
glm::vec3 mapMousePointToUnitSphere(const glm::vec2 &point, double radius, const glm::vec2 &center)
{
    float x = point[0] - center[0];
    float y = -point[1] + center[1];
    float z = 0.0f;
    if (x * x + y * y < radius * radius / 2.0f) {
        z = std::sqrt(radius * radius - (x * x + y * y));
    }
    else {
        z = (radius * radius / 2.0f) / std::sqrt(x * x + y * y);
    }
    return glm::normalize(glm::vec3(x, y, z));
}

void cgComputeNormals(const std::vector<glm::vec3> &vertices,
                      const std::vector<std::int32_t> &indices,
                      std::vector<glm::vec3> *normals)
{
    normals->resize(vertices.size(), glm::vec3(0.0f, 0.0f, 0.0f));

    // Compute per-vertex normals by averaging the unnormalized face normals
    std::int32_t vertexIndex0, vertexIndex1, vertexIndex2;
    glm::vec3 normal;
    int numIndices = indices.size();
    for (int i = 0; i < numIndices; i += 3) {
        vertexIndex0 = indices[i];
        vertexIndex1 = indices[i + 1];
        vertexIndex2 = indices[i + 2];
        normal = glm::cross(vertices[vertexIndex1] - vertices[vertexIndex0],
                            vertices[vertexIndex2] - vertices[vertexIndex0]);
        (*normals)[vertexIndex0] += normal;
        (*normals)[vertexIndex1] += normal;
        (*normals)[vertexIndex2] += normal;
    }

    int numNormals = normals->size();
    for (int i = 0; i < numNormals; i++) {
        (*normals)[i] = glm::normalize((*normals)[i]);
    }
}
} // namespace

void cgTrackballStartTracking(CgTrackball &trackball, const glm::vec2 &point)
{
    trackball.vStart = mapMousePointToUnitSphere(point, trackball.radius, trackball.center);
    trackball.qStart = glm::quat(trackball.qCurrent);
    trackball.tracking = true;
}

void cgTrackballStopTracking(CgTrackball &trackball)
{
    trackball.tracking = false;
}

void cgTrackballMove(CgTrackball &trackball, const glm::vec2 &point)
{
    glm::vec3 vCurrent = mapMousePointToUnitSphere(point, trackball.radius, trackball.center);
    glm::vec3 rotationAxis = glm::cross(trackball.vStart, vCurrent);
    float dotProduct = std::max(std::min(glm::dot(trackball.vStart, vCurrent), 1.0f), -1.0f);
    float rotationAngle = std::acos(dotProduct);
    float eps = 0.01f;
    if (rotationAngle < eps) {
        trackball.qCurrent = glm::quat(trackball.qStart);
    }
    else {
        // Note: here we provide rotationAngle in radians. Older versions
        // of GLM (0.9.3 or earlier) require the angle in degrees.
        glm::quat q = glm::angleAxis(rotationAngle, rotationAxis);
        q = glm::normalize(q);
        trackball.qCurrent = glm::normalize(glm::cross(q, trackball.qStart));
    }
}

glm::mat4 cgTrackballGetRotationMatrix(CgTrackball &trackball)
{
    return glm::mat4_cast(trackball.qCurrent);
}

bool cgObjMeshLoad(CgOBJMesh &mesh, const std::string &filename)
{
    const std::string VERTEX_LINE("v ");
    const std::string FACE_LINE("f ");

    // Open OBJ file
    std::ifstream f(filename.c_str());
    if (!f.is_open()) {
        std::cerr << "Could not open " << filename << std::endl;
        return false;
    }

    // Extract vertices and indices
    std::string line;
    glm::vec3 vertex;
    std::int32_t vertexIndex0, vertexIndex1, vertexIndex2;
    while (!f.eof()) {
        std::getline(f, line);
        if (line.substr(0, 2) == VERTEX_LINE) {
            std::istringstream vertexLine(line.substr(2));
            vertexLine >> vertex.x;
            vertexLine >> vertex.y;
            vertexLine >> vertex.z;
            mesh.vertices.push_back(vertex);
        }
        else if (line.substr(0, 2) == FACE_LINE) {
            std::istringstream faceLine(line.substr(2));
            faceLine >> vertexIndex0;
            faceLine >> vertexIndex1;
            faceLine >> vertexIndex2;
            mesh.indices.push_back(vertexIndex0 - 1);
            mesh.indices.push_back(vertexIndex1 - 1);
            mesh.indices.push_back(vertexIndex2 - 1);
        }
        else {
            // Ignore line
        }
    }

    // Close OBJ file
    f.close();

    // Compute normals
    cgComputeNormals(mesh.vertices, mesh.indices, &mesh.normals);

    // Display log message
    std::cout << "Loaded OBJ file " << filename << std::endl;
    int numTriangles = mesh.indices.size() / 3;
    std::cout << "Number of triangles: " << numTriangles << std::endl;

    return true;
}

struct uvec3Less {
    bool operator() (const glm::uvec3 &a, const glm::uvec3 &b) const
    {
        return (a.x < b.x) |
               ((a.x == b.x) & (a.y < b.y)) |
               ((a.x == b.x) & (a.y == b.y) & (a.z < b.z));
    }
};

bool cgObjMeshUVLoad(CgOBJMeshUV &mesh, const std::string &filename)
{
    const std::string VERTEX_LINE("v ");
    const std::string TEXCOORD_LINE("vt ");
    const std::string NORMAL_LINE("vn ");
    const std::string FACE_LINE("f ");

    std::string line;
    glm::vec3 vertex;
    glm::vec3 normal;
    glm::vec3 texcoord;
    std::int32_t vindex[3];
    std::int32_t tindex[3];
    std::int32_t nindex[3];

    // Open OBJ file
    std::ifstream f(filename.c_str());
    if (!f.is_open()) {
        std::cerr << "Could not open " << filename << std::endl;
        return false;
    }

    // First pass: read vertex data into temporary mesh
    CgOBJMeshUV tmp_mesh;
    while (!f.eof()) {
        std::getline(f, line);
        if (line.substr(0, 2) == VERTEX_LINE) {
            std::istringstream ss(line.substr(2));
            ss >> vertex.x >> vertex.y >> vertex.z;
            tmp_mesh.vertices.push_back(vertex);
        }
        else if (line.substr(0, 3) == TEXCOORD_LINE) {
            std::istringstream ss(line.substr(3));
            ss >> texcoord.x >> texcoord.y >> texcoord.z;
            tmp_mesh.texcoords.push_back(texcoord);
        }
        else if (line.substr(0, 3) == NORMAL_LINE) {
            std::istringstream ss(line.substr(3));
            ss >> normal.x >> normal.y >> normal.z;
            tmp_mesh.normals.push_back(normal);
        }
        else {
            // Ignore line
        }
    }

    // Rewind file
    f.clear();
    f.seekg(0);

    // Clear old mesh and pre-allocate space for new mesh data
    mesh.vertices.clear();
    mesh.vertices.reserve(tmp_mesh.vertices.size());
    mesh.texcoords.clear();
    mesh.texcoords.reserve(tmp_mesh.texcoords.size());
    mesh.normals.clear();
    mesh.normals.reserve(tmp_mesh.normals.size());
    mesh.indices.clear();

    // Set up dictionary for mapping unique tuples to indices
    std::map<glm::uvec3, unsigned, uvec3Less> visited;
    unsigned next_index = 0;
    glm::uvec3 key;

    // Second pass: read faces and construct per-vertex texcoords/normals.
    // Note: OBJ-indices start at one, so we need to subtract indices by one.
    while (!f.eof()) {
        std::getline(f, line);
        if (line.substr(0, 2) == FACE_LINE) {
            if (std::sscanf(line.c_str(), "f %d %d %d",
                            &vindex[0], &vindex[1], &vindex[2]) == 3) {
                for (unsigned i = 0; i < 3; ++i) {
                    key = glm::uvec3(vindex[i], 0, 0);
                    if (visited.count(key) == 0) {
                        visited[key] = next_index++;
                        mesh.vertices.push_back(tmp_mesh.vertices[vindex[i] - 1]);
                    }
                    mesh.indices.push_back(visited[key]);
                }
            }
            else if (std::sscanf(line.c_str(), "f %d/%d %d/%d %d/%d",
                                 &vindex[0], &tindex[0],
                                 &vindex[1], &tindex[1],
                                 &vindex[2], &tindex[2]) == 6) {
                for (unsigned i = 0; i < 3; ++i) {
                    key = glm::uvec3(vindex[i], tindex[i], 0);
                    if (visited.count(key) == 0) {
                        visited[key] = next_index++;
                        mesh.vertices.push_back(tmp_mesh.vertices[vindex[i] - 1]);
                        mesh.texcoords.push_back(tmp_mesh.texcoords[tindex[i] - 1]);
                    }
                    mesh.indices.push_back(visited[key]);
                }
            }
            else if (std::sscanf(line.c_str(), "f %d//%d %d//%d %d//%d",
                                 &vindex[0], &nindex[0],
                                 &vindex[1], &nindex[1],
                                 &vindex[2], &nindex[2]) == 6) {
                for (unsigned i = 0; i < 3; ++i) {
                    key = glm::uvec3(vindex[i], nindex[i], 0);
                    if (visited.count(key) == 0) {
                        visited[key] = next_index++;
                        mesh.vertices.push_back(tmp_mesh.vertices[vindex[i] - 1]);
                        mesh.normals.push_back(tmp_mesh.normals[nindex[i] - 1]);
                    }
                    mesh.indices.push_back(visited[key]);
                }
            }
            else if (std::sscanf(line.c_str(), "f %d/%d/%d %d/%d/%d %d/%d/%d",
                                 &vindex[0], &tindex[0], &nindex[0],
                                 &vindex[1], &tindex[1], &nindex[1],
                                 &vindex[2], &tindex[2], &nindex[2]) == 9) {
                for (unsigned i = 0; i < 3; ++i) {
                    key = glm::uvec3(vindex[i], tindex[i], nindex[i]);
                    if (visited.count(key) == 0) {
                        visited[key] = next_index++;
                        mesh.vertices.push_back(tmp_mesh.vertices[vindex[i] - 1]);
                        mesh.texcoords.push_back(tmp_mesh.texcoords[tindex[i] - 1]);
                        mesh.normals.push_back(tmp_mesh.normals[nindex[i] - 1]);
                    }
                    mesh.indices.push_back(visited[key]);
                }
            }
        }
    }

    // Compute normals (if OBJ-file did not contain normals)
    if (mesh.normals.size() == 0) {
        cgComputeNormals(mesh.vertices, mesh.indices, &mesh.normals);
    }

    return true;
}

} // namespace cg
