/// @file    cgTypes.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include <cstdint>
#include <cstring>
#include <array>
#include <vector>
#include <deque>
#include <unordered_set>
#include <unordered_map>
#include <memory>

namespace cg {

/// Return status codes
enum class CgStatus {
    Success=0,
    Failure=1,
};

/// Container for fixed sized arrays
template <typename ValueType, size_t N>
struct CgArray {
    typedef std::array<ValueType, N> type;
    typedef ValueType value_type;
};

/// Container for dynamic sized arrays
template <typename ValueType>
struct CgVector {
    typedef std::vector<ValueType> type;
    typedef ValueType value_type;
};

/// Container for queues
template <typename ValueType>
struct CgDeque {
    typedef std::deque<ValueType> type;
    typedef ValueType value_type;
};

/// Container for unordered maps (hash maps)
template <typename KeyType, typename ValueType,
          typename Hash=std::hash<KeyType> >
struct CgUnorderedMap {
    typedef std::unordered_map<KeyType, ValueType, Hash> type;
    typedef KeyType key_type;
    typedef ValueType value_type;
};

/// Container for unordered sets (hash sets)
template <typename ValueType, typename Hash=std::hash<ValueType> >
struct CgUnorderedSet {
    typedef std::unordered_set<ValueType, Hash> type;
    typedef ValueType value_type;
};

/// Container for single-ownership smart pointer
template <typename ValueType>
struct CgUniquePtr {
    typedef std::unique_ptr<ValueType> type;
    typedef ValueType value_type;
};

/// Container for fixed-size strings
template <size_t N>
struct CgFixedString {
    typename CgArray<char, N>::type buffer;

    CgFixedString(const char *s)
    {
        unsigned i = 0;
        while ((i < N) && ((buffer[i] = s[i]) != '\0')) { ++i; }
        buffer[N - 1] = '\0';
    }

    const char *c_str() const
    {
        return &buffer[0];
    }

    bool operator==(const CgFixedString &s) const
    {
        return std::strncmp(&buffer[0], &s.buffer[0], N) == 0;
    }
};

/// Hash function that makes CgFixedString usable with CgUnordered[Set|Map]
template <size_t N>
struct CgFixedStringHash {
    size_t operator()(const CgFixedString<N> &s) const
    {
        // Compute hash as byte sum of string
        size_t hash = 0;
        unsigned i = 0;
        while ((i < N) && (s.buffer[i] != '\0')) { hash += s.buffer[i]; ++i; }
        return hash;
    }
};

} // namespace cg
