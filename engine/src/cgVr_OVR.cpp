/// @file    cgVr_OVR.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#ifdef CG_WITH_OVR_SDK

#include "cgVr_OVR.h"
#include "cgTypes.h"
#include "cgLogging.h"
#include "cgRenderer.h"

#ifndef _WIN32
#define GLFW_EXPOSE_NATIVE_X11
#define GLFW_EXPOSE_NATIVE_GLX
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#undef Success
#endif // _WIN32

#include <sixense.h>

namespace cg {

static sixenseControllerData s_controllers[2];
static int s_buttons[2] = { 0 };        // For event detection
static float s_triggers[2] = { 0.0f };  // For event detection
static int s_dpad[2] = { 0 };           // For event detection

CgStatus cgHmdInit(CgHmd *hmd)
{
    // vr::HmdError hmd_error;
    // hmd->hmd = vr::VR_Init(&hmd_error, vr::VRApplication_Scene);
    // if (!hmd->hmd) {
    //     cgLogError("%s\n", vr::VR_GetVRInitErrorAsEnglishDescription(hmd_error));
    //     return CgStatus::Failure;
    // }
    // return CgStatus::Success;
    vr::HmdError hmd_error;
    vr::VR_Init(&hmd_error, vr::VRApplication_Scene);

    // Initialize Oculus devices (HMD and sensor)
    ovr_Initialize();
    hmd->hmd = ovrHmd_Create(0);
    if (!hmd->hmd) {
        hmd->hmd = ovrHmd_CreateDebug(ovrHmd_DK1);
    }
    if (!hmd->hmd) {
        cgLogError("cgHmdInit: Could not create HMD\n");
        return CgStatus::Failure;
    }
    ovrHmd_ConfigureTracking(hmd->hmd, ovrTrackingCap_Orientation |
                             ovrTrackingCap_MagYawCorrection |
                             ovrTrackingCap_Position, 0);

    // Initialize Sixense devices (Hydra controllers)
    sixenseInit();  // Don't check status 

    return CgStatus::Success;
}

CgStatus cgHmdShutdown(CgHmd *hmd)
{
	// vr::VR_Shutdown();
    // return CgStatus::Success;

    // Shutdown Oculus devices
    if (hmd->hmd) {
        ovrHmd_Destroy(hmd->hmd);
    }
	ovr_Shutdown();

    // Shutdown Sixense devices
    sixenseExit();

    return CgStatus::Success;
}

CgStatus cgHmdConfigureRendering(CgHmd *hmd)
{
	// hmd->hmd->GetRecommendedRenderTargetSize(&hmd->renderTargetSize[0], &hmd->renderTargetSize[1]);
	// hmd->renderTargetSize[0] *= 2;  // Multiply width to obtain size for left + right eye 
    // cgLogInfo("cgHmdConfigureRendering: HMD framebuffer size: %d %d\n", hmd->renderTargetSize[0], hmd->renderTargetSize[1]);

    // if (!vr::VRCompositor()) {
    //     cgLogError("cgHmdConfigureRendering: Could not initialize VR compositor\n");
    //     return CgStatus::Failure;
    // }
    // return CgStatus::Success;

    const ovrSizei resFovLeft = ovrHmd_GetFovTextureSize(hmd->hmd, ovrEye_Left,
            hmd->hmd->DefaultEyeFov[0], 0.84f);
    const ovrSizei resFovRight = ovrHmd_GetFovTextureSize(hmd->hmd, ovrEye_Right,
            hmd->hmd->DefaultEyeFov[1], 0.84f);

    hmd->renderTargetSize = glm::vec2(resFovLeft.w + resFovRight.w, resFovLeft.h);
    hmd->renderTargetSize = glm::vec2(1280, 800);
    cgLogInfo("cgHmdConfigureRendering: HMD framebuffer size: %d %d\n",
              hmd->renderTargetSize[0], hmd->renderTargetSize[1]);
    cgLogInfo("cgHmdConfigureRendering: HMD backbuffer size: %d %d\n",
              hmd->hmd->Resolution.w, hmd->hmd->Resolution.h);

    ovrGLConfig cfg;
    cfg.OGL.Header.API = ovrRenderAPI_OpenGL;
    cfg.OGL.Header.BackBufferSize = {hmd->hmd->Resolution.w, hmd->hmd->Resolution.h};
    cfg.OGL.Header.Multisample = 1;
#ifndef _WIN32
    cfg.OGL.Disp = glfwGetX11Display();
#endif // _WIN32
    if (!ovrHmd_ConfigureRendering(hmd->hmd, &cfg.Config, ovrDistortionCap_Chromatic | ovrDistortionCap_TimeWarp, hmd->hmd->DefaultEyeFov, hmd->eyeRenderDesc)) {
        cgLogError("cgHmdConfigureRendering: Could not initialize VR compositor\n");
        return CgStatus::Failure;
    }
    return CgStatus::Success;
}

CgStatus cgHmdCreateRenderTarget(CgHmd *hmd)
{
    // Save OpenGL state
    GLint prevDrawFramebufferBinding;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &prevDrawFramebufferBinding);
    GLint prevTextureBinding2D;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTextureBinding2D);

    // Create GL texture for main render target (RGBA texture)
    GLuint renderTarget;
    glGenTextures(1, &renderTarget);
    glBindTexture(GL_TEXTURE_2D, renderTarget);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, hmd->renderTargetSize[0],
                 hmd->renderTargetSize[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    // Create GL depth + stencil texture for HMD. Only needed when we want to
    // draw something that needs depth testing directly to the HMD framebuffer)
    GLuint depthTexture;
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, hmd->renderTargetSize[0],
                 hmd->renderTargetSize[1], 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);

    // Create GL framebuffer for HMD
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, renderTarget, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                           GL_TEXTURE_2D, depthTexture, 0);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        cgLogError("cgHmdCreateRenderTarget: Incomplete framebuffer\n");
        return CgStatus::Failure;
    }

    // Store additional information
    hmd->renderTarget = renderTarget;
    hmd->depthTexture = depthTexture;
    hmd->framebuffer = framebuffer;
    hmd->viewport[0] = glm::vec4(0, 0, hmd->renderTargetSize[0] / 2, hmd->renderTargetSize[1]);
    hmd->viewport[1] = glm::vec4(hmd->renderTargetSize[0] / 2, 0, hmd->renderTargetSize[0] / 2, hmd->renderTargetSize[1]);

    // Restore OpenGL state
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, prevDrawFramebufferBinding);
    glBindTexture(GL_TEXTURE_2D, prevTextureBinding2D);

    return CgStatus::Success;
}

void cgHmdBeginFrame(CgHmd *hmd)
{
	// // Begin HMD frame
	// vr::VRCompositor()->WaitGetPoses(&hmd->poses[0], vr::k_unMaxTrackedDeviceCount, nullptr, 0);

	// // Obtain pose matrices of tracked devices
    // for (unsigned di = 0; di < vr::k_unMaxTrackedDeviceCount; ++di) {
    //     vr::HmdMatrix34_t pose = hmd->poses[di].mDeviceToAbsoluteTracking; 
    //     // Convert OpenVR row matrix to OpenGL column matrix
    //     hmd->poseMatrix[di] = glm::mat4();
	//     for (unsigned i = 0; i < 3; ++i) {
	//     	for (unsigned j = 0; j < 4; ++j) {
	//     		hmd->poseMatrix[di][j][i] = pose.m[i][j];
	//     	}
	//     }
    // }

	// // Obtain projection matrices for left and right eye
	// vr::HmdMatrix44_t projectionMatrix[2];
	// projectionMatrix[0] = hmd->hmd->GetProjectionMatrix(vr::Eye_Left, 0.1f, 10.0f, vr::API_OpenGL);
    // projectionMatrix[1] = hmd->hmd->GetProjectionMatrix(vr::Eye_Right, 0.1f, 10.0f, vr::API_OpenGL);
	// hmd->projectionMatrix[0] = glm::transpose(*(glm::mat4 *)(&projectionMatrix[0].m[0][0]));
	// hmd->projectionMatrix[1] = glm::transpose(*(glm::mat4 *)(&projectionMatrix[1].m[0][0]));

    // // Compute view matrices for left and right eye
	// vr::HmdMatrix34_t matEyeLeft = hmd->hmd->GetEyeToHeadTransform(vr::Eye_Left);
	// vr::HmdMatrix34_t matEyeRight = hmd->hmd->GetEyeToHeadTransform(vr::Eye_Right);
	// hmd->viewMatrix[0] = glm::translate(glm::mat4(), glm::vec3(0.0315f, 0.0f, 0.0f)) *
    //                      glm::inverse(hmd->poseMatrix[vr::k_unTrackedDeviceIndex_Hmd]);
	// hmd->viewMatrix[1] = glm::translate(glm::mat4(), glm::vec3(-0.0315f, 0.0f, 0.0f)) *
    //                      glm::inverse(hmd->poseMatrix[vr::k_unTrackedDeviceIndex_Hmd]);

	// Begin HMD frame
    ovrFrameTiming hmdFrameTiming = ovrHmd_BeginFrame(hmd->hmd, 0);

    // Obtain pose matrices of tracked devices
    ovrTrackingState trackingState = ovrHmd_GetTrackingState(hmd->hmd, ovr_GetTimeInSeconds());
    if (trackingState.StatusFlags & (ovrStatus_OrientationTracked | ovrStatus_PositionTracked)) {

        hmd->eyeRenderDesc[0] = ovrHmd_GetRenderDesc(hmd->hmd, ovrEye_Left, hmd->hmd->DefaultEyeFov[0]);
        hmd->eyeRenderDesc[1] = ovrHmd_GetRenderDesc(hmd->hmd, ovrEye_Right, hmd->hmd->DefaultEyeFov[1]);
        ovrVector3f hmdToEyeViewOffset[2];
        hmdToEyeViewOffset[0] = hmd->eyeRenderDesc[0].HmdToEyeViewOffset;
        hmdToEyeViewOffset[1] = hmd->eyeRenderDesc[1].HmdToEyeViewOffset;

        //ovrMatrix4f twMatrix[2];
        //ovrHmd_GetEyeTimewarpMatricesDebug(hmd->hmd, ovrEye_Left, hmd->poses[0], &twMatrix[0], 16.7e-3);
        //glm::vec4 tp = glm::transpose(*(glm::mat4 *)(&twMatrix[0].M[0][0])) * glm::vec4(1.0, 0.0, 0.0, 1.0);
        //cgLogInfo("%f %f %f %f\n", tp[0], tp[1], tp[2], tp[3]);
        
        // Convert OVR pose to OpenGL column matrix
        ovrHmd_GetEyePoses(hmd->hmd, 0, hmdToEyeViewOffset, hmd->poses, nullptr);
        glm::vec3 p0 = *(glm::vec3 *)(&hmd->poses[0].Position.x);
        glm::vec3 p1 = *(glm::vec3 *)(&hmd->poses[1].Position.x);
        glm::quat q0 = *(glm::quat *)(&hmd->poses[0].Orientation.x);
        hmd->poseMatrix[0] = glm::translate(glm::mat4(), 0.5f * (p0 + p1)) * glm::mat4_cast(q0);

        // Store new tracking state
        hmd->trackingState = trackingState;
    }

    // Obtain pose matrices for Hydras
    size_t controllerCount = sixenseGetNumActiveControllers();
    if (controllerCount) {
        sixenseGetNewestData(0, &s_controllers[0]);  // No idea which hand
        sixenseGetNewestData(1, &s_controllers[1]);  // is which...

        hmd->poseMatrix[1] =
            glm::translate(glm::mat4(), glm::vec3(1e-3f, 1e-3f, 1e-3f) * *reinterpret_cast<glm::vec3 *>(&s_controllers[0].pos[0])) *
                glm::mat4(*reinterpret_cast<glm::mat3 *>(&s_controllers[0].rot_mat[0][0]));
        hmd->poseMatrix[2] =
            glm::translate(glm::mat4(), glm::vec3(1e-3f, 1e-3f, 1e-3f) * *reinterpret_cast<glm::vec3 *>(&s_controllers[1].pos[0])) *
                glm::mat4(*reinterpret_cast<glm::mat3 *>(&s_controllers[1].rot_mat[0][0]));
    }

	// // Obtain projection matrices for left and right eye
    ovrMatrix4f projectionMatrix[2];
    projectionMatrix[0] = ovrMatrix4f_Projection(hmd->eyeRenderDesc[0].Fov, 0.1f, 10.0f, true);
    projectionMatrix[1] = ovrMatrix4f_Projection(hmd->eyeRenderDesc[1].Fov, 0.1f, 10.0f, true);
	hmd->projectionMatrix[0] = glm::transpose(*(glm::mat4 *)(&projectionMatrix[0].M[0][0]));
	hmd->projectionMatrix[1] = glm::transpose(*(glm::mat4 *)(&projectionMatrix[1].M[0][0]));

    // // Compute view matrices for left and right eye
	hmd->viewMatrix[0] = glm::translate(glm::mat4(), glm::vec3(0.0315f, 0.0f, 0.0f)) *
                         glm::inverse(hmd->poseMatrix[0]);
	hmd->viewMatrix[1] = glm::translate(glm::mat4(), glm::vec3(-0.0315f, 0.0f, 0.0f)) *
                         glm::inverse(hmd->poseMatrix[0]);
}

void cgHmdEndFrame(CgHmd *hmd)
{
	// // Create descriptors for render targets
	// vr::Texture_t eye_texture[2];
	// vr::VRTextureBounds_t eye_bounds[2];
	// eye_texture[0] = { (void *)(std::uintptr_t)hmd->renderTarget, vr::API_OpenGL, vr::ColorSpace_Gamma };
	// eye_bounds[0] = { 0.0f, 0.0f, 0.5f, 1.0f };
	// eye_texture[1] = { (void *)(std::uintptr_t)hmd->renderTarget, vr::API_OpenGL, vr::ColorSpace_Gamma };
	// eye_bounds[1] = { 0.5f, 0.0f, 1.0f, 1.0f };

	// // End HMD frame (present to display)
	// vr::VRCompositor()->Submit(vr::Eye_Left, &eye_texture[0], &eye_bounds[0]);
	// vr::VRCompositor()->Submit(vr::Eye_Right, &eye_texture[1], &eye_bounds[1]);

	// vr::VRCompositor()->PostPresentHandoff();

    // Dismiss health-safety warning message
    ovrHmd_DismissHSWDisplay(hmd->hmd);

    // Create descriptors for render targets
    ovrGLTexture eye_texture[2];
    glm::ivec2 res = hmd->renderTargetSize;
    eye_texture[0].OGL.Header.API = ovrRenderAPI_OpenGL;
    eye_texture[0].OGL.Header.TextureSize = {res[0], res[1]};
    eye_texture[0].OGL.Header.RenderViewport = {{0, 0}, {res[0] / 2, res[1]}};
    eye_texture[0].OGL.TexId = hmd->renderTarget;
    eye_texture[1] = eye_texture[0];
    eye_texture[1].OGL.Header.RenderViewport = {{res[0] / 2, 0}, {res[0] / 2, res[1]}};

    // End HMD frame (present to display)
    ovrHmd_EndFrame(hmd->hmd, hmd->poses, (ovrTexture *)&eye_texture[0]);
}

glm::vec3 cgHmdAngularVelocity(CgHmd *hmd)
{
    return *(glm::vec3 *)(&hmd->trackingState.HeadPose.AngularVelocity.x);
}

void cgHmdProcessEvents(CgHmd *hmd, void *userdata)
{
    vr::VREvent_t event;

    // Generate fake OpenVR VREvent:s from Sixense device events. The mapping
    // is similar to the one in the official OpenVR driver for Razer Hydra.
    for (int ci = 0; ci < 2; ++ci) {
        event.trackedDeviceIndex = ci + 1;  // 1=Left controller; 2=Right controller

        int diff = s_controllers[ci].buttons ^ s_buttons[ci];
        // Emulate system button
        if (diff & SIXENSE_BUTTON_START) {
            if (s_controllers[ci].buttons & SIXENSE_BUTTON_START) {
                event.eventType = vr::VREvent_ButtonPress;
            }
            else {
                event.eventType = vr::VREvent_ButtonUnpress;
            }
            event.data.controller.button = vr::k_EButton_System;
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }
        // Emulate menu button
        if (diff & SIXENSE_BUTTON_4) {
            if (s_controllers[ci].buttons & SIXENSE_BUTTON_4) {
                event.eventType = vr::VREvent_ButtonPress;
            }
            else {
                event.eventType = vr::VREvent_ButtonUnpress;
            }
            event.data.controller.button = vr::k_EButton_ApplicationMenu;
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }
        // Emulate grip button
        if (diff & SIXENSE_BUTTON_BUMPER) {
            if (s_controllers[ci].buttons & SIXENSE_BUTTON_BUMPER) {
                event.eventType = vr::VREvent_ButtonPress;
            }
            else {
                event.eventType = vr::VREvent_ButtonUnpress;
            }
            event.data.controller.button = vr::k_EButton_Grip;
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }
        // Emulate D-pad (touchpad emulated)
        if (diff & SIXENSE_BUTTON_JOYSTICK) {
            if (s_controllers[ci].buttons & SIXENSE_BUTTON_JOYSTICK) {
                event.eventType = vr::VREvent_ButtonPress;
                float x = s_controllers[ci].joystick_x;
                float y = s_controllers[ci].joystick_y;
                if ((x < 0.0f) && (std::abs(y) <= std::abs(x))) {
                    event.data.controller.button = vr::k_EButton_DPad_Left;
                    cg::cgLogInfo("D-pad Left\n");
                }
                else if ((y >= 0.0f) && (std::abs(x) <= std::abs(y))) {
                    event.data.controller.button = vr::k_EButton_DPad_Up;
                    cg::cgLogInfo("D-pad Up\n");
                }
                else if ((x >= 0.0f) && (std::abs(y) <= std::abs(x))) {
                    event.data.controller.button = vr::k_EButton_DPad_Right;
                    cg::cgLogInfo("D-pad Right\n");
                }
                else if ((y < 0.0f) && (std::abs(x) <= std::abs(y))) {
                    event.data.controller.button = vr::k_EButton_DPad_Down;
                    cg::cgLogInfo("D-pad Down\n");
                }
                // Store D-pad direction for unpress event
                s_dpad[ci] = event.data.controller.button;
            }
            else {
                event.eventType = vr::VREvent_ButtonUnpress;
                event.data.controller.button = s_dpad[ci];
            }
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }
        // Emulate touchpad button
        if (diff & SIXENSE_BUTTON_JOYSTICK) {
            if (s_controllers[ci].buttons & SIXENSE_BUTTON_JOYSTICK) {
                event.eventType = vr::VREvent_ButtonPress;
            }
            else {
                event.eventType = vr::VREvent_ButtonUnpress;
            }
            event.data.controller.button = vr::k_EButton_SteamVR_Touchpad;
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }
        // Emulate analog trigger button
        if (s_triggers[ci] < 0.5f && s_controllers[ci].trigger >= 0.5f) {
            event.eventType = vr::VREvent_ButtonPress;
            event.data.controller.button = vr::k_EButton_SteamVR_Trigger;
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }
        if (s_triggers[ci] >= 0.5f && s_controllers[ci].trigger < 0.5f) {
            event.eventType = vr::VREvent_ButtonUnpress;
            event.data.controller.button = vr::k_EButton_SteamVR_Trigger;
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
        }

        // Store current button/trigger state for future event detection
        s_buttons[ci] = s_controllers[ci].buttons;
        s_triggers[ci] = s_controllers[ci].trigger;
    }
}

void cgHmdLoadRenderModel(CgHmd *hmd, CgHmdRenderModel *dstRenderModel, const int index)
{
    // // Obtain render model for device index
    // char str[80] = { 0 };
    // hmd->hmd->GetStringTrackedDeviceProperty(index, vr::Prop_RenderModelName_String, str, 80, nullptr);
    char str[] = "/home/fredrik/.steam/steam/SteamApps/common/SteamVR/resources/rendermodels/vr_controller_vive_1_5/vr_controller_vive_1_5.obj";  // Use known model name
    vr::RenderModel_t *srcRenderModel = nullptr;
    while (vr::VRRenderModels()->LoadRenderModel_Async(str, &srcRenderModel) == vr::VRRenderModelError_Loading) {}

    // Obtain render model texture map
    vr::RenderModel_TextureMap_t *srcRenderModelTexture = nullptr;
    while (vr::VRRenderModels()->LoadTexture_Async(srcRenderModel->diffuseTextureId, &srcRenderModelTexture) == vr::VRRenderModelError_Loading) {}

    // Save OpenGL state
    GLint prevVertexArrayBinding;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVertexArrayBinding);
    GLint prevTextureBinding2D;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTextureBinding2D);

    // Create VAO for drawing model mesh
    glGenVertexArrays(1, &dstRenderModel->vao);
    glBindVertexArray(dstRenderModel->vao);

    // Create GL buffer for model mesh vertices (stored in interleaved format)
    glGenBuffers(1, &dstRenderModel->vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, dstRenderModel->vertexBuffer);
	size_t verticesNBytes = srcRenderModel->unVertexCount * sizeof(srcRenderModel->rVertexData[0]);
    size_t vertexStride = sizeof(srcRenderModel->rVertexData[0]);
	glBufferData(GL_ARRAY_BUFFER, verticesNBytes, srcRenderModel->rVertexData, GL_STATIC_DRAW);
    glEnableVertexAttribArray(CG_POSITION);
    glVertexAttribPointer(CG_POSITION, 3, GL_FLOAT, GL_FALSE, vertexStride, 0);
    glEnableVertexAttribArray(CG_NORMAL);
    glVertexAttribPointer(CG_NORMAL, 3, GL_FLOAT, GL_FALSE, vertexStride, (void*)12);
    glEnableVertexAttribArray(CG_TEXCOORD0);
    glVertexAttribPointer(CG_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, vertexStride, (void*)24);

    // Create GL buffer for model mesh indices
    glGenBuffers(1, &dstRenderModel->indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dstRenderModel->indexBuffer);
	size_t indicesNBytes = (srcRenderModel->unTriangleCount * 3) * sizeof(srcRenderModel->rIndexData[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesNBytes, srcRenderModel->rIndexData, GL_STATIC_DRAW);

    // Create GL texture for model texture map
    glGenTextures(1, &dstRenderModel->texture);
    glBindTexture(GL_TEXTURE_2D, dstRenderModel->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
                 srcRenderModelTexture->unWidth,
                 srcRenderModelTexture->unHeight,
                 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 srcRenderModelTexture->rubTextureMapData);

    // Store additional information
    dstRenderModel->numVertices = srcRenderModel->unVertexCount;
    dstRenderModel->numIndices = srcRenderModel->unTriangleCount * 3;

    // Restore OpenGL state
    glBindVertexArray(prevVertexArrayBinding);
    glBindTexture(GL_TEXTURE_2D, prevTextureBinding2D);

    // Delete render model data
    vr::VRRenderModels()->FreeTexture(srcRenderModelTexture);
    vr::VRRenderModels()->FreeRenderModel(srcRenderModel);
}

} // namespace cg

#endif // CG_WITH_OVR_SDK
