#version 450

#define SPLAT_MODE_SQUARE 0
#define SPLAT_MODE_ROUND 1
#define SPLAT_MODE_DISK 2

layout(binding = 4, std140) uniform SplatUniforms {
    ivec2 resolution;
    float splat_size;
    int splat_mode;
    int show_clipmap_lod;
    int show_normals;
} u_splat;

flat in vec4 v_color;
flat in vec3 v_disk_normal;
flat in float v_seed;

layout(location = 0) out vec4 rt_color;

// Anti-aliased step function from "OpenGL Insights", Chapter 7
float aastep(float threshold, float value)
{
    float afwidth = 0.7 * length(vec2(dFdx(value), dFdy(value)));
    return smoothstep(threshold - afwidth, threshold + afwidth, value);
}

// Simplified adaption of Inigo Quilez' original alpha-to-coverage function,
// modified to take only alpha and a single spatial seed as input.
int a2c(float alpha, float seed)
{
    const int MSAA = 4;

    int mask = (1 << int(alpha * MSAA + 0.5)) - 1;
    float hash = fract(sin(seed) * 43758.5453);
    int shift = int(hash * MSAA);

    return ((mask << shift) | (mask >> (MSAA - shift))) & ((1 << MSAA) - 1);
}

void main()
{
    vec4 color = v_color;
    //color.a += 0.125;  // Add small alpha bias to reduce holes
    if (u_splat.splat_mode == SPLAT_MODE_SQUARE) {
        color.a *= 1.0;
    }
    if (u_splat.splat_mode == SPLAT_MODE_ROUND) {
        float r3 = length(gl_PointCoord.xy - 0.5) * 2.0;
        color.a *= 1.0 - aastep(1.0, r3);
    }
    if (u_splat.splat_mode == SPLAT_MODE_DISK) {
        vec2 xy = (gl_PointCoord.xy * 2.0 - 1.0) * vec2(1.0, -1.0);
        float dz = dot(-v_disk_normal.xy / v_disk_normal.z, xy);
        float r3 = length(vec3(xy, dz));
        color.a *= 1.0 - aastep(1.0, r3);
    }
    if (color.a < 0.01) { discard; }

    rt_color = clamp(color, 0.0, 1.0);
    gl_SampleMask[0] = a2c(color.a, v_seed);
}
