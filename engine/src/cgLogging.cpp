/// @file    cgLogging.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgLogging.h"

#include <cassert>
#include <cstdio>
#include <cstdarg>
#include <mutex>
#include <thread>

namespace cg {

static CgLogLevel s_level = CG_NOT_SET;  // Current log level

static std::FILE *s_stream = stderr;  // Current log stream

static std::mutex s_mutex;  // Mutex for locking

void cgSetLoggingLevel(const CgLogLevel level)
{
    std::lock_guard<std::mutex> lock(s_mutex);

    s_level = level;
}

void cgSetLogFilename(const char *filename)
{
    std::lock_guard<std::mutex> lock(s_mutex);

    // Open file or revert output to stdeer
    if (s_stream != stderr) {
        std::fclose(s_stream);
        s_stream = stderr;
    }
    std::FILE *stream = std::fopen(filename, "a");
    if (stream) {
        s_stream = stream;
    }
}

// Function that all cgLog* calls are forwarded to
static void vlog(const CgLogLevel &level, const char *format, va_list args)
{
    std::lock_guard<std::mutex> lock(s_mutex);

    // Suppress logging if level is below threshold (or is not set)
    if (level < s_level || s_level == CG_NOT_SET) { return; }

    // Write log header
    if (level <= CG_DEBUG) {
        std::fprintf(s_stream, "CG:DEBUG:");
    }
    else if (level <= CG_INFO) {
        std::fprintf(s_stream, "CG:INFO:");
    }
    else if (level <= CG_WARNING) {
        std::fprintf(s_stream, "CG:WARNING:");
    }
    else if (level <= CG_ERROR) {
        std::fprintf(s_stream, "CG:ERROR:");
    }
    else if (level <= CG_CRITICAL) {
        std::fprintf(s_stream, "CG:CRITICAL:");
    }

    // Append thread-id
    std::thread::id tid = std::this_thread::get_id();
    if (sizeof(tid) >= sizeof(std::uint64_t)) {
        std::fprintf(s_stream, "[%lu]:", *reinterpret_cast<std::uint64_t *>(&tid));
    } else {
        std::fprintf(s_stream, "[%u]:", *reinterpret_cast<std::uint32_t *>(&tid));
    }

    // Append log message
    std::vfprintf(s_stream, format, args);
}

void cgLog(CgLogLevel level, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vlog(level, format, args);
    va_end(args);
}

void cgLogDebug(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vlog(CG_DEBUG, format, args);
    va_end(args);
}

void cgLogInfo(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vlog(CG_INFO, format, args);
    va_end(args);
}

void cgLogWarning(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vlog(CG_WARNING, format, args);
    va_end(args);
}

void cgLogError(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vlog(CG_ERROR, format, args);
    va_end(args);
}

void cgLogCritical(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vlog(CG_CRITICAL, format, args);
    va_end(args);
}

} // namespace cg
