#version 450

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 0) uniform samplerBuffer u_vertices;

layout(location = 0) out vec3 v_view_pos;

void main()
{
    vec3 local_pos = texelFetch(u_vertices, gl_VertexID).rgb;

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));

    v_view_pos = view_pos.xyz;
    gl_Position = u_scene.projection * view_pos;
}
