/// @file    gl_vr.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2018 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include <GL/glew.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <openvr.h>

// Forward declarations
struct CgHmd;
struct CgHmdRenderModel;

typedef void (*CgHmdButtonCallback_t)(CgHmd *, vr::VREvent_t *, void *);

struct CgHmd {
    vr::IVRSystem *hmd;
    vr::TrackedDevicePose_t poses[vr::k_unMaxTrackedDeviceCount];
    glm::uvec2 renderTargetSize;  // Recommended render target size
    GLuint renderTarget;          // Main render target texture (RGBA texture)
    GLuint depthTexture;          // Depth texture for when we want to draw directly
                                  // to the HMD framebuffer (for debugging, etc.)
    GLuint framebuffer;           // HMD framebuffer that we can render or blit to
    glm::mat4 poseMatrix[vr::k_unMaxTrackedDeviceCount];
    glm::mat4 projectionMatrix[2];
    glm::mat4 viewMatrix[2];
    glm::vec4 viewport[2];
	int eye;
    CgHmdButtonCallback_t buttonCallback = nullptr;
};

struct CgHmdRenderModel {
    GLuint vao;
    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint texture;
    int numVertices;
    int numIndices;
};

bool cgHmdInit(CgHmd *hmd);

bool cgHmdShutdown(CgHmd *hmd);

bool cgHmdConfigureRendering(CgHmd *hmd);

bool cgHmdCreateRenderTarget(CgHmd *hmd);

void cgHmdBeginFrame(CgHmd *hmd);

void cgHmdEndFrame(CgHmd *hmd);

void cgHmdProcessEvents(CgHmd *hmd, void *userdata);

void cgHmdLoadRenderModel(CgHmd *hmd, CgHmdRenderModel *renderModel, int index=0);


// Example of a callback for button events:
//
// void controllerButtonCallback(cg::CgHmd *hmd, vr::VREvent_t *event, void *userdata)
// {
//     Context *ctx = static_cast<Context *>(userdata);
//
//     if (event->data.controller.button == vr::k_EButton_SteamVR_Trigger) {
//         if (event->eventType == vr::VREvent_ButtonPress) {
//             ctx->grip_state = true;
//         }
//         else if (event->eventType == vr::VREvent_ButtonUnpress) {
//             ctx->grip_state = false;
//         }
//     }
// }
