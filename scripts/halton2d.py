import numpy
import matplotlib.pyplot as plt

def halton(index, base):
    i = index
    b = base
    f = 1.0
    r = 0.0
    while i > 0:
        f = f / b
        r = r + f * (i % b)
        i = int(i / b)
    return r

n = 4096
x = [halton(i, 2) for i in range(1, n + 1)]
y = [halton(i, 3) for i in range(1, n + 1)]
for v in zip(x, y):
    print("    %.5ff, %.5ff," % v)

plt.figure()
plt.plot(x[0:10], y[0:10], 'or')
plt.plot(x[10:100], y[10:100], 'ob')
plt.plot(x[100:256], y[100:256], 'og')
plt.axis('square')
plt.show()
