/// @file    04_imgui.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgTypes.h"
#include "cgLogging.h"
#include "cgGL.h"
#include "cgRenderer.h"

#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

#include <cstdlib>
#include <iostream>

// Struct for application context
struct Context {
    int width;
    int height;
    float aspect;
    GLFWwindow *window;
    GLuint defaultVAO;
    glm::vec4 clearColor;
    float elapsedTime;
};

void init(Context &ctx)
{
    // Initialize default GL pipeline state setters
    cg::cgInitRenderStateSetters();
}

void update(Context &ctx)
{
    // Create some UI items
    ImGui::SliderFloat3("Background color", &ctx.clearColor[0], 0.0f, 1.0f);
    ImGui::Text("Framerate: %.0lf", ImGui::GetIO().Framerate);
}

void render(Context &ctx)
{
    // Create pipeline state
    cg::CgRenderStateInfo pipelineStateInfo;
    pipelineStateInfo[GL_VIEWPORT] = glm::ivec4(0, 0, ctx.width, ctx.height);

    // Clear buffers
    glClearBufferfv(GL_COLOR, 0, &ctx.clearColor[0]);
    glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

    // Render
    cg::cgRendererDraw(cg::CgCamera(), nullptr, 0, ctx.elapsedTime, &pipelineStateInfo);
}

void errorCallback(int error, const char* description)
{
    std::cerr << description << std::endl;
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    // Forward events to ImGui's event handler
    ImGui_ImplGlfwGL3_MouseButtonCallback(window, button, action, mods);
    // Skip further event handling if cursor is over UI
    if (ImGui::GetIO().WantCaptureMouse) { return; }
}

void resizeCallback(GLFWwindow* window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->width = width;
    ctx->height = height;
    ctx->aspect = float(width) / float(height);
}

int main(void)
{
    // Set logging level
    cg::cgSetLoggingLevel(cg::CG_INFO);

    // Create application context
    Context ctx;
    ctx.clearColor = glm::vec4(0.2f, 0.2f, 0.2f, 0.0f);

    // Create GLFW window and GL context
    glfwSetErrorCallback(errorCallback);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    ctx.width = 512;
    ctx.height = 512;
    ctx.aspect = float(ctx.width) / float(ctx.height);
    ctx.window = glfwCreateWindow(ctx.width, ctx.height, "example 04 - imgui", nullptr, nullptr);
    if (!ctx.window) {
        std::exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(ctx.window);
    glfwSetWindowUserPointer(ctx.window, &ctx);
    glfwSetMouseButtonCallback(ctx.window, mouseButtonCallback);
    glfwSetFramebufferSizeCallback(ctx.window, resizeCallback);

    // Load GL API functions
    if (cg::cgLoadGL() != cg::CgStatus::Success) {
        std::exit(EXIT_FAILURE);
    }
    cg::cgLogInfo("GL version: %s\n", glGetString(GL_VERSION));

    // Initialize ImGui
    ImGui_ImplGlfwGL3_Init(ctx.window, false /*do not install callbacks*/);

    // Create default VAO and set initial pipeline state
    glGenVertexArrays(1, &ctx.defaultVAO);
    glBindVertexArray(ctx.defaultVAO);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    // Initialize application
    init(ctx);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window)) {
        glfwPollEvents();
        ctx.elapsedTime = glfwGetTime();
        ImGui_ImplGlfwGL3_NewFrame();
        update(ctx);
        render(ctx);
        ImGui::Render();
        glfwSwapBuffers(ctx.window);
    }

    // Shutdown
    glfwDestroyWindow(ctx.window);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
