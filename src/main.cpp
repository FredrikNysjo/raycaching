/// @file    main.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2017 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///
/// @section ISSUES
///
/// - Issue #1: Broken DSA functions on Windows (closed)

#include "gl_utils.h"
#include "gl_state.h"
#include "raycaching.h"
#include "vtk_utils.h"
#include "marching_cubes.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <thread>
#include <chrono>

#define USE_Z_PREPASS 1

struct VolumeTransform {
    glm::vec3 translation;
    glm::vec3 scaling;

    VolumeTransform() { translation = glm::vec3(0.0f); scaling = glm::vec3(1.0f); }
};

struct SceneUniforms {
    glm::mat4 view;
    glm::mat4 projection;
    float time;
    float pad[3];
    HemisphereLight hemisphere;
    Light light;

    SceneUniforms() { time = 0.0f; }
};

struct DrawableUniforms {
    glm::mat4 model;
    glm::mat4 normal;
    Material material;
};

struct Mesh {
    std::vector<glm::vec3> vertices;
    std::vector<int> indices;
};

struct Camera {
    glm::mat4 projectionMatrix;
    glm::mat4 viewMatrix;
    glm::vec4 viewportNDC;

    Camera() { viewportNDC = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f); }
};

struct Window {
    int width;
    int height;
    float aspect;
    GLFWwindow *id;

    Window() { width = 1000; height = 700; aspect = 1000.0f / 700.0f; id = nullptr; }
};

struct Scene {
    glm::vec4 background_color[2];
    float elapsed_time;

    Scene() { background_color[0] = glm::vec4(1.0f), background_color[1] = glm::vec4(0.0f); elapsed_time = 0.0f; }
};

struct Context {
    Window window;
    Scene scene;

    std::unordered_map<std::string, GLuint> programs;
    std::unordered_map<std::string, GLuint> vaos;
    std::unordered_map<std::string, GLuint> buffers;
    std::unordered_map<std::string, GLuint> textures;
    std::unordered_map<std::string, GLuint> framebuffers;
    std::unordered_map<std::string, GLPipelineStateInfo> pipelines;
    std::unordered_map<std::string, GLTimerQuery> timers;
    GLTimerQuery splat_count;

    std::array<Camera, 2> camera;
    cg::CgTrackball trackball;

    SceneUniforms scene_uniforms;
    DrawableUniforms drawable_uniforms;
    cg::VolumeInt16 volume;
    VolumeUniforms volume_uniforms;
    VolumeTransform volume_transform;
    RaycastUniforms raycast_uniforms;
    ClipmapUniforms clipmap_uniforms;
    BrickCacheInfo splat_buffer_info;
    SplatUniforms splat_uniforms;
    Mesh mesh;

    bool use_raycaching = true;
    bool use_parallel_prefix_sum = true;
    bool prediction_enabled = true;
    int num_rays_per_frame = 8000;
    bool needs_recentering = true;
    bool show_isosurface = true;
    bool show_clipmap_grid = false;
    bool show_mesh = false;
    std::string input_filename = "";
    float input_scale = 4.0f;
    bool use_experimental = false;
    bool show_background = false;
    bool show_importance = false;
    bool step_frame = false;

    glm::vec4 viewport[2];
    glm::mat4 view_matrix[2];
    glm::mat4 projection_matrix[2];

    Context()
    {
        view_matrix[0] = glm::translate(glm::mat4(), glm::vec3(-0.032f, 0.0f, 0.0f));
        view_matrix[1] = glm::translate(glm::mat4(), glm::vec3(0.032f, 0.0f, 0.0f));
    }
};

std::string shaderDir(void)
{
    std::string root_dir = cg::cgGetEnvSafe("CG_ENGINE_ROOT");
    if (root_dir.empty()) {
        root_dir = "..";
    }
    return root_dir + "/src/shaders/";
}

void createFramebuffer(GLuint *framebuffer, GLuint *color_texture,
                       GLuint *depth_texture, int width, int height)
{
    glDeleteTextures(1, color_texture);
    glCreateTextures(GL_TEXTURE_2D_MULTISAMPLE, 1, color_texture);
    glTextureStorage2DMultisample(*color_texture, 4, GL_RGBA16F, width, height, GL_TRUE);

    glDeleteTextures(1, depth_texture);
    glCreateTextures(GL_TEXTURE_2D_MULTISAMPLE, 1, depth_texture);
    glTextureStorage2DMultisample(*depth_texture, 4, GL_DEPTH24_STENCIL8, width, height, GL_TRUE);

    glDeleteFramebuffers(1, framebuffer);
    glCreateFramebuffers(1, framebuffer);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT0, *color_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_DEPTH_STENCIL_ATTACHMENT, *depth_texture, 0);
    GLenum status = glCheckNamedFramebufferStatus(*framebuffer, GL_DRAW_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "Error: Incomplete framebuffer\n";
    }
}

void createVolumeTexture(GLuint *texture, const cg::VolumeInt16 &volume)
{
    int width = volume.base.dimensions.x;
    int height = volume.base.dimensions.y;
    int depth = volume.base.dimensions.z;

    // Set border color to minimum Hounsfield value
    glm::vec4 border_color = glm::vec4(-1024.0f / 32767.5f);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_3D, 1, texture);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
    glTextureParameterfv(*texture, GL_TEXTURE_BORDER_COLOR, &border_color[0]);
    glTextureParameteri(*texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(*texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTextureStorage3D(*texture, 1, GL_R16_SNORM, width, height, depth);
    glTextureSubImage3D(*texture, 0, 0, 0, 0, width, height, depth, GL_RED,
                        GL_SHORT, &volume.base.data[0]);
}

void createUniformBuffer(GLuint *buffer, int num_bytes=65536)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_DYNAMIC_DRAW);
}

void createDummyVolume(cg::VolumeInt16 &volume)
{
    volume.base.dimensions = glm::ivec3(256);
    volume.base.spacing = glm::vec3(1.0f);
    volume.base.data.resize(256 * 256 * 256 * sizeof(std::int16_t));

    std::int16_t *ptr = (std::int16_t *)&volume.base.data[0];
    for (int z = 0; z < 256; ++z) {
        for (int y = 0; y < 256; ++y) {
            for (int x = 0; x < 256; ++x) {
                glm::vec3 p = glm::mod(glm::vec3(x, y, z) + 0.5f, glm::vec3(64.0f)) - 32.0f;
                float d = glm::length(p);
                //float d = std::max(std::abs(p.x), std::max(std::abs(p.y), std::abs(p.z)));
                ptr[256 * (256 * z + y) + x] = 400.0f * glm::smoothstep(17.5f, 14.5f, d);
            }
        }
    }
}

void createMarchingCubesBuffer(GLuint *vertex_buffer, GLuint *texture,
                               GLuint *index_buffer, const Mesh &mesh)
{
    glDeleteBuffers(1, vertex_buffer);
    glCreateBuffers(1, vertex_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *vertex_buffer);  // Workaround for DSA issue
    size_t num_vertex_bytes = mesh.vertices.size() * sizeof(mesh.vertices[0]);
    glNamedBufferData(*vertex_buffer, num_vertex_bytes, &mesh.vertices[0], GL_STATIC_DRAW);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RGB32F, *vertex_buffer);

    glDeleteBuffers(1, index_buffer);
    glCreateBuffers(1, index_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *index_buffer);  // Workaround for DSA issue
    size_t num_index_bytes = mesh.indices.size() * sizeof(mesh.indices[0]);
    glNamedBufferData(*index_buffer, num_index_bytes, &mesh.indices[0], GL_STATIC_DRAW);
}

void setupGLPrograms(Context &ctx)
{
    for (auto &it : ctx.programs) { glDeleteProgram(it.second); }
    ctx.programs.clear();

    ctx.programs["draw_raycasting"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "draw_raycasting.vert", GL_VERTEX_SHADER),
         cg::cgLoadShader(shaderDir() + "draw_raycasting.frag", GL_FRAGMENT_SHADER)});
    ctx.programs["point_raycast2"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "raycast_cached3.vert", GL_VERTEX_SHADER)});
    ctx.programs["copy_result"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "copy_kernel.vert", GL_VERTEX_SHADER)});
    ctx.programs["sort"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "sort.vert", GL_VERTEX_SHADER)});
    ctx.programs["draw_splats"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "draw_splats.vert", GL_VERTEX_SHADER),
         cg::cgLoadShader(shaderDir() + "draw_splats.frag", GL_FRAGMENT_SHADER)});
    ctx.programs["draw_mesh"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "draw_mesh.vert", GL_VERTEX_SHADER),
         cg::cgLoadShader(shaderDir() + "draw_mesh.frag", GL_FRAGMENT_SHADER)});
    ctx.programs["draw_mesh_depth"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "draw_mesh.vert", GL_VERTEX_SHADER)});
    ctx.programs["draw_clipmap"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "draw_clipmap.vert", GL_VERTEX_SHADER),
         cg::cgLoadShader(shaderDir() + "draw_clipmap.geom", GL_GEOMETRY_SHADER),
         cg::cgLoadShader(shaderDir() + "draw_clipmap.frag", GL_FRAGMENT_SHADER)});
    ctx.programs["composite"] = cg::cgCreateShaderProgram(
        {cg::cgLoadShader(shaderDir() + "composite.vert", GL_VERTEX_SHADER),
         cg::cgLoadShader(shaderDir() + "composite.frag", GL_FRAGMENT_SHADER)});
}

void setupGLPipelines(Context &ctx)
{
    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_raycasting"]);
    glEnable(GL_DEPTH_TEST);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_raycasting"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["point_raycast2"]);
    glEnable(GL_RASTERIZER_DISCARD);
    glCaptureCurrentPipelineState(&ctx.pipelines["raycast"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["copy_result"]);
    glEnable(GL_RASTERIZER_DISCARD);
    glCaptureCurrentPipelineState(&ctx.pipelines["copy_result"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["sort"]);
    glEnable(GL_RASTERIZER_DISCARD);
    glCaptureCurrentPipelineState(&ctx.pipelines["sort"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_splats"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
    glCaptureCurrentPipelineState(&ctx.pipelines["splat"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_splats"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDisable(GL_MULTISAMPLE);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,
                        GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glCaptureCurrentPipelineState(&ctx.pipelines["splat_transparent"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_mesh"]);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
#if USE_Z_PREPASS
    glDepthMask(GL_FALSE);
    glDepthFunc(GL_EQUAL);
#endif
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_mesh"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_mesh_depth"]);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_mesh_depth"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["composite"]);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["composite"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_clipmap"]);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_clipmap"]);

    glClearCurrentPipelineState();  // Just in case...
}

void setupGLFramebuffers(Context &ctx)
{
    createFramebuffer(&ctx.framebuffers["forward"], &ctx.textures["color"],
                      &ctx.textures["depth"], ctx.window.width, ctx.window.height);
}

void setupGLBuffers(Context &ctx)
{
    createClipmapBuffer(&ctx.buffers["clipmap"], &ctx.textures["clipmap"], ctx.clipmap_uniforms.dimensions);
    createBrickCache(&ctx.buffers["splats"], &ctx.textures["splats"], ctx.splat_buffer_info);
    createIndexBuffer(&ctx.buffers["index"], &ctx.textures["index"], ctx.splat_buffer_info);
    createResultBuffer(&ctx.buffers["result"], ctx.splat_buffer_info);
    createTimestampBuffer(&ctx.buffers["timestamp"], ctx.splat_buffer_info);
    createTileBuffer(&ctx.buffers["tile"], ctx.splat_buffer_info);
    createHaltonTexture(&ctx.buffers["halton3d"], &ctx.textures["halton3d"]);

    createUniformBuffer(&ctx.buffers["scene_uniforms"]);
    createUniformBuffer(&ctx.buffers["drawable_uniforms"]);
    createUniformBuffer(&ctx.buffers["splat_uniforms"]);
    createUniformBuffer(&ctx.buffers["raycast_uniforms"]);
    createUniformBuffer(&ctx.buffers["volume_uniforms"]);
    createUniformBuffer(&ctx.buffers["clipmap_uniforms"]);
}

void setupGLQueries(Context &ctx)
{
    for (auto &it : ctx.timers) { glDeleteQueries(1, &it.second.query); }
    ctx.timers.clear();

    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["raycast"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["sort"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["splat"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["other"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["resolve"].query);

    glDeleteQueries(1, &ctx.splat_count.query);
    glCreateQueries(GL_PRIMITIVES_GENERATED, 1, &ctx.splat_count.query);
}

void setupTrackball(Context &ctx)
{
    ctx.trackball.radius = double(std::min(ctx.window.width, ctx.window.height)) / 2.0;
    ctx.trackball.center = glm::vec2(ctx.window.width, ctx.window.height) / 2.0f;
}

void setupCameras(Context &ctx)
{
    ctx.viewport[0] = glm::vec4(0.0f, 0.0f, ctx.window.width / 2.0f, ctx.window.height);
    ctx.viewport[1] = glm::vec4(ctx.window.width/2.0f, 0.0f, ctx.window.width/2.0f, ctx.window.height);
    ctx.projection_matrix[0] = glm::perspective(glm::radians(90.0f), ctx.window.aspect/2, 0.1f, 100.0f);
    ctx.projection_matrix[1] = ctx.projection_matrix[0];
}

void doInitialization(Context &ctx)
{
    // Initialize GL objects
    setupGLPrograms(ctx);
    setupGLPipelines(ctx);
    setupGLFramebuffers(ctx);
    setupGLBuffers(ctx);
    setupGLQueries(ctx);

    // Setup trackball and viewing
    setupTrackball(ctx);
    setupCameras(ctx);

    // Load volume
    if (ctx.input_filename != "") {
        cg::volumeLoadVTK((cg::VolumeBase *)&ctx.volume, ctx.input_filename);
    }
    else {
        createDummyVolume(ctx.volume);
    }
    ctx.volume_transform.translation = glm::vec3(0.0f);
    ctx.volume_transform.scaling = glm::vec3(ctx.volume.base.dimensions) *
        ctx.volume.base.spacing * 1e-3f * ctx.input_scale;
    ctx.volume_uniforms.world_from_image = (
        glm::translate(glm::mat4(), ctx.volume_transform.translation) *
            glm::scale(glm::mat4(), ctx.volume_transform.scaling) *
                glm::translate(glm::mat4(), glm::vec3(-1.0f)) *
                    glm::scale(glm::mat4(), glm::vec3(2.0f)));
    ctx.volume_uniforms.image_from_world = glm::inverse(ctx.volume_uniforms.world_from_image);
    ctx.volume_uniforms.spacing = glm::vec4(ctx.volume.base.spacing, 0.0f);
    createVolumeTexture(&ctx.textures["volume"], ctx.volume);

    // Extract Marching cubes mesh (for comparison)
    {
        auto tic = glfwGetTime();
        float isovalue = ctx.volume_uniforms.threshold * float(1 << 15);
        marching::marchingCubesIndexed(&ctx.volume, isovalue, &ctx.mesh.vertices, &ctx.mesh.indices);
        createMarchingCubesBuffer(&ctx.buffers["mc"], &ctx.textures["mc"],
                                  &ctx.buffers["mc_indices"], ctx.mesh);
        std::printf("MC extraction: %f s\n", float(glfwGetTime() - tic));
        std::printf("MC vertex count: %d\n", ctx.mesh.vertices.size());
    }
}

void updateBrickCache(Context &ctx, int mode, int num_points)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 4, ctx.buffers["raycast_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 5, ctx.buffers["volume_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 6, ctx.buffers["clipmap_uniforms"]);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, ctx.buffers["result"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, ctx.buffers["clipmap"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, ctx.buffers["splats"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, ctx.buffers["timestamp"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, ctx.buffers["tile"]);
    glBindTextureUnit(0, ctx.textures["volume"]);
    glBindTextureUnit(2, ctx.textures["halton3d"]);

    glUsePipelineState(ctx.pipelines["raycast"]);
    {
        if (mode == RAYCACHE_MODE_RAYCAST) {
            // Cast new primary rays
            glUniform1i(1, RAYCACHE_MODE_RAYCAST);
            glDrawArrays(GL_POINTS, 0, num_points);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);

            if (ctx.raycast_uniforms.importance_samples > 0) {
                // Cast importance rays
                glUniform1i(2, ctx.raycast_uniforms.importance_samples);
                glDrawArrays(GL_POINTS, 0, num_points);
                glUniform1i(2, 0);
                glMemoryBarrier(GL_ALL_BARRIER_BITS);
            }
        }
        if (mode == RAYCACHE_MODE_UPDATE) {
            // Clear clipmap bits for bricks to be updated
            glUniform1i(1, RAYCACHE_MODE_CLEAR);
            glDrawArrays(GL_POINTS, 0, num_points);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);

            // Re-insert bricks to be updated
            glUniform1i(1, RAYCACHE_MODE_UPDATE);
            glDrawArrays(GL_POINTS, 0, num_points);
        }
        if (mode == RAYCACHE_MODE_CLEAR) {
            // Clear clipmap bits of all bricks
            glUniform1i(1, RAYCACHE_MODE_CLEAR);
            glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["splats"]);
            glDrawArraysIndirect(GL_POINTS, (void *)32);
        }
        if (mode == RAYCACHE_MODE_INSERT) {
            // Re-insert previous bricks in cache
            glUniform1i(1, RAYCACHE_MODE_INSERT);
            glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["splats"]);
            glDrawArraysIndirect(GL_POINTS, (void *)32);
        }
    }
    glClearCurrentPipelineState();

    // Make sure buffer writes are visible at this point
    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    if (mode != RAYCACHE_MODE_CLEAR) {
        int num_points_adjusted = num_points;
        if (mode == RAYCACHE_MODE_RAYCAST && (ctx.raycast_uniforms.importance_samples > 0)) {
            num_points_adjusted = 2 * num_points;
        }

        if (mode != RAYCACHE_MODE_INSERT) {
            glUsePipelineState(ctx.pipelines["raycast"]);
            {
                // Process bricks currently inserted in the ResultBuffer
                glUniform1i(1, RAYCACHE_MODE_PROCESS);
                glDrawArrays(GL_POINTS, 0, num_points_adjusted * 8);
                glMemoryBarrier(GL_ALL_BARRIER_BITS);
            }
            glClearCurrentPipelineState();
        }

        glUsePipelineState(ctx.pipelines["copy_result"]);
        {
            // Write bricks to cache
            glUniform1i(1, COPY_MODE_INSERT);
            glDrawArrays(GL_POINTS, 0, num_points_adjusted);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);

            // Update counters and draw commands
            glUniform1i(1, COPY_MODE_UPDATE);
            glDrawArrays(GL_POINTS, 0, 1);
        }
        glClearCurrentPipelineState();
    }

    // Make sure buffer writes are visible at this point
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void clearBrickCache(Context &ctx)
{
    setupGLBuffers(ctx);
}

void doRayCachingPass(Context &ctx)
{
    glm::vec3 viewer = glm::inverse(glm::mat3(cg::cgTrackballGetRotationMatrix(ctx.trackball))) * glm::vec3(0.0f, 0.0f, 1.9f);  // TODO

    // Check if clipmap needs to be re-centered
    glm::vec3 center = glm::vec3(ctx.clipmap_uniforms.center);
    glm::vec3 abs_diff = glm::abs(center - viewer);
    float threshold = ctx.clipmap_uniforms.radius * glm::exp2(0.0f);
    if (std::max(abs_diff.x, std::max(abs_diff.y, abs_diff.z)) > threshold) {
        ctx.needs_recentering = true;
    }

    if (ctx.needs_recentering) {
        updateBrickCache(ctx, RAYCACHE_MODE_CLEAR, ctx.splat_buffer_info.capacity);

        // Update clipmap center
        glm::vec3 center_new = glm::floor(viewer / threshold) * threshold;
        ctx.clipmap_uniforms.center = glm::vec4(center_new, 0.0f);
        glNamedBufferSubData(ctx.buffers["clipmap_uniforms"], 0,
                             sizeof(ctx.clipmap_uniforms), &ctx.clipmap_uniforms);

        updateBrickCache(ctx, RAYCACHE_MODE_INSERT, ctx.splat_buffer_info.capacity);
        ctx.needs_recentering = false;
    }
    else {
        int num_update_points = ctx.num_rays_per_frame;
        updateBrickCache(ctx, RAYCACHE_MODE_UPDATE, num_update_points);

        int num_rays = ctx.raycast_uniforms.num_rays_per_tile * (16 * 16);
        updateBrickCache(ctx, RAYCACHE_MODE_RAYCAST, num_rays);
    }
}

void doSortPass(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["clipmap_uniforms"]);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ctx.buffers["index"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, ctx.buffers["splats"]);
    glBindTextureUnit(0, ctx.textures["splats"]);

    glUsePipelineState(ctx.pipelines["sort"]);
    {
        glUniform1i(1, SORT_MODE_CLEAR);
        glDrawArrays(GL_POINTS, 0, ctx.clipmap_uniforms.subres * 128 * 8);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);

        glUniform1i(1, SORT_MODE_BIN);
        glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["splats"]);
        glDrawArraysIndirect(GL_POINTS, (void *)32);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);

        if (ctx.use_parallel_prefix_sum) {
            glUniform1i(1, SORT_MODE_PREFIX_SUM_PARALLEL1);
            glDrawArrays(GL_POINTS, 0, 8);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);

            glUniform1i(1, SORT_MODE_PREFIX_SUM_PARALLEL2);
            glDrawArrays(GL_POINTS, 0, 8);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);
        }
        else {
            glUniform1i(1, SORT_MODE_PREFIX_SUM_SEQUENTIAL);
            glDrawArrays(GL_POINTS, 0, 1);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);
        }

        glUniform1i(1, SORT_MODE_WRITE_INDEX);
        glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["splats"]);
        glDrawArraysIndirect(GL_POINTS, (void *)32);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glClearCurrentPipelineState();
}

void doRayCastingPass(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 4, ctx.buffers["raycast_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 5, ctx.buffers["volume_uniforms"]);

    glBindTextureUnit(0, ctx.textures["volume"]);
    glBindTextureUnit(1, ctx.textures["halton3d"]);

    glUsePipelineState(ctx.pipelines["draw_raycasting"]);
    {
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
    glClearCurrentPipelineState();
}

void doClipmapGridPass(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 3, ctx.buffers["drawable_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 4, ctx.buffers["splat_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 6, ctx.buffers["clipmap_uniforms"]);

    for (int eye = 0; eye < 2; ++eye) {
        ctx.scene_uniforms.projection = ctx.camera[eye].projectionMatrix;
        ctx.scene_uniforms.view = ctx.camera[eye].viewMatrix;
        glNamedBufferSubData(ctx.buffers["scene_uniforms"], 0,
                             sizeof(ctx.scene_uniforms), &ctx.scene_uniforms);

        ctx.drawable_uniforms.model = glm::mat4();
        ctx.drawable_uniforms.normal = ctx.drawable_uniforms.model;  // TODO
        glNamedBufferSubData(ctx.buffers["drawable_uniforms"], 0,
                             sizeof(ctx.drawable_uniforms), &ctx.drawable_uniforms);

        glBindTextureUnit(0, ctx.textures["splats"]);

        glUsePipelineState(ctx.pipelines["draw_clipmap"]);
        {
            glViewport(ctx.viewport[eye][0], ctx.viewport[eye][1],
                       ctx.viewport[eye][2], ctx.viewport[eye][3]);
            glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["splats"]);
            glDrawArraysIndirect(GL_POINTS, (void *)32);
        }
        glClearCurrentPipelineState();
    }
}

void doSplattingPass(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 3, ctx.buffers["drawable_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 4, ctx.buffers["splat_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 6, ctx.buffers["clipmap_uniforms"]);

    for (int eye = 0; eye < 2; ++eye) {
        ctx.scene_uniforms.projection = ctx.camera[eye].projectionMatrix;
        ctx.scene_uniforms.view = ctx.camera[eye].viewMatrix;
        glNamedBufferSubData(ctx.buffers["scene_uniforms"], 0,
                             sizeof(ctx.scene_uniforms), &ctx.scene_uniforms);

        ctx.drawable_uniforms.model = glm::mat4();
        ctx.drawable_uniforms.normal = ctx.drawable_uniforms.model;  // TODO
        glNamedBufferSubData(ctx.buffers["drawable_uniforms"], 0,
                             sizeof(ctx.drawable_uniforms), &ctx.drawable_uniforms);

        glBindTextureUnit(0, ctx.textures["splats"]);

        const auto &pipeline = (ctx.drawable_uniforms.material.albedo[3] < 1.0f) ?
            ctx.pipelines["splat_transparent"] : ctx.pipelines["splat"];
        glUsePipelineState(pipeline);
        {
            glViewport(ctx.viewport[eye][0], ctx.viewport[eye][1],
                       ctx.viewport[eye][2], ctx.viewport[eye][3]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ctx.buffers["index"]);
            glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["splats"]);
            glDrawElementsIndirect(GL_POINTS, GL_UNSIGNED_INT, (void *)16);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }
        glClearCurrentPipelineState();
    }
}

void doMeshPass(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["drawable_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, ctx.buffers["raycast_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 3, ctx.buffers["volume_uniforms"]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ctx.buffers["mc_indices"]);
    glBindTextureUnit(0, ctx.textures["mc"]);
    glBindTextureUnit(1, ctx.textures["volume"]);
    glBindTextureUnit(2, ctx.textures["halton3d"]);

    for (int eye = 0; eye < 2; ++eye) {
        ctx.scene_uniforms.projection = ctx.camera[eye].projectionMatrix;
        ctx.scene_uniforms.view = ctx.camera[eye].viewMatrix;
        glNamedBufferSubData(ctx.buffers["scene_uniforms"], 0,
                             sizeof(ctx.scene_uniforms), &ctx.scene_uniforms);

        ctx.drawable_uniforms.model =
            ctx.volume_uniforms.world_from_image *
                glm::scale(glm::mat4(), 1.0f / glm::vec3(ctx.volume.base.dimensions));
        ctx.drawable_uniforms.normal = glm::transpose(glm::inverse(ctx.drawable_uniforms.model));
        glNamedBufferSubData(ctx.buffers["drawable_uniforms"], 0,
                             sizeof(ctx.drawable_uniforms), &ctx.drawable_uniforms);

#if USE_Z_PREPASS
        glUsePipelineState(ctx.pipelines["draw_mesh_depth"]);
        {
            glViewport(ctx.viewport[eye][0], ctx.viewport[eye][1],
                       ctx.viewport[eye][2], ctx.viewport[eye][3]);
            glUniform1i(0, eye);
            glDrawBuffer(GL_NONE);
            glDrawElements(GL_TRIANGLES, ctx.mesh.indices.size(), GL_UNSIGNED_INT, 0);
            glDrawBuffer(GL_COLOR_ATTACHMENT0);
        }
        glClearCurrentPipelineState();
#endif // USE_Z_PREPASS

        glUsePipelineState(ctx.pipelines["draw_mesh"]);
        {
            glViewport(ctx.viewport[eye][0], ctx.viewport[eye][1],
                       ctx.viewport[eye][2], ctx.viewport[eye][3]);
            glUniform1i(0, eye);
            glDrawElements(GL_TRIANGLES, ctx.mesh.indices.size(), GL_UNSIGNED_INT, 0);
        }
        glClearCurrentPipelineState();
    }
}

void doCompositePass(Context &ctx)
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ctx.buffers["tile"]);

    glUsePipelineState(ctx.pipelines["composite"]);
    {
        glUniform4fv(1, 1, &ctx.scene.background_color[0][0]);
        glUniform4fv(2, 1, &ctx.scene.background_color[1][0]);
        glUniform1i(3, ctx.show_importance);
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
    glClearCurrentPipelineState();
}

void doRendering(Context &ctx)
{
    static glm::mat4 prev_view[] = { ctx.camera[0].viewMatrix, ctx.camera[1].viewMatrix };
    glm::mat4 current_view[] = { ctx.camera[0].viewMatrix, ctx.camera[1].viewMatrix };

    if (ctx.prediction_enabled && ctx.use_raycaching) {
        ctx.camera[0].viewMatrix = predictHeadPose(prev_view[0], current_view[0]);
        ctx.camera[1].viewMatrix = predictHeadPose(prev_view[1], current_view[1]);
    }

    // Update uniforms
    ctx.splat_uniforms.resolution = glm::ivec2(ctx.window.width * 0.5f, ctx.window.height);  // TODO
    ctx.scene_uniforms.time = ctx.scene.elapsed_time;
    for (int i = 0; i < 2; ++i) {
        ctx.raycast_uniforms.projection[i] = ctx.camera[i].projectionMatrix;
        ctx.raycast_uniforms.view[i] = ctx.camera[i].viewMatrix;
        ctx.raycast_uniforms.view_inv[i] = glm::inverse(ctx.camera[i].viewMatrix);
    }
    glNamedBufferSubData(ctx.buffers["raycast_uniforms"], 0,
                         sizeof(ctx.raycast_uniforms), &ctx.raycast_uniforms);
    glNamedBufferSubData(ctx.buffers["volume_uniforms"], 0,
                         sizeof(ctx.volume_uniforms), &ctx.volume_uniforms);
    glNamedBufferSubData(ctx.buffers["clipmap_uniforms"], 0,
                         sizeof(ctx.clipmap_uniforms), &ctx.clipmap_uniforms);
    glNamedBufferSubData(ctx.buffers["splat_uniforms"], 0,
                         sizeof(ctx.splat_uniforms), &ctx.splat_uniforms);

    // Clear timers
    for (auto &ti : ctx.timers) { ti.second.result = 0; }

    // Clear framebuffer
    // Use non-DSA functions as workaround for Issue #1
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ctx.framebuffers["forward"]);
    glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
    glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

    if (ctx.show_background) {
        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["other"].query);
        doCompositePass(ctx);
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["other"].query, GL_QUERY_RESULT,
                              &ctx.timers["other"].result);
        if (ctx.show_importance) { glFlush(); glFinish(); }
    }

    if (ctx.use_raycaching) {
        if (ctx.raycast_uniforms.raycasting_enabled) {
            ctx.raycast_uniforms.frame += 1;
            glBeginQuery(GL_TIME_ELAPSED, ctx.timers["raycast"].query);
            doRayCachingPass(ctx);
            glEndQuery(GL_TIME_ELAPSED);
            glGetQueryObjectui64v(ctx.timers["raycast"].query, GL_QUERY_RESULT,
                                  &ctx.timers["raycast"].result);
        }

        if (ctx.prediction_enabled) {
            ctx.camera[0].viewMatrix = current_view[0];
            ctx.camera[1].viewMatrix = current_view[1];
        }

        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["sort"].query);
        doSortPass(ctx);
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["sort"].query, GL_QUERY_RESULT,
                              &ctx.timers["sort"].result);

        if (ctx.show_isosurface) {
            glBeginQuery(GL_TIME_ELAPSED, ctx.timers["splat"].query);
            glBeginQuery(GL_PRIMITIVES_GENERATED, ctx.splat_count.query);
            doSplattingPass(ctx);
            glEndQuery(GL_PRIMITIVES_GENERATED);
            glEndQuery(GL_TIME_ELAPSED);
            glGetQueryObjectui64v(ctx.splat_count.query, GL_QUERY_RESULT,
                                  &ctx.splat_count.result);
            glGetQueryObjectui64v(ctx.timers["splat"].query, GL_QUERY_RESULT,
                                  &ctx.timers["splat"].result);
        }

        if (ctx.show_clipmap_grid) {
            doClipmapGridPass(ctx);
        }
    }
    else {  // Do raycasting for every pixel (for reference)
        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["raycast"].query);
        if (ctx.show_mesh) {
            doMeshPass(ctx);
        }
        else {
            doRayCastingPass(ctx);
        }
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["raycast"].query, GL_QUERY_RESULT,
                              &ctx.timers["raycast"].result);
    }

    glBeginQuery(GL_TIME_ELAPSED, ctx.timers["resolve"].query);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glEnable(GL_FRAMEBUFFER_SRGB);
    glBlitNamedFramebuffer(ctx.framebuffers["forward"], 0,
                           0, 0, ctx.window.width, ctx.window.height,
                           0, 0, ctx.window.width, ctx.window.height,
                           GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glDisable(GL_FRAMEBUFFER_SRGB);
    glEndQuery(GL_TIME_ELAPSED);
    glGetQueryObjectui64v(ctx.timers["resolve"].query, GL_QUERY_RESULT,
                          &ctx.timers["resolve"].result);

    prev_view[0] = current_view[0];
    prev_view[1] = current_view[1];

    if (ctx.step_frame) {
        ctx.step_frame = false;
        ctx.raycast_uniforms.raycasting_enabled = false;
    }
}

void showSettingsWindow(Context &ctx)
{
    ImGui::SetNextWindowCollapsed(true, ImGuiSetCond_Once);
    if (ImGui::Begin("Settings")) {
        if (ImGui::CollapsingHeader("Volume")) {
            ImGui::SliderFloat("Threshold", &ctx.volume_uniforms.threshold, -0.03f, 0.09f);
        }
        if (ImGui::CollapsingHeader("Raycasting")) {
            ImGui::Checkbox("Use RayCaching", &ctx.use_raycaching);
            ImGui::Checkbox("Show mesh", (bool *)&ctx.show_mesh);
            ImGui::SliderInt("Rays per tile", &ctx.raycast_uniforms.num_rays_per_tile, 0, 128);
            ImGui::SliderInt("Updates per frame", &ctx.num_rays_per_frame, 0, 100000);
            ImGui::SliderFloat("Step length", &ctx.raycast_uniforms.step_length, 0.001, 0.010, "%.3f");
            ImGui::SliderInt("Max steps", &ctx.raycast_uniforms.max_steps, 0, 256);
            ImGui::Checkbox("Importance sampling", (bool *)&ctx.raycast_uniforms.importance_samples);
            ImGui::Checkbox("Predictive sampling", (bool *)&ctx.prediction_enabled);
            ImGui::Checkbox("Use normal refinement", (bool *)&ctx.raycast_uniforms.use_normal_refinement);
            ImGui::Checkbox("Shadows enabled", (bool *)&ctx.raycast_uniforms.num_shadow_rays);
            ImGui::Checkbox("AO enabled", (bool *)&ctx.raycast_uniforms.num_occlusion_rays);
            ImGui::Checkbox("Raycasting enabled", (bool *)&ctx.raycast_uniforms.raycasting_enabled);
            if (ImGui::Button("Step frame")) {
                ctx.step_frame = true;
                ctx.raycast_uniforms.raycasting_enabled = true;
            }
            if (ImGui::SliderInt("Clipmap sub-res", &ctx.clipmap_uniforms.subres, 1, 4)) {
                clearBrickCache(ctx);
            }
        }
        if (ImGui::CollapsingHeader("Sorting")) {
            ImGui::Checkbox("Use parallel prefix sum", (bool *)&ctx.use_parallel_prefix_sum);
        }
        if (ImGui::CollapsingHeader("Splatting")) {
            ImGui::Combo("Splat mode", &ctx.splat_uniforms.splat_mode, "Square\0Round\0Disk\0\0");
            ImGui::SliderFloat("Splat size", &ctx.splat_uniforms.splat_size, 0.1f, 4.0f, "%.1f");
            ImGui::Checkbox("Show isosurface", (bool *)&ctx.show_isosurface);
            ImGui::Checkbox("Show normals", (bool *)&ctx.splat_uniforms.show_normals);
            ImGui::Checkbox("Show clipmap levels", (bool *)&ctx.splat_uniforms.show_clipmap_lod);
            ImGui::Checkbox("Show clipmap grid", (bool *)&ctx.show_clipmap_grid);
        }
        if (ImGui::CollapsingHeader("Lighting")) {
            ImGui::ColorEdit3("Ground color", &ctx.scene_uniforms.hemisphere.ground_color[0]);
            ImGui::ColorEdit3("Sky color", &ctx.scene_uniforms.hemisphere.sky_color[0]);
            ImGui::SliderFloat("Ambient intensity", &ctx.scene_uniforms.hemisphere.intensity, 0.0f, 1.0f);
            ImGui::ColorEdit3("Light color", &ctx.scene_uniforms.light.color[0]);
            ImGui::SliderFloat("Light intensity", &ctx.scene_uniforms.light.intensity, 0.0f, 1.0f);
            ImGui::ColorEdit4("Albedo", &ctx.drawable_uniforms.material.albedo[0]);
            ImGui::SliderFloat("Gloss", &ctx.drawable_uniforms.material.gloss, 0.0f, 1.0f);
            ImGui::SliderFloat("Metallic", &ctx.drawable_uniforms.material.metallic, 0.0f, 1.0f);
            ImGui::SliderFloat("Wrap", &ctx.drawable_uniforms.material.wrap, 0.0f, 1.0f);
        }
        if (ImGui::CollapsingHeader("Scene")) {
            ImGui::ColorEdit4("Background color 0", &ctx.scene.background_color[0][0]);
            ImGui::ColorEdit4("Background color 1", &ctx.scene.background_color[1][0]);
            ImGui::Checkbox("Show background", (bool *)&ctx.show_background);
            ImGui::Checkbox("Show importance", (bool *)&ctx.show_importance);
        }
        if (ImGui::Button("Clear cache")) {
            clearBrickCache(ctx);
        }
    }
    ImGui::End();
}

void showPerformanceWindow(Context &ctx)
{
    ImGui::SetNextWindowCollapsed(true, ImGuiSetCond_Once);
    if (ImGui::Begin("Performance")) {
        ImGui::Text("Splats per frame: %lu", ctx.splat_count.result);
        ImGui::Text("Resolution: %lux%lu", ctx.window.width, ctx.window.height);
        ImGui::Text("Frame rate: %.1f", ImGui::GetIO().Framerate);
        ImGui::Text("Frame times (GPU):");
        {
            int i = 0; double t0 = 0.0, t1 = 0.0;
            for (auto &timer : ctx.timers) {
                t0 += (t1 = (double(timer.second.result) / 1e6));
                ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV((i++) / 7.0f, 0.6f, 0.6f));
                ImGui::Button("", ImVec2(t1 * (ImGui::GetWindowWidth() / 11.1), 8));
                if (ImGui::IsItemHovered()) { ImGui::SetTooltip("%s: %.1lf (ms)", timer.first.c_str(), t1); }
                ImGui::PopStyleColor();
            }
            ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV(i / 7.0f, 0.6f, 0.6f));
            ImGui::Button("", ImVec2(t0 * (ImGui::GetWindowWidth() / 11.1), 8));
            if (ImGui::IsItemHovered()) { ImGui::SetTooltip("total: %.1lf (ms)", t0); }
            ImGui::PopStyleColor();
        }
        {
            int i = 0;
            for (auto &timer : ctx.timers) {
                ImGui::TextColored(ImColor::HSV((i++) / 7.0f, 0.6f, 0.6f), "%s", timer.first.c_str());
                ImGui::SameLine();
            }
            ImGui::TextColored(ImColor::HSV(i / 7.0f, 0.6f, 0.6f), "total");
        }
    }
    ImGui::End();
}

void doUpdate(Context &ctx)
{
    // Update GUI
    showSettingsWindow(ctx);
    showPerformanceWindow(ctx);

    // Update cameras
    for (int i = 0; i < 2; ++i) {
        ctx.camera[i].projectionMatrix = ctx.projection_matrix[i];
        ctx.camera[i].viewMatrix = ctx.view_matrix[i] * glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -1.9f)) * cg::cgTrackballGetRotationMatrix(ctx.trackball);
        ctx.camera[i].viewportNDC = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
    }
}

void errorCallback(int /*error*/, const char* description)
{
    std::cerr << description << std::endl;
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    ImGui_ImplGlfwGL3_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    double x, y;
    glfwGetCursorPos(window, &x, &y);

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            ctx->trackball.center = glm::vec2(x, y);
            cg::cgTrackballStartTracking(ctx->trackball, glm::vec2(x, y));
        }
        if (action == GLFW_RELEASE) {
            cg::cgTrackballStopTracking(ctx->trackball);
        }
    }
}

void cursorPosCallback(GLFWwindow* window, double x, double y)
{
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (ctx->trackball.tracking) {
        cg::cgTrackballMove(ctx->trackball, glm::vec2(x, y));
    }
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (key == GLFW_KEY_F5 && action == GLFW_PRESS) {
        setupGLPrograms(*ctx);
        setupGLPipelines(*ctx);
    }
}

void resizeCallback(GLFWwindow* window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.width = width;
    ctx->window.height = height;
    ctx->window.aspect = float(width) / float(height);

    setupGLFramebuffers(*ctx);
    setupTrackball(*ctx);
    setupCameras(*ctx);
}

int main(int argc, char *argv[])
{
    // Create application context (for passing around)
    Context ctx;
    if (argc > 1) {
        ctx.input_filename = std::string(argv[1]);
    }
    if (argc > 2) {
        ctx.input_scale = float(std::atof(argv[2]));
    }

    // Create window and GL context
    glfwSetErrorCallback(errorCallback);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    ctx.window.id = glfwCreateWindow(ctx.window.width, ctx.window.height, "RayCaching Demo", 0, 0);
    if (!ctx.window.id) {
        std::exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(ctx.window.id);
    glfwSetWindowUserPointer(ctx.window.id, &ctx);
    glfwSetMouseButtonCallback(ctx.window.id, mouseButtonCallback);
    glfwSetCursorPosCallback(ctx.window.id, cursorPosCallback);
    glfwSetKeyCallback(ctx.window.id, keyCallback);
    glfwSetFramebufferSizeCallback(ctx.window.id, resizeCallback);

    // Load GL functions
    glewExperimental = true;
    GLenum glew_status = glewInit();
    if (GLEW_OK != glew_status) {
        std::cerr << glewGetErrorString(glew_status) << std::endl;
        std::exit(EXIT_FAILURE);
    }
    std::cout << "GL version: " << glGetString(GL_VERSION) << std::endl;

    // Create default VAO and bind it once here
    glCreateVertexArrays(1, &ctx.vaos["default"]);
    glBindVertexArray(ctx.vaos["default"]);

    // Initialize GUI
    ImGui_ImplGlfwGL3_Init(ctx.window.id, false /*do not install callbacks*/);

    // Initialize application
    doInitialization(ctx);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window.id)) {
        glfwPollEvents();
        ctx.scene.elapsed_time = glfwGetTime();
        ImGui_ImplGlfwGL3_NewFrame();
        doUpdate(ctx);
        doRendering(ctx);
        ImGui::Render();
        glfwSwapBuffers(ctx.window.id);
    }

    // Shutdown everything
    glfwDestroyWindow(ctx.window.id);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
