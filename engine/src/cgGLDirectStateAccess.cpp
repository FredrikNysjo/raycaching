/// @file    cgGLDirectStateAccess.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgGL.h"
#include "cgGLDirectStateAccess.h"

#include <cassert>

namespace cg {

static CgUnorderedMap<GLuint, GLenum>::type s_textureTargets;

static CgUnorderedMap<GLenum, GLenum>::type s_textureBindingStates = {
    { GL_TEXTURE_1D, GL_TEXTURE_BINDING_1D },
    { GL_TEXTURE_1D_ARRAY, GL_TEXTURE_BINDING_1D_ARRAY },
    { GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D },
    { GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BINDING_2D_ARRAY },
    { GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_BINDING_2D_MULTISAMPLE },
    { GL_TEXTURE_2D_MULTISAMPLE_ARRAY, GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY },
    { GL_TEXTURE_3D, GL_TEXTURE_BINDING_3D },
    { GL_TEXTURE_BUFFER, GL_TEXTURE_BINDING_BUFFER },
    { GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BINDING_CUBE_MAP },
    { GL_TEXTURE_RECTANGLE, GL_TEXTURE_BINDING_RECTANGLE },
};

void cgGlBindTexture(GLenum target, GLuint texture)
{
    // Store target binding for texture before calling API function
    if ((texture != 0) && (s_textureTargets.count(texture) == 0)) {
        s_textureTargets[texture] = target;
    }
#undef glBindTexture
    glBindTexture(target, texture);
#define glBindTexture cg::cgGlBindTexture
}

void cgGlDeleteTextures(GLsizei n, const GLuint *textures)
{
    // Clear target bindings for textures before calling API function
    for (int i = 0; i < n; ++i) {
        s_textureTargets.erase(textures[i]);
    }
#undef glDeleteTextures
    glDeleteTextures(n, textures);
#define glDeleteTextures cg::cgGlDeleteTextures
}

void cgGlGetTextureParameterfv(const GLuint texture,
                               const GLenum value, GLfloat *data)
{
    GLuint target = s_textureTargets[texture];
    GLint prevTextureBinding;
    glGetIntegerv(s_textureBindingStates[target], &prevTextureBinding);
    glBindTexture(target, texture);
    glGetTexParameterfv(target, value, data);
    glBindTexture(target, prevTextureBinding);
}

void cgGlGetTextureParameteriv(const GLuint texture,
                               const GLenum value, GLint *data)
{
    GLuint target = s_textureTargets[texture];
    GLint prevTextureBinding;
    glGetIntegerv(s_textureBindingStates[target], &prevTextureBinding);
    glBindTexture(target, texture);
    glGetTexParameteriv(target, value, data);
    glBindTexture(target, prevTextureBinding);
}

void cgGlGetTextureParameterIiv(const GLuint texture,
                                const GLenum value, GLint *data)
{
    GLuint target = s_textureTargets[texture];
    GLint prevTextureBinding;
    glGetIntegerv(s_textureBindingStates[target], &prevTextureBinding);
    glBindTexture(target, texture);
    glGetTexParameterIiv(target, value, data);
    glBindTexture(target, prevTextureBinding);
}

void cgGlGetTextureParameterIuiv(const GLuint texture,
                                 const GLenum value, GLuint *data)
{
    GLuint target = s_textureTargets[texture];
    GLint prevTextureBinding;
    glGetIntegerv(s_textureBindingStates[target], &prevTextureBinding);
    glBindTexture(target, texture);
    glGetTexParameterIuiv(target, value, data);
    glBindTexture(target, prevTextureBinding);
}

void cgGlGetTextureLevelParameterfv(const GLuint texture, const GLint lod,
                                    const GLenum value, GLfloat *data)
{
    GLuint target = s_textureTargets[texture];
    GLint prevTextureBinding;
    glGetIntegerv(s_textureBindingStates[target], &prevTextureBinding);
    glBindTexture(target, texture);
    glGetTexLevelParameterfv(target, lod, value, data);
    glBindTexture(target, prevTextureBinding);
}

void cgGlGetTextureLevelParameteriv(const GLuint texture, const GLint lod,
                                    const GLenum value, GLint *data)
{
    GLuint target = s_textureTargets[texture];
    GLint prevTextureBinding;
    glGetIntegerv(s_textureBindingStates[target], &prevTextureBinding);
    glBindTexture(target, texture);
    glGetTexLevelParameteriv(target, lod, value, data);
    glBindTexture(target, prevTextureBinding);
}

void cgGlGetNamedBufferParameteriv(const GLuint buffer,
                                   const GLenum pname, GLint *data)
{
    GLint prevArrayBufferBinding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &prevArrayBufferBinding);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glGetBufferParameteriv(GL_ARRAY_BUFFER, pname, data);
    glBindBuffer(GL_ARRAY_BUFFER, prevArrayBufferBinding);
}

void cgGlGetNamedBufferParameteri64v(const GLuint buffer,
                                     const GLenum pname, GLint64 *data)
{
    GLint prevArrayBufferBinding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &prevArrayBufferBinding);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glGetBufferParameteri64v(GL_ARRAY_BUFFER, pname, data);
    glBindBuffer(GL_ARRAY_BUFFER, prevArrayBufferBinding);
}

void cgGlGetNamedFramebufferAttachmentParameteriv(const GLuint framebuffer,
                                                  const GLenum attachment,
                                                  const GLenum pname,
                                                  GLint *params)
{
    GLint prevDrawFramebufferBinding;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &prevDrawFramebufferBinding);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
    glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER, attachment,
                                          pname, params);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, prevDrawFramebufferBinding);
}

} // namespace cg
