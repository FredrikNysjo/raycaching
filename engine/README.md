# CG Engine

## Overview

Rendering engine for computer graphics and virtual reality (VR) research.

## Features

- Platform support: Linux, Windows
- Language: C++11
- Compiler support: GCC 4.6 (or higher), Visual Studio 2013 (or higher)
- Build system: CMake
- API support: OpenGL 3.3+ (core profile)
- VR support: OpenVR (currently works on Windows only)
- Third-party dependencies: cJSON, GLEW, GLFW, GLM, ImGui, lodepng, OpenVR

## System requirements

- GPU and driver supporting OpenGL 3.3+ and the following extensions:
    - `GL_ARB_shading_language_420pack`

## Build instructions

You use CMake to build the engine and the examples. Each example builds the
engine as a CMake subdirectory via its CMakeLists.txt, which is the recommended
way to build the engine in an application. Third-party dependencies are
included under the `third_party` directory as a Git subtree.

### Build dependencies for Linux

Dependencies: GCC 4.6.3 (or higher), make, CMake 2.8.7 (or higher), Mesa 10.1
(or higher)

Additional libraries you might need to install are Xcursor, Xrandr, Xi, and
Xmu. On Ubuntu 12.04/14.04, you can install these libraries with

    sudo apt-get install libxmu-dev libxi-dev libxrandr-dev libxcursor-dev

### Build dependencies for Windows

Dependencies: Visual Studio 2013 (or higher), CMake 2.8.12 (or higher). CMake
3.1 is required for Visual Studio 2015)

## Runtime dependencies

- OpenVR uses the OpenVR runtime, which can be installed via
  [Steam](http://www.steampowered.com).

## License

Copyright (c) 2016 Fredrik Nysjö

This software may be modified and distributed under the terms of the MIT
license. See `LICENSE.md` for details.

Third-party dependencies cJSON, GLEW, GLFW, GLM, ImGui, lodepng, and OpenVR are
distributed under their own respective licenses. See `third_party/LICENSES.md`
for details.