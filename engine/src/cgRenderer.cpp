/// @file    cgRenderer.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgRenderer.h"

#include <cstring>
#include <cassert>

namespace cg {

CgRenderStateData::CgRenderStateData(int value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(const glm::ivec2 &value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(const glm::ivec3 &value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(const glm::ivec4 &value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(float value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(const glm::vec2 &value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(const glm::vec3 &value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}

CgRenderStateData::CgRenderStateData(const glm::vec4 &value)
{
    std::memset(&data[0], 0, sizeof(data));
    std::memcpy(&data[0], &value, sizeof(value));
}


CgUnorderedMap<GLenum, void *>::type CgRenderStateData::getters;
CgUnorderedMap<GLenum, void *>::type CgRenderStateData::setters;

void cgInitRenderStateSetters()
{
#define cgInitRenderStateiv(ENUM, SETTER) { \
    CgRenderStateData::setters[ENUM] = (void *)SETTER; \
    CgRenderStateData::getters[ENUM] = (void *)glGetIntegerv; \
}
#define cgInitRenderStatefv(ENUM, SETTER) { \
    CgRenderStateData::setters[ENUM] = (void *)SETTER; \
    CgRenderStateData::getters[ENUM] = (void *)glGetFloatv; \
}

    cgInitRenderStateiv(GL_VIEWPORT, cgGlSetViewport);
    cgInitRenderStateiv(GL_SCISSOR_TEST, cgGlSetScissorTest);
    cgInitRenderStateiv(GL_SCISSOR_BOX, cgGlSetScissorBox);
    cgInitRenderStateiv(GL_DEPTH_CLAMP, cgGlSetDepthClamp);
    cgInitRenderStateiv(GL_RASTERIZER_DISCARD, cgGlSetRasterizerDiscard);
    cgInitRenderStateiv(GL_POLYGON_MODE, cgGlSetPolygonMode);
    cgInitRenderStateiv(GL_CULL_FACE, cgGlSetCullFace);
    cgInitRenderStateiv(GL_CULL_FACE_MODE, cgGlSetCullFaceMode);
    cgInitRenderStateiv(GL_FRONT_FACE, cgGlSetFrontFace);
    cgInitRenderStateiv(GL_POLYGON_OFFSET_POINT, cgGlSetPolygonOffsetPoint);
    cgInitRenderStateiv(GL_POLYGON_OFFSET_LINE, cgGlSetPolygonOffsetLine);
    cgInitRenderStateiv(GL_POLYGON_OFFSET_FILL, cgGlSetPolygonOffsetFill);
    cgInitRenderStatefv(GL_POLYGON_OFFSET_FACTOR, cgGlSetPolygonOffsetFactor);
    cgInitRenderStatefv(GL_POLYGON_OFFSET_UNITS, cgGlSetPolygonOffsetUnits);
    cgInitRenderStatefv(GL_LINE_WIDTH, cgGlSetLineWidth);
    cgInitRenderStateiv(GL_MULTISAMPLE, cgGlSetMultisample);
    cgInitRenderStateiv(GL_SAMPLE_SHADING, cgGlSetSampleShading);
    cgInitRenderStatefv(GL_MIN_SAMPLE_SHADING_VALUE, cgGlSetMinSampleShadingValue);
    cgInitRenderStateiv(GL_SAMPLE_MASK, cgGlSetSampleMask);
    cgInitRenderStateiv(GL_SAMPLE_MASK_VALUE, cgGlSetSampleMaskValue);
    cgInitRenderStateiv(GL_SAMPLE_ALPHA_TO_COVERAGE, cgGlSetSampleAlphaToCoverage);
    cgInitRenderStateiv(GL_SAMPLE_ALPHA_TO_ONE, cgGlSetSampleAlphaToOne);
    cgInitRenderStateiv(GL_DEPTH_TEST, cgGlSetDepthTest);
    cgInitRenderStateiv(GL_DEPTH_WRITEMASK, cgGlSetDepthWritemask);
    cgInitRenderStateiv(GL_DEPTH_FUNC, cgGlSetDepthFunc);
    cgInitRenderStatefv(GL_DEPTH_RANGE, cgGlSetDepthRange);
    cgInitRenderStateiv(GL_STENCIL_TEST, cgGlSetStencilTest);
    cgInitRenderStateiv(GL_STENCIL_FAIL, cgGlSetStencilFail);
    cgInitRenderStateiv(GL_STENCIL_PASS_DEPTH_FAIL, cgGlSetStencilPassDepthFail);
    cgInitRenderStateiv(GL_STENCIL_PASS_DEPTH_PASS, cgGlSetStencilPassDepthPass);
    cgInitRenderStateiv(GL_STENCIL_FUNC, cgGlSetStencilFunc);
    cgInitRenderStateiv(GL_STENCIL_REF, cgGlSetStencilRef);
    cgInitRenderStateiv(GL_STENCIL_WRITEMASK, cgGlSetStencilWritemask);
    cgInitRenderStateiv(GL_STENCIL_VALUE_MASK, cgGlSetStencilValueMask);
    cgInitRenderStateiv(GL_STENCIL_BACK_FAIL, cgGlSetStencilBackFail);
    cgInitRenderStateiv(GL_STENCIL_BACK_PASS_DEPTH_FAIL, cgGlSetStencilBackPassDepthFail);
    cgInitRenderStateiv(GL_STENCIL_BACK_PASS_DEPTH_PASS, cgGlSetStencilBackPassDepthPass);
    cgInitRenderStateiv(GL_STENCIL_BACK_FUNC, cgGlSetStencilBackFunc);
    cgInitRenderStateiv(GL_STENCIL_BACK_REF, cgGlSetStencilBackRef);
    cgInitRenderStateiv(GL_STENCIL_BACK_WRITEMASK, cgGlSetStencilBackWritemask);
    cgInitRenderStateiv(GL_STENCIL_BACK_VALUE_MASK, cgGlSetStencilBackValueMask);
    cgInitRenderStateiv(GL_COLOR_LOGIC_OP, cgGlSetColorLogicOp);
    cgInitRenderStateiv(GL_LOGIC_OP_MODE, cgGlSetLogicOpMode);
    cgInitRenderStateiv(GL_BLEND, cgGlSetBlend);
    cgInitRenderStateiv(GL_BLEND_SRC_RGB, cgGlSetBlendSrcRGB);
    cgInitRenderStateiv(GL_BLEND_DST_RGB, cgGlSetBlendDstRGB);
    cgInitRenderStateiv(GL_BLEND_EQUATION_RGB, cgGlSetBlendEquationRGB);
    cgInitRenderStateiv(GL_BLEND_SRC_ALPHA, cgGlSetBlendSrcAlpha);
    cgInitRenderStateiv(GL_BLEND_DST_ALPHA, cgGlSetBlendDstAlpha);
    cgInitRenderStateiv(GL_BLEND_EQUATION_ALPHA, cgGlSetBlendEquationAlpha);
    cgInitRenderStateiv(GL_COLOR_WRITEMASK, cgGlSetColorWritemask);
    cgInitRenderStatefv(GL_BLEND_COLOR, cgGlSetBlendColor);

#undef cgInitRenderStateiv
#undef cgInitRenderStatefv
}


// Single uniform buffer for scene uniforms and drawable uniforms. The buffer
// is invalidated at the beginning of each cgRenderDrawables() call to avoid
// syncronization with previous calls.
static GLuint s_ubo = 0;

// Stack for storing block info data
static CgVector<CgUniformBlockInfo>::type s_uniformBlocks;

// Push uniform data for drawable to buffer and return uniform block info that
// can be used for binding the block when drawing or for pushing new uniform
// data to the buffer. Buffer must be mapped to an aligned offset before we
// call this function.
static void _cgPushUniformData(const GLuint buffer, void *bufferPtr,
                               const GLuint index, const std::uint32_t tag,
                               const void *data, const size_t size,
                               CgVector<CgUniformBlockInfo>::type *uniformBlocks)
{
    assert(bufferPtr != nullptr);
    assert(data != nullptr);
    assert(uniformBlocks != nullptr);

    // Compute HW aligned block size and offset
    GLintptr sizeAligned = (size % 256) ? (256 * ((size / 256) + 1)) : size;
    GLsizeiptr offsetAligned = (uniformBlocks->size() ?
        uniformBlocks->back().offsetAligned + uniformBlocks->back().sizeAligned : 0);

    // Push uniform data to mapped GPU buffer
    std::memcpy((std::uint8_t *)bufferPtr + offsetAligned, data, size);

    // Store uniform block info
    CgUniformBlockInfo head = {
        tag, index, buffer, offsetAligned, sizeAligned };
    uniformBlocks->push_back(head);
}

// Upload uniform data and store uniform block infos for rendering
void _cgRendererUploadUniforms(const CgCamera &camera,
                               const CgDrawable *drawables,
                               const size_t drawableCount, const float time)
{
    assert(drawables != nullptr || drawableCount == 0);

    // Save OpenGL state
    GLint prevUniformBufferBinding;
    glGetIntegerv(GL_UNIFORM_BUFFER_BINDING, &prevUniformBufferBinding);

    // Clear uniform block list
    s_uniformBlocks.clear();

    // Bind and map uniform buffer. We also invalidate the buffer before
    // mapping it, to prevent syncronization.
    if (!s_ubo) { glGenBuffers(1, &s_ubo); }
    glBindBuffer(GL_UNIFORM_BUFFER, s_ubo);
    glBufferData(GL_UNIFORM_BUFFER, 1000000, nullptr, GL_STATIC_DRAW);
    void *bufferPtr = glMapBufferRange(GL_UNIFORM_BUFFER, 0, 1000000,
                                       GL_MAP_WRITE_BIT);

    // Push scene uniforms to mapped buffer and store their block infos
    glm::mat4 view = camera.viewMatrix;
    glm::mat4 projection = camera.projectionMatrix;
    CgRendererSceneUniforms sceneUniforms = { view, projection, time };
    _cgPushUniformData(s_ubo, bufferPtr, CG_SCENE_UNIFORMS,
                       0, &sceneUniforms, sizeof(sceneUniforms),
                       &s_uniformBlocks);

    // Push drawable uniforms to mapped buffer and store their block infos
    for (unsigned di = 0; di < drawableCount; ++di) {
        const CgDrawable *drawable = &drawables[di];

        // Push default drawable uniforms
        glm::mat4 model = drawable->modelMatrix;
        glm::mat4 mv = view * model;
        glm::mat4 mvp = projection * mv;
        glm::mat4 normal = glm::mat4(glm::transpose(glm::inverse(glm::mat3(mv))));
        CgRendererDrawableUniforms drawableUniforms = { mvp, mv, model, normal };
        _cgPushUniformData(s_ubo, bufferPtr, CG_DRAWABLE_UNIFORMS,
                           di, &drawableUniforms, sizeof(drawableUniforms),
                           &s_uniformBlocks);

        // Push custom drawable uniforms
        if (drawable->uniformBlockData) {
            for (auto it : *drawable->uniformBlockData) {
                GLuint blockBinding = it.first;
                const CgUniformBlockData &data = it.second;
                _cgPushUniformData(s_ubo, bufferPtr, blockBinding,
                                   di, data.ptr, data.size,
                                   &s_uniformBlocks);
            }
        }
    }

    // Unmap buffer to upload data
    glUnmapBuffer(GL_UNIFORM_BUFFER);

    // Restore OpenGL state
    glBindBuffer(GL_UNIFORM_BUFFER, prevUniformBufferBinding);
}

// Render drawables
void cgRendererDraw(const CgCamera &camera, const CgDrawable *drawables,
                    const size_t drawableCount, const float time,
                    const CgRenderStateInfo *pipelineState)
{
    assert(drawables != nullptr || drawableCount == 0);

    // Save OpenGL state
    GLint prevCurrentProgram;
    glGetIntegerv(GL_CURRENT_PROGRAM, &prevCurrentProgram);
    GLint prevVertexArrayBinding;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVertexArrayBinding);
    GLint prevActiveTexture;
    glGetIntegerv(GL_ACTIVE_TEXTURE, &prevActiveTexture);

    // Define current OpenGL object bindings
    GLuint program = prevCurrentProgram;
    GLuint vao = prevVertexArrayBinding;
    CgTextureBinding textures[CG_MAX_DRAWABLE_TEXTURES];
    CgRenderStateInfo currentState0;
    CgRenderStateInfo currentState1;

    // Upload uniform data and populate block info list
    _cgRendererUploadUniforms(camera, drawables, drawableCount, time);

    // Bind scene uniforms (always the first block)
    CgUniformBlockInfo *uniformBlock = &s_uniformBlocks[0];
    glBindBufferRange(GL_UNIFORM_BUFFER, uniformBlock->index,
                      uniformBlock->buffer, uniformBlock->offsetAligned,
                      uniformBlock->sizeAligned);
    uniformBlock++;

    // Apply pipeline render state
    if (pipelineState) {
        for (auto it : *pipelineState) {
            GLenum name = it.first;
            const CgRenderStateData &renderState = it.second;
            ((cgGlGetv)(CgRenderStateData::getters[name]))(name, &currentState0[name].data[0]);
            ((cgGlSetv)(CgRenderStateData::setters[name]))(&renderState.data[0]);
        }
    }

    // Draw objects
    for (unsigned di = 0; di < drawableCount; ++di) {
        const CgDrawable *drawable = &drawables[di];

        // Activate program
        if (drawable->program != program) {
            program = drawable->program;
            glUseProgram(program);
        }

        // Bind vertex format
        if (drawable->vao != vao) {
            vao = drawable->vao;
            glBindVertexArray(vao);
        }

        // Bind textures
        for (unsigned ti = 0; ti < CG_MAX_DRAWABLE_TEXTURES; ++ti) {
            if ((drawable->textures[ti].target != textures[ti].target ||
                 drawable->textures[ti].texture != textures[ti].texture)) {
                textures[ti] = drawable->textures[ti];
                glActiveTexture(GL_TEXTURE0 + ti);
                glBindTexture(textures[ti].target, textures[ti].texture);
            }
        }

        // Bind drawable uniforms
        while (uniformBlock->tag == di) {
            glBindBufferRange(GL_UNIFORM_BUFFER, uniformBlock->index,
                              uniformBlock->buffer, uniformBlock->offsetAligned,
                              uniformBlock->sizeAligned);
            uniformBlock++;
        }

        // Apply dynamic render state
        if (drawable->dynamicStateInfo) {
            for (auto it : *drawable->dynamicStateInfo) {
                GLenum name = it.first;
                const CgRenderStateData &renderState = it.second;
                ((cgGlGetv)(CgRenderStateData::getters[name]))(name, &currentState1[name].data[0]);
                ((cgGlSetv)(CgRenderStateData::setters[name]))(&renderState.data[0]);
            }
        }

        // Draw!
        const CgDrawInfo &drawInfo = drawable->drawInfo;
        if (drawInfo.indexCount > 0) {
            glDrawElementsInstancedBaseVertex(drawInfo.primitiveType,
                                              drawInfo.indexCount, drawInfo.indexType,
                                              (void *)(size_t)drawInfo.firstIndex,
                                              drawInfo.instances, drawInfo.baseVertex);
        }
        else {
            glDrawArraysInstanced(drawInfo.primitiveType, drawInfo.baseVertex,
                                  drawInfo.vertexCount, drawInfo.instances);
        }

        // Undo dynamic render state
        if (drawable->dynamicStateInfo) {
            for (auto it : *drawable->dynamicStateInfo) {
                GLenum name = it.first;
                ((cgGlSetv)(CgRenderStateData::setters[name]))(&currentState1[name].data[0]);
            }
        }
    }

    // Undo pipeline render state
    if (pipelineState) {
        for (auto it : *pipelineState) {
            GLenum name = it.first;
            ((cgGlSetv)(CgRenderStateData::setters[name]))(&currentState0[name].data[0]);
        }
    }

    // Restore OpenGL state
    glUseProgram(prevCurrentProgram);
    glBindVertexArray(prevVertexArrayBinding);
    glActiveTexture(prevActiveTexture);
}

} // namespace cg
