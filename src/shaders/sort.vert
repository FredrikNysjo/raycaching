#version 450

#define SORT_MODE_BIN 0
#define SORT_PREFIX_SUM_SEQUENTIAL 1
#define SORT_MODE_WRITE_INDEX 2
#define SORT_MODE_CLEAR 3
#define SORT_MODE_PREFIX_SUM_PARALLEL1 4
#define SORT_MODE_PREFIX_SUM_PARALLEL2 5

#define BRICK_SIZE 5

#define NUM_BINS_PER_LOD (u_clipmap.subres * 128)
#define NUM_BINS (u_clipmap.subres * 128 * 8)
#define NUM_LAYERS_PER_LOD (u_clipmap.subres * 32)
#define NUM_BIN_GROUPS 8
#define BIN_GROUP_SIZE (NUM_BINS_PER_LOD)
int END_OF_BUFFER = ((8 * 1000000) - 1 - NUM_BIN_GROUPS);
// Note: Need to make END_OF_BUFFER a variable in order to prevent the NVIDIA
// compiler from hanging.

layout(binding = 1, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(location = 1) uniform int u_mode;

layout(binding = 0) uniform usamplerBuffer u_brick_texture;

layout(std430, binding = 1) restrict buffer IndexBuffer {
    int data[];
} u_index_buffer;

layout(std430, binding = 2) restrict buffer SplatBuffer {
    int start;
    int count;
    int capacity;
    int pad;
    ivec4 cmd[2];  // DrawArraysInstancedCommand
    uvec4 data[];
} u_splat_buffer;

vec4 clipmapFromWorld(in vec3 p, in vec3 clipmap_center, in int lod)
{
    vec3 q = (p - u_clipmap.center.xyz) / u_clipmap.radius;
    vec3 pos = u_clipmap.dimensions[0] * ((q / exp2(lod)) * 0.5 + 0.5);

    return vec4(pos, lod);
}

ivec2 tileAxes(in vec3 r)
{
    int view_axis = 0;
    if (abs(r.y) > abs(r.x) && abs(r.y) > abs(r.z)) { view_axis = 1; }
    if (abs(r.z) > abs(r.x) && abs(r.z) > abs(r.y)) { view_axis = 2; }

    return ivec2(view_axis + 1, view_axis + 2) % 3;
}

// Brick voxel traversal orders based on in which octant a brick lies
const int g_traversal_order[] = int[](
    0, 1, 2, 4, 3, 6, 5, 7,
    1, 0, 3, 5, 2, 4, 7, 6,
    2, 0, 3, 6, 1, 4, 7, 5,
    3, 1, 2, 7, 0, 5, 6, 4,
    4, 6, 5, 0, 7, 2, 1, 3,
    5, 7, 4, 1, 6, 3, 0, 2,
    6, 7, 4, 2, 5, 3, 0, 1,
    7, 5, 6, 3, 4, 2, 1, 0
);

int binIndex(vec4 clipmap_pos)
{
    int lod = int(clipmap_pos.w);
    ivec2 ax = tileAxes(clipmap_pos.xyz - 64.0);
    ivec3 tmp = ivec3(abs(clipmap_pos.xyz - 64.0) * u_clipmap.subres) - NUM_LAYERS_PER_LOD;
    int layer = clamp(max(tmp.x, max(tmp.y, tmp.z)), 0, NUM_LAYERS_PER_LOD - 1);
    int bin_idx = (layer * 4 + (tmp[ax[1]] % 2) * 2 + (tmp[ax[0]] % 2)) + (NUM_BINS_PER_LOD * lod);

    return bin_idx;
}

void main()
{
    if (u_mode == SORT_MODE_BIN) {
        uvec4 header = texelFetch(u_brick_texture, 0);
        int brick_idx = int((gl_VertexID + header.r) % header.b);
        int brick_ofs = 3 + BRICK_SIZE * brick_idx;
        uvec4 brick_data = texelFetch(u_brick_texture, brick_ofs);
        vec3 world_pos = uintBitsToFloat(brick_data.rgb);
        int lod = int(brick_data.a & 0xfu);
        uint mask = (brick_data.a >> 8u) & 0xffu;
        if (bool(brick_data.a >> 31u)) { return; }  // Skip hidden bricks

        vec4 clipmap_pos = clipmapFromWorld(world_pos, u_clipmap.center.xyz, lod);
        int bin_idx = binIndex(clipmap_pos);

        atomicAdd(u_index_buffer.data[END_OF_BUFFER - bin_idx], bitCount(mask));
    }

    if (u_mode == SORT_PREFIX_SUM_SEQUENTIAL) {
        // Compute prefix sums (in back-to-front order)
        for (int i = (NUM_BINS - 2); i >= 0; --i) {
            int prev_sum = u_index_buffer.data[END_OF_BUFFER - (i + 1)];
            u_index_buffer.data[END_OF_BUFFER - i] += prev_sum;
        }
        // Update draw count for indirect drawing
        u_splat_buffer.cmd[0][0] = u_index_buffer.data[END_OF_BUFFER];
    }

    if (u_mode == SORT_MODE_PREFIX_SUM_PARALLEL1 ||
        u_mode == SORT_MODE_PREFIX_SUM_PARALLEL2) {
        int group_idx = gl_VertexID;
        int group_ofs = END_OF_BUFFER - (BIN_GROUP_SIZE * group_idx);
        int prefix_sum = 0;
        if (u_mode == SORT_MODE_PREFIX_SUM_PARALLEL1) {
            // Compute prefix sums of group elements (in back-to-front order)
            for (int i = (BIN_GROUP_SIZE - 2); i >= 0; --i) {
                prefix_sum = u_index_buffer.data[group_ofs - (i + 1)];
                u_index_buffer.data[group_ofs - i] += prefix_sum;
            }
            // Save prefix sum of last element for next pass
            prefix_sum = u_index_buffer.data[group_ofs];
            u_index_buffer.data[END_OF_BUFFER + 1 + group_idx] = prefix_sum;
        }
        if (u_mode == SORT_MODE_PREFIX_SUM_PARALLEL2) {
            // Sum last elements of previous groups
            for (int i = (NUM_BIN_GROUPS - 1); i > group_idx; --i) {
                prefix_sum += u_index_buffer.data[END_OF_BUFFER + 1 + i];
            }
            // Add sum to prefix sums of group elements
            for (int i = (BIN_GROUP_SIZE - 1); i >= 0; --i) {
                u_index_buffer.data[group_ofs - i] += prefix_sum;
            }
            if (group_idx == 0) {
                // Update draw count for indirect draw command
                u_splat_buffer.cmd[0][0] = u_index_buffer.data[END_OF_BUFFER];
            }
        }
    }

    if (u_mode == SORT_MODE_WRITE_INDEX) {
        uvec4 header = texelFetch(u_brick_texture, 0);
        int brick_idx = int((gl_VertexID + header.r) % header.b);
        int brick_ofs = 3 + BRICK_SIZE * brick_idx;
        uvec4 brick_data = texelFetch(u_brick_texture, brick_ofs);
        vec3 world_pos = uintBitsToFloat(brick_data.rgb);
        int lod = int(brick_data.a & 0xfu);
        uint mask = (brick_data.a >> 8u) & 0xffu;
        if (bool(brick_data.a >> 31u)) { return; }  // Skip hidden bricks

        vec4 clipmap_pos = clipmapFromWorld(world_pos, u_clipmap.center.xyz, lod);
        int bin_idx = binIndex(clipmap_pos);

        int count = atomicAdd(u_index_buffer.data[END_OF_BUFFER - bin_idx], -bitCount(mask));
        int octant = int(dot(sign(clipmap_pos.xyz) * 0.5 + 0.5, vec3(1, 2, 4)));
        //for (int i = 0; i < 8; ++i) {  // Use same traversal order everywhere
        for (int j = 0; j < 8; ++j) { int i = g_traversal_order[octant * 8 + j];
            if (bool(mask & (1u << i))) {
                count -= 1;
                u_index_buffer.data[count] = (brick_idx * 8 * 8 + (bin_idx & 7) * 8 + i);
            }
        }
    }

    if (u_mode == SORT_MODE_CLEAR) {
        u_index_buffer.data[END_OF_BUFFER - gl_VertexID] = 0;
    }
}
