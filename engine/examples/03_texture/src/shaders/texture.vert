#version 330
#extension GL_ARB_shading_language_420pack : require

//layout(location = 0) in vec4 a_position;
//layout(location = 1) in vec3 a_color;
//layout(location = 2) in vec3 a_normal;
//layout(location = 3) in vec3 a_texcoord0;

out vec2 v_texcoord;

//layout(binding = 1, std140) uniform SceneUniforms {
//    mat4 u_view;
//    mat4 u_projection;
//    float u_time;
//};

layout(binding = 3, std140) uniform DrawableUniforms {
    mat4 u_mvp;
    mat4 u_mv;
    mat4 u_model;
};

void main()
{
    vec4 position = vec4(0.0);
    if (gl_VertexID == 0) {
        position = vec4(-0.5, -0.5, 0.0, 1.0);
    }
    if (gl_VertexID == 1) {
        position = vec4(0.5, -0.5, 0.0, 1.0);
    }
    if (gl_VertexID == 2) {
        position = vec4(-0.5, 0.5, 0.0, 1.0);
    }
    if (gl_VertexID == 3) {
        position = vec4(0.5, 0.5, 0.0, 1.0);
    }
    v_texcoord = position.xy + 0.5;
    v_texcoord.y = 1.0 - v_texcoord.y;  // Flip Y-axis
    gl_Position = u_mvp * position;
}
