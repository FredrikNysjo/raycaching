/// @file    cgRenderer.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"
#include "cgGL.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

namespace cg {

/// @name Constants
///@{
static const unsigned CG_MAX_DRAWABLE_TEXTURES = 6;
///@}

/// Default vertex attribute bindings
enum CgAttributeBinding {
    CG_POSITION = 0,
    CG_COLOR = 1,
    CG_NORMAL = 2,
    CG_TEXCOORD0 = 3,
    CG_ATTRIBUTE_BINDING_END = CG_TEXCOORD0,
};

/// Default uniform block bindings
enum CgUniformBlockBinding {
    CG_SCENE_UNIFORMS = 1,
    CG_RENDER_PASS_UNIFORMS = 2,
    CG_DRAWABLE_UNIFORMS = 3,
    CG_UNIFORM_BLOCK_BINDING_END = CG_DRAWABLE_UNIFORMS,
};

/// Struct for basic materials
struct CgMaterial {
    std::uint32_t type;
    glm::vec3 albedo;
    float gloss;
    float metallic;
};

/// Struct for camera
struct CgCamera {
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;
    float zNear;
    float zFar;
    glm::vec4 viewportNDC;
};

/// Struct for draw call info
struct CgDrawInfo {
    std::uint32_t vertexCount;
    std::uint32_t indexCount;
    std::uint32_t baseVertex;
    std::uint32_t firstIndex;
    GLenum indexType;
    GLenum primitiveType;
    std::int32_t instances;

    CgDrawInfo() :
        vertexCount(0),
        indexCount(0),
        baseVertex(0),
        firstIndex(0),
        indexType(GL_UNSIGNED_INT),
        primitiveType(GL_TRIANGLES),
        instances(1)
    {}
};

/// Struct for texture bindings
struct CgTextureBinding {
    GLenum target;
    std::uint32_t texture;  ///< GL handle

    CgTextureBinding(GLenum target=GL_TEXTURE_2D, GLuint texture=0) :
        target(target),
        texture(texture)
    {}
};

/// Struct for storing info about data for uniform block
struct CgUniformBlockData {
    const void *ptr;  ///< Weak ref. to data
    size_t size;  ///< Data size in bytes
};

/// Struct for storing uniform block info
struct CgUniformBlockInfo {
    std::uint32_t tag;  ///< Custom tag (drawable index)
    GLuint index;  ///< Binding index
    GLuint buffer;  ///< Uniform buffer
    GLintptr offsetAligned;  ///< Offset aligned to 256 or 16 bytes
    GLsizeiptr sizeAligned;  ///< Size aligned to multiple of 256 or 16 bytes
};

/// Struct for default render pass uniform block
struct CgRendererSceneUniforms {
    glm::mat4 view;
    glm::mat4 projection;
    float time;
};

/// Struct for default drawable uniforms
struct CgRendererDrawableUniforms {
    glm::mat4 mvp;
    glm::mat4 mv;
    glm::mat4 model;
    glm::mat4 normal;
};

/// Struct for render state data
struct CgRenderStateData {
    std::uint8_t data[sizeof(glm::vec4)];

    static CgUnorderedMap<GLenum, void *>::type getters;
    static CgUnorderedMap<GLenum, void *>::type setters;

    CgRenderStateData() {}
    CgRenderStateData(int value);
    CgRenderStateData(const glm::ivec2 &value);
    CgRenderStateData(const glm::ivec3 &value);
    CgRenderStateData(const glm::ivec4 &value);
    CgRenderStateData(float value);
    CgRenderStateData(const glm::vec2 &value);
    CgRenderStateData(const glm::vec3 &value);
    CgRenderStateData(const glm::vec4 &value);
};

/// Typedef for data structure for storing uniform block data info
typedef CgUnorderedMap<GLuint, CgUniformBlockData>::type CgUniformBlockDataInfo;

/// Typedef for data structure for storing render state info
typedef CgUnorderedMap<GLenum, CgRenderStateData>::type CgRenderStateInfo;

/// Struct for drawables
struct CgDrawable {
    std::uint32_t program;  ///< GL handle
    std::uint32_t vao;  ///< GL handle
    CgTextureBinding textures[CG_MAX_DRAWABLE_TEXTURES];
    CgUniformBlockDataInfo *uniformBlockData;  ///< Weak ref. to uniform data 
    CgRenderStateInfo *dynamicStateInfo;  ///< Weak ref. to custom render state
    CgDrawInfo drawInfo;
    std::uint64_t renderMask;  ///< Bit-mask for enabling/disabling passes
    glm::mat4 modelMatrix;

    CgDrawable() :
        uniformBlockData(nullptr),
        dynamicStateInfo(nullptr),
        renderMask(0uLL)
    {}
};

/// Initialize static RenderState::setters with the default setters above
void cgInitRenderStateSetters();

/// Render multiple drawable at once. This function affects the following OpenGL
/// state: GL_TEXTURE_BINDING_*, GL_UNIFORM_BUFFER_BINDING (indexed)
void cgRendererDraw(const CgCamera &camera, const CgDrawable *drawables,
                    const size_t drawableCount, const float time,
                    const CgRenderStateInfo *pipelineStateInfo=nullptr);

} // namespace cg
