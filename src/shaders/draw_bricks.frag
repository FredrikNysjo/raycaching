#version 450

layout(location = 0) out vec4 frag_color;

in vec3 v_uvw;
flat in int v_lod;

const vec3 g_lod_colors[] = vec3[](
    vec3(0.90, 0.30, 0.30),
    vec3(0.30, 0.90, 0.30),
    vec3(0.30, 0.30, 0.90)
);

// Anti-aliased step function from "OpenGL Insights", Chapter 7
float aastep(float threshold, float value)
{
    float afwidth = 0.7 * length(vec2(dFdx(value), dFdy(value)));
    return smoothstep(threshold - afwidth, threshold + afwidth, value);
}

void main()
{
    vec3 color = vec3(0.95);

    //color *= g_lod_colors[(v_lod + 1) % 3];
    //color *= vec3(dot(v_uvw, vec3(0.2, 0.7, 0.1)));
    color *= v_uvw;

    vec3 d1 = abs(v_uvw - 0.5);
    int side = 0;
    if (d1.y > d1.x && d1.y > d1.z) { side = 1; }
    if (d1.z > d1.x && d1.z > d1.y) { side = 2; }
    vec3 d2 = abs(fract(v_uvw * 2.0) - 0.5);
    float grid_color = 1.0 - aastep(0.45, max(d2[(side + 1) % 3], d2[(side + 2) % 3]));
    float fade = 0.6;
    color.rgb *= mix(1.0, grid_color, fade);

    frag_color = vec4(color, 1.0);
}
