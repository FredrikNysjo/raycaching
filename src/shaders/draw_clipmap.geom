#version 450

#define SPLAT_SIZE 5

layout(binding = 1, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    float time;
} u_scene;

layout(binding = 3, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 6, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(binding = 0) uniform usamplerBuffer u_splat_texture;

layout(points) in;
layout(line_strip, max_vertices = 16) out;

flat in int v_vertex_id[];

flat out int v_lod;

const vec3 g_wirecube[] = vec3[](
    vec3(0, 0, 1),
    vec3(1, 0, 1),
    vec3(1, 0, 0),
    vec3(0, 0, 0),
    vec3(0, 0, 1),
    vec3(0, 1, 1),
    vec3(1, 1, 1),
    vec3(1, 0, 1),  // End of first line strip
    vec3(0, 1, 0),
    vec3(0, 1, 1),
    vec3(1, 1, 1),
    vec3(1, 1, 0),
    vec3(0, 1, 0),
    vec3(0, 0, 0),
    vec3(1, 0, 0),
    vec3(1, 1, 0)  // End of second line strip
);

vec4 clipmapFromWorld(in vec3 p, in vec3 clipmap_center, in float lod)
{
    vec3 q = (p - u_clipmap.center.xyz) / u_clipmap.radius;
    vec3 pos = u_clipmap.dimensions[0] * ((q / exp2(lod)) * 0.5 + 0.5);

    return vec4(pos, lod);
}

vec4 worldFromClipmap(in vec4 p, in vec3 clipmap_center)
{
    vec3 q = ((p.xyz / u_clipmap.dimensions[0]) - 0.5) * (exp2(p.w) / 0.5);
    vec3 pos = (q * u_clipmap.radius) + u_clipmap.center.xyz;

    return vec4(pos, 1.0);
}

void main()
{
    // Fetch point data
    uvec4 header = texelFetch(u_splat_texture, 0);
    int offset = 3 + SPLAT_SIZE * int((v_vertex_id[0] + header.r) % header.b);
    uvec4 data[SPLAT_SIZE];
    data[0] = texelFetch(u_splat_texture, offset + 0);
    if (bool(data[0].w & (1 << 31))) { return; }  // Skip rendering hidden layer

    // Unpack attributes
    vec4 world_pos = vec4(uintBitsToFloat(data[0].xyz), 1.0);
    int lod = int(data[0].w & 0xf);

    vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, u_clipmap.center.xyz, lod);

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 8; ++j) {
            vec4 clipmap_pos1 = vec4(floor(clipmap_pos.xyz) + g_wirecube[8 * i + j], lod);
            vec4 world_pos1 = worldFromClipmap(clipmap_pos1, u_clipmap.center.xyz);

            gl_Position = u_scene.projection * u_scene.view * world_pos1;
            v_lod = lod;
            EmitVertex();
        }
        EndPrimitive();
    }
}
