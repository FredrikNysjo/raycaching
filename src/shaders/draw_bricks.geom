#version 450

#define BRICK_SIZE 5

layout(binding = 1, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    float time;
} u_scene;

layout(binding = 3, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 6, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(binding = 0) uniform usamplerBuffer u_brick_texture;

layout(points) in;
layout(triangle_strip, max_vertices = 14) out;

flat in int v_vertex_id[];

out vec3 v_uvw;
flat out int v_lod;

// From "Optimizing Triangle Strips for Fast Rendering" by Evans et al.
const int g_cube_strip[] = int[](
    3, 2, 7, 6, 4, 2, 0, 3, 1, 7, 5, 4, 1, 0
);

void main()
{
    // Fetch point data
    uvec4 header = texelFetch(u_brick_texture, 0);
    int brick_ofs = 3 + BRICK_SIZE * int((v_vertex_id[0] + header.r) % header.b);
    uvec4 brick_data = texelFetch(u_brick_texture, brick_ofs);
    vec4 world_pos = vec4(uintBitsToFloat(brick_data.xyz), 1.0);
    int lod = int(brick_data.w & 0xf);

    float brick_radius = (1.0 / u_clipmap.subres) * exp2(lod) * u_clipmap.spacing;

    for (int i = 0; i < 14; ++i) {
        int idx = g_cube_strip[i];
        vec3 cube_pos = vec3(idx & 1, (idx >> 1) & 1, (idx >> 2));
        vec3 world_pos_ = world_pos.xyz + (cube_pos - 0.5) * brick_radius;
        gl_Position = u_scene.projection * u_scene.view * vec4(world_pos_, 1.0);
        v_uvw = cube_pos;
        v_lod = lod;
        EmitVertex();
    }
    EndPrimitive();
}
