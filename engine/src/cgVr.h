/// @file    cgVr.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#ifdef CG_WITH_OVR_SDK
#include "cgVr_OVR.h"
#endif // CG_WITH_OVR_SDK

#ifndef CG_WITH_OVR_SDK

#include "cgGL.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <openvr.h>

namespace cg {

// Forward declarations
struct CgHmd;
struct CgHmdRenderModel;

/// Typedefs for event callbacks
typedef void (*CgHmdButtonCallback_t)(CgHmd *, vr::VREvent_t *, void *);

/// Struct for HMD rendering
struct CgHmd {
    vr::IVRSystem *hmd;
    vr::TrackedDevicePose_t poses[vr::k_unMaxTrackedDeviceCount];
    glm::uvec2 renderTargetSize;  ///< Recommended render target size
    GLuint renderTarget;  ///< Main render target texture (RGBA texture)
    GLuint depthTexture;  ///< Depth texture for when we want to draw directly
                          ///< to the HMD framebuffer (for debugging, etc.)
    GLuint framebuffer;  ///< HMD framebuffer that we can render or blit to
    glm::mat4 poseMatrix[vr::k_unMaxTrackedDeviceCount];
    glm::mat4 projectionMatrix[2];
    glm::mat4 viewMatrix[2];
    glm::vec4 viewport[2];
	unsigned eye;
    CgHmdButtonCallback_t buttonCallback;

    CgHmd() :
        buttonCallback(nullptr)
    {}
};

/// Struct for HMD render models
struct CgHmdRenderModel {
    GLuint vao;
    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint texture;
    int numVertices;
    int numIndices;
};

/// Initialize HMD
CgStatus cgHmdInit(CgHmd *hmd);

/// Shutdown HMD
CgStatus cgHmdShutdown(CgHmd *hmd);

/// Initialize VR compositor and obtain recommended HMD render target size
CgStatus cgHmdConfigureRendering(CgHmd *hmd);

/// Create HMD render targets and framebuffer for rendering
CgStatus cgHmdCreateRenderTarget(CgHmd *hmd);

/// Begin HMD frame
void cgHmdBeginFrame(CgHmd *hmd);

/// End HMD frame
void cgHmdEndFrame(CgHmd *hmd);

/// Process HMD events
void cgHmdProcessEvents(CgHmd *hmd, void *userdata);

/// Load render model for indexed device (headset, controller)
void cgHmdLoadRenderModel(CgHmd *hmd, CgHmdRenderModel *renderModel, const int index=0);

// Example event callback:
//
// void controllerButtonCallback(cg::CgHmd *hmd, vr::VREvent_t *event, void *userdata)
// {
//     Context *ctx = static_cast<Context *>(userdata);
// 
//     if (event->data.controller.button == vr::k_EButton_SteamVR_Trigger) {
//         if (event->eventType == vr::VREvent_ButtonPress) {
//             ctx->gripState = true;
//         }
//         else if (event->eventType == vr::VREvent_ButtonUnpress) {
//             ctx->gripState = false;
//         }
//     }
// }

} // namespace cg

#endif // CG_WITH_OVR_SDK
