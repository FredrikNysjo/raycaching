#version 450

layout(location = 0) out vec4 frag_color;

flat in int v_lod;

// Cosine based palette from http://www.iquilezles.org/www/articles/palettes/palettes.htm
vec3 palette(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
    return a + b * cos(6.28318 * (c * t + d));
}
#define PALETTE_RAINBOW(t) palette(t, vec3(0.5), vec3(0.5), vec3(1.0), vec3(0.0, 0.33, 0.67))

void main()
{
    frag_color = vec4(PALETTE_RAINBOW(v_lod / 7.0), 1.0);
}
