/// @file    cgMesh.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"
#include "cgGL.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

namespace cg {

/// Struct for basic indexed triangle mesh
struct CgMesh {
    CgVector<glm::vec3>::type vertices;
    CgVector<glm::vec3>::type colors;
    CgVector<glm::vec3>::type normals;
    CgVector<glm::vec3>::type texcoords;
    CgVector<std::uint32_t>::type indices;
};

/// Struct for representing vertex array object (VAO) created from mesh
struct CgMeshVAO {
    GLuint vao;
    GLuint vertexVBO;
    GLuint colorVBO;
    GLuint normalVBO;
    GLuint texcoordVBO;
    GLuint indexVBO;
    int numVertices;
    int numIndices;
    GLenum indexType;
    GLenum primitiveType;
};

/// Create mesh OpenGL buffers and VAO for mesh data
void cgCreateMeshVAO(const CgMesh &mesh, CgMeshVAO *meshVAO);

} // namespace cg
