#version 450
#extension GL_ARB_gpu_shader_int64 : enable
#extension GL_NV_shader_atomic_int64 : enable

#define RAYCAST_MODE_CREATE 0
#define RAYCAST_MODE_UPDATE 1
#define RAYCAST_MODE_CLEAR 2
#define RAYCAST_MODE_INSERT 3
#define RAYCAST_MODE_UPDATE_EXPERIMENTAL 4

#define BRICK_SIZE 5

#define TILE_SIZE_X 16
#define TILE_SIZE_Y 16
#define NUM_TILES (TILE_SIZE_X * TILE_SIZE_Y)

//#define USE_ANISOTROPIC_CLIPMAP_VOXELS  // Requires 64-bit integer atomics
#ifdef USE_ANISOTROPIC_CLIPMAP_VOXELS
#define CLIPMAP_VALUE_TYPE uint64_t
#else // USE_ANISOTROPIC_CLIPMAP_VOXELS
#define CLIPMAP_VALUE_TYPE uint
#endif // USE_ANISOTROPIC_CLIPMAP_VOXELS

layout(binding = 4, std140) uniform RaycastUniforms {
    mat4 projection[2];
    mat4 view[2];
    mat4 view_inv[2];
    int num_occlusion_rays;
    int num_shadow_rays;
    int max_steps;
    int raycasting_enabled;
    int num_importance_rays;
    float step_length;
    int use_normal_refinement;
    int num_rays_per_tile;
    int frame;
} u_raycast;

layout(binding = 5, std140) uniform VolumeUniforms {
    mat4 world_from_image;
    mat4 image_from_world;
    vec4 spacing;
    vec4 clip[2];
    float threshold;
} u_volume;

layout(binding = 6, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(location = 1) uniform int u_mode;
layout(location = 2) uniform int u_importance_samples = 0;

layout(binding = 0) uniform sampler3D u_volume_texture;
layout(binding = 2) uniform samplerBuffer u_halton;

layout(std430, binding = 2) restrict buffer ResultBuffer {
    int count;
    int total_count;
    int pad[2];
    uvec4 data[];
} u_result_buffer;

layout(std430, binding = 3) restrict buffer ClipmapBuffer {
    CLIPMAP_VALUE_TYPE data[];
} u_clipmap_buffer;

layout(std430, binding = 4) restrict buffer BrickCache {
    int start;
    int count;
    int capacity;
    int pad;
    ivec4 cmd[2];  // DrawArraysInstancedCommand
    uvec4 data[];
} u_brick_cache;

layout(std430, binding = 5) restrict buffer TimestampBuffer {
    uint data[];
} u_timestamp_buffer;

layout(std430, binding = 6) restrict buffer TileBuffer {
    int data[];
} u_tile_buffer;

uvec4 g_data[BRICK_SIZE];
vec3 g_normal;
float g_coverage;

float imageLinear(vec3 p)
{
    return textureLod(u_volume_texture, p, 0.0).r;
}

vec3 imageGradient(vec3 p, vec3 delta)
{
    vec3 grad = vec3(0.0);
    grad.x -= imageLinear(p - vec3(delta.x, 0.0, 0.0));
    grad.x += imageLinear(p + vec3(delta.x, 0.0, 0.0));
    grad.y -= imageLinear(p - vec3(0.0, delta.y, 0.0));
    grad.y += imageLinear(p + vec3(0.0, delta.y, 0.0));
    grad.z -= imageLinear(p - vec3(0.0, 0.0, delta.z));
    grad.z += imageLinear(p + vec3(0.0, 0.0, delta.z));

    return grad;
}

vec3 imageNormal(vec3 p, vec3 spacing)
{
    vec3 delta = 1.0 / vec3(textureSize(u_volume_texture, 0));
    delta *= min(spacing.x, min(spacing.y, spacing.z)) / spacing;
    vec3 grad = imageGradient(p, delta);

    return -normalize(grad);
}

vec4 imageRayCast(vec3 ray_origin, vec3 ray_dir, float step,
                  int max_steps, float threshold)
{
    vec3 p = ray_origin;
    float value = imageLinear(p);
    for (int i = 0; i < max_steps; ++i) {
        value = imageLinear(p);
        if (value >= threshold) { break; }
        p += (step * ray_dir);
    }

    int num_refinements = 4;  // Set to 0 to disable
    if (value >= threshold) {
        vec3 q = p - (step * ray_dir);
        for (int i = 0; i < num_refinements; ++i) {
            vec3 midpoint = (q + p) * 0.5;
            value = imageLinear(midpoint);
            if (value >= threshold) { p = midpoint; }
            if (value < threshold) { q = midpoint; }
        }
        value = imageLinear(p);
    }

    return vec4(p, value);
}

// Intersect image-space ray with 1-unit AABB centered at vec3(0.5), using
// method from Chapter 5.3 in "Real-Time Collision Detection" book
vec2 imageRayBoxIntersect(vec3 ray_origin, vec3 ray_dir)
{
    float tmin = 0.0;
    float tmax = 9999.0;
    for (int i = 0; i < 3; ++i) {
        float odd = 1.0 / (abs(ray_dir[i]) > 1e-5 ? ray_dir[i] : 1e-5);
        vec2 t = (vec2(0.0, 1.0) - ray_origin[i]) * odd;
        tmin = max(tmin, (t[0] > t[1] ? t[1] : t[0]));
        tmax = min(tmax, (t[0] > t[1] ? t[0] : t[1]));
    }
    if (tmin > tmax) { tmin = tmax = -1.0; }

    return vec2(tmin, tmax);
}

vec3 reconstructViewPos(vec3 ndc_pos, mat4 projection)
{
    float a = projection[0][0];
    float b = projection[1][1];
    float c = projection[2][2];
    float d = projection[3][2];
    float e = projection[2][0];
    float f = projection[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);

    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

vec4 clipmapFromWorld(vec3 p)
{
    vec3 q = (p - u_clipmap.center.xyz) / u_clipmap.radius;
    float lod = ceil(log2(max(1.0, max(abs(q.x), max(abs(q.y), abs(q.z))))));
    vec3 pos = u_clipmap.dimensions[0] * ((q / exp2(lod)) * 0.5 + 0.5);

    return vec4(pos, lod);
}

vec4 worldFromClipmap(vec4 p)
{
    vec3 q = ((p.xyz / u_clipmap.dimensions[0]) - 0.5) * (exp2(p.w) / 0.5);
    vec3 pos = (q * u_clipmap.radius) + u_clipmap.center.xyz;

    return vec4(pos, 1.0);
}

int clipmapVoxelIndex(vec4 clipmap_pos, ivec4 clipmap_size)
{
    ivec4 p = ivec4(clipmap_pos);
    return clipmap_size.x * (clipmap_size.y * (clipmap_size.z * p.w + p.z) + p.y) + p.x;
}

CLIPMAP_VALUE_TYPE clipmapSubvoxelBit(vec4 clipmap_pos)
{
    vec3 ndc_pos = (clipmap_pos.xyz - (0.5 * u_clipmap.dimensions[0])) / (0.5 * u_clipmap.dimensions[0]);
    float dist = max(abs(ndc_pos.x), max(abs(ndc_pos.y), abs(ndc_pos.z)));

    CLIPMAP_VALUE_TYPE mask = 0;
    int sz = u_clipmap.subres;
    vec3 subvoxel = floor(sz * fract(clipmap_pos.xyz));
    mask = CLIPMAP_VALUE_TYPE(1) << uint(int(subvoxel.z) * (sz * sz) + int(subvoxel.y) * sz + int(subvoxel.x));

    return mask;
}

float clipmapSubvoxelRadius(vec4 clipmap_pos)
{
    return (0.5 / u_clipmap.subres) * exp2(clipmap_pos.w) * u_clipmap.spacing;
}

vec4 clipmapSubvoxelWorldPos(vec3 p)
{
    vec4 clipmap_pos = clipmapFromWorld(p);
    int sz = u_clipmap.subres;
    vec4 clipmap_pos_new = vec4((floor(sz * clipmap_pos.xyz) + 0.5) / sz, clipmap_pos.w);

    return worldFromClipmap(clipmap_pos_new);
}

// Clear clipmap subvoxel (using atomics)
void clipmapClearSubvoxel(vec4 clipmap_pos)
{
    int voxel_idx = clipmapVoxelIndex(clipmap_pos, u_clipmap.dimensions);
    CLIPMAP_VALUE_TYPE subvoxel_bit = clipmapSubvoxelBit(clipmap_pos);
    atomicAnd(u_clipmap_buffer.data[voxel_idx], ~subvoxel_bit);
}

// Write and test clipmap subvoxel (using atomics). Returns true if the test
// succeeds, false otherwise.
bool clipmapWriteAndTestSubvoxel(vec4 clipmap_pos)
{
     int voxel_idx = clipmapVoxelIndex(clipmap_pos, u_clipmap.dimensions);
     CLIPMAP_VALUE_TYPE subvoxel_bit = clipmapSubvoxelBit(clipmap_pos);
     CLIPMAP_VALUE_TYPE voxel_mask = atomicOr(u_clipmap_buffer.data[voxel_idx], subvoxel_bit);

     return (voxel_mask & subvoxel_bit) == CLIPMAP_VALUE_TYPE(0);
}

// Updates timestamp of clipmap voxel
void timestampUpdate(vec4 clipmap_pos, uint timestamp)
{
    ivec4 ts_pos = ivec4(clipmap_pos.xyz * 0.5, clipmap_pos.w);
    int ts_idx = 64 * (64 * (64 * ts_pos.w + ts_pos.z) + ts_pos.y) + ts_pos.x;

    u_timestamp_buffer.data[ts_idx] = timestamp;
}

// Fetches timestamp of clipmap voxel
uint timestampFetch(vec4 clipmap_pos)
{
    ivec4 ts_pos = ivec4(clipmap_pos.xyz * 0.5, clipmap_pos.w);
    int ts_idx = 64 * (64 * (64 * ts_pos.w + ts_pos.z) + ts_pos.y) + ts_pos.x;

    return u_timestamp_buffer.data[ts_idx];
}

// Convert normalized Cartesian XYZ to normalized spherical XY
vec2 cartToSphere(vec3 c)
{
    return (vec2(atan(c.y, c.x), acos(c.z)) / 3.141592) * 0.5 + 0.5;
}

// Convert normalized spherical XY to normalized Cartesian XYZ
vec3 sphereToCart(vec2 s)
{
    float theta = (s.x * 2.0 - 1.0) * 3.141592;
    float phi = (s.y * 2.0 - 1.0) * 3.141592;

    return vec3(cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi));
}

float computeShadowRay(vec3 world_pos, float subvox_radius, int sub_idx)
{
    vec3 sub_pos = vec3(sub_idx & 1, (sub_idx >> 1) & 1, sub_idx >> 2) - 0.5;
    sub_pos += (g_coverage - 0.5) * g_normal;
    vec4 p = u_volume.image_from_world * vec4(world_pos + sub_pos * subvox_radius, 1.0);
    vec4 L = u_volume.image_from_world * (u_raycast.view_inv[0] * vec4(1.0, 2.0, 3.0, 1.0));
    p.xyz += u_raycast.step_length * g_normal;

    float ray_step = u_raycast.step_length;
    vec3 ray_dir = normalize(L.xyz - p.xyz);
    vec3 ray_start = p.xyz + ray_step * ray_dir;
    float threshold = max(u_volume.threshold, imageLinear(ray_start));
    vec4 t = imageRayCast(ray_start, ray_dir, ray_step, 100, threshold);

    float visibility = (100.0 - length(t.xyz - ray_start) / ray_step) / 100.0;
    return visibility;
}

vec3 squareToSphere(vec2 uv)
{
    float s = uv.s * 2.0 * 3.141592;
    float t = uv.t * 2.0 - 1.0;
    float r = sqrt(1.0 - t * t);
    return vec3(sin(s) * r, cos(s) * r, t);
}

float computeAORays(vec3 world_pos, float subvox_radius, int sub_idx)
{
    vec3 sub_pos = vec3(sub_idx & 1, (sub_idx >> 1) & 1, sub_idx >> 2) - 0.5;
    sub_pos += (g_coverage - 0.5) * g_normal;
    vec4 p = u_volume.image_from_world * vec4(world_pos + sub_pos * subvox_radius, 1.0);
    p.xyz += u_raycast.step_length * g_normal;

    float visibility = 0.0;
    float ray_step = u_raycast.step_length * 2.0;
    for (int i = 0; i < 8; ++i) {
        vec3 ray_dir = squareToSphere(texelFetch(u_halton, i).rg);
        ray_dir = dot(ray_dir, g_normal) < 0.0 ? -ray_dir : ray_dir;
        vec3 ray_start = p.xyz + ray_step * ray_dir;
        float threshold = max(u_volume.threshold, imageLinear(ray_start));
        vec4 t = imageRayCast(ray_start, ray_dir, ray_step, 10, threshold);
        visibility += (10.0 - length(t.xyz - ray_start) / ray_step) / 10.0;
    }
    visibility /= 8.0;

    return visibility;
}

// Compute coverage and normal data for 2^3 brick. Brick position and radius
// shall be in world coordinates.
float updateBrick(vec3 world_pos, float subvox_radius, int sub_idx)
{
    vec3 image_pos = vec3(u_volume.image_from_world * vec4(world_pos, 1.0));
    vec3 image_radius = mat3(u_volume.image_from_world) * vec3(subvox_radius);

    vec3 sub_pos = vec3(sub_idx & 1, (sub_idx >> 1) & 1, sub_idx >> 2);
    vec3 p = image_pos + (sub_pos - 0.5) * image_radius;

    float taps[27];
    int mask = 0;
    for (int z = 0; z < 3; ++z) {
        for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 3; ++x) {
                int idx = z * 9 + y * 3 + x;  // Sample index
                vec3 delta = vec3(x, y, z) * 0.5 - 0.5;
                taps[idx] = imageLinear(p + delta * image_radius);
                mask |= (taps[idx] >= u_volume.threshold) ? (1 << idx) : 0;
            }
        }
    }

    vec3 dir = normalize(world_pos - u_clipmap.center.xyz);
    vec3 normal_sum = vec3(0.0);
    float dist_sum = 0.0;
    float weight_sum = 0.0;
    for (int z = 0; z < 2; ++z) {
        for (int y = 0; y < 2; ++y) {
            for (int x = 0; x < 2; ++x) {
                int idx = z * 9 + y * 3 + x;
                vec3 grad_forward = vec3(taps[idx + 1], taps[idx + 3], taps[idx + 9]) - taps[idx];
                float midpoint = (taps[idx] + taps[idx + 1] + taps[idx + 3] + taps[idx + 4] +
                                  taps[idx + 9] + taps[idx + 10] + taps[idx + 12] + taps[idx + 13]) / 8.0;
                float t = (u_volume.threshold - midpoint) / length(grad_forward);
                float d = clamp(-t, -0.5, 0.5) + 0.5;

                int corner_mask = (0x361b << idx);  // :D
                int combined_mask = (corner_mask & mask);
                float weight = (combined_mask > 0 && combined_mask != corner_mask) ? 1.0 : 0.0;
                if (dot(grad_forward, dir) < -0.1) { weight = 0.0; }
                dist_sum += weight * d;
                normal_sum += weight * -normalize(grad_forward + 1e-5);
                weight_sum += weight;
            }
        }
    }
    vec3 normal = normalize(normal_sum / max(1.0, weight_sum));
    float dist = dist_sum / max(1.0, weight_sum);

    if (bool(u_raycast.use_normal_refinement)) {
        vec3 delta = (dist - 0.5) * normal;
        normal = imageNormal(p + delta * image_radius, u_volume.spacing.xyz);
    }

    float c = (weight_sum > 0.0) ? clamp(dist, 0.02, 0.98) : 0.0;
    float o = weight_sum / 8.0;
    vec2 n = cartToSphere(normal);
    g_data[1 + (sub_idx >> 2)][sub_idx & 3] = packUnorm4x8(vec4(c, n.x, n.y, o));

    g_normal = normal;
    g_coverage = dist;
    return c;
}

uint brickMask()
{
    uint mask = 0u;
    for (int i = 0; i < 8; ++i) {
        mask |= bool(g_data[i / 4 + 1][i % 4] & 0xffu) ? (1u << i) : 0u;
    }

    return mask;
}

int appendBrickToResultBuffer()
{
    int result_idx = atomicAdd(u_result_buffer.count, 1);
    for (int i = 0; i < BRICK_SIZE; ++i) {
        u_result_buffer.data[BRICK_SIZE * result_idx + i] = g_data[i];
    }

    return result_idx;
}

void main()
{
    // Fetch brick data (negative index used for fetching zeros)
    int brick_idx = (u_brick_cache.start + gl_VertexID) % u_brick_cache.capacity;
    brick_idx = (u_mode == RAYCAST_MODE_CREATE) ? -BRICK_SIZE : brick_idx;
    for (int i = 0; i < BRICK_SIZE; ++i) {
        g_data[i] = u_brick_cache.data[BRICK_SIZE * brick_idx + i];
    }

    // Unpack brick data
    vec4 world_pos = vec4(uintBitsToFloat(g_data[0].xyz), 1.0);
    int lod = int(g_data[0].w & 0xf);

    if (u_mode == RAYCAST_MODE_CREATE) {
        if (gl_VertexID >= (u_brick_cache.capacity - u_brick_cache.count)) { return; }

        // Compute UV for ray
        int tile_idx = gl_VertexID / u_raycast.num_rays_per_tile;
        int eye_idx = gl_VertexID % 2;
        if (bool(u_importance_samples)) {
            tile_idx = u_tile_buffer.data[3 * NUM_TILES + 1 + tile_idx];
        }
        vec2 tile = vec2(tile_idx % TILE_SIZE_X, tile_idx / TILE_SIZE_X);
        int seed = atomicAdd(u_tile_buffer.data[tile_idx * 3 + eye_idx], 1) + (2048 * eye_idx);
        vec2 uv = (tile + texelFetch(u_halton, seed % 4096).rg) / vec2(TILE_SIZE_X, TILE_SIZE_Y);

        // Compute ray start and stop position
        vec3 ndc_pos = vec3(uv, 0.0) * 2.0 - 1.0;
        vec3 view_pos = reconstructViewPos(ndc_pos, u_raycast.projection[eye_idx]);
        mat4 image_from_view = u_volume.image_from_world * u_raycast.view_inv[eye_idx];
        vec3 image_pos = vec3(image_from_view * vec4(view_pos, 1.0));
        vec3 ray_dir = normalize(mat3(image_from_view) * view_pos);
        vec2 t = imageRayBoxIntersect(image_pos, ray_dir);
        vec3 ray_start = image_pos + ray_dir * t[0];
        vec3 ray_stop = image_pos + ray_dir * t[1];

        // Do raycasting
        float ray_length = length(ray_stop - ray_start);
        int num_steps = int(ray_length / u_raycast.step_length);
        float jitter = texelFetch(u_halton, gl_VertexID % 1024).r;
        ray_start -= (jitter * u_raycast.step_length) * ray_dir;
        vec4 hit = imageRayCast(ray_start, ray_dir, u_raycast.step_length,
                num_steps, u_volume.threshold);
        if (hit.w < u_volume.threshold) { return; }

        // Check that intersection point is inside clipmap
        world_pos = u_volume.world_from_image * vec4(hit.xyz, 1.0);
        world_pos = clipmapSubvoxelWorldPos(world_pos.xyz);
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz);
        if (int(clipmap_pos.w) >= u_clipmap.dimensions.w) { return; }

        timestampUpdate(clipmap_pos, uint(u_raycast.frame));
        if (!clipmapWriteAndTestSubvoxel(clipmap_pos)) { return; }

        if (!bool(u_importance_samples)) {
            if (atomicMax(u_tile_buffer.data[tile_idx * 3 + 2], u_raycast.frame) != u_raycast.frame) {
                int count = atomicAdd(u_tile_buffer.data[3 * NUM_TILES], 1);
                u_tile_buffer.data[3 * NUM_TILES + 1 + (count % NUM_TILES)] = tile_idx;
            }
        }

        // Pack brick data
        g_data[0].xyz = floatBitsToUint(world_pos.xyz);
        g_data[0].w = uint(clipmap_pos.w);

        int result_idx = appendBrickToResultBuffer();
    }

    if (u_mode == RAYCAST_MODE_UPDATE) {
        if (gl_VertexID >= u_brick_cache.count) { return; }
        atomicAdd(u_result_buffer.total_count, 1);

        // Snap to clipmap subvoxel
        world_pos = clipmapSubvoxelWorldPos(world_pos.xyz);
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz);
        if (brickMask() == 0u) { return; }

        // Do clipmap test to see if brick can be discarded
        uint voxel_timestamp = timestampFetch(clipmap_pos);
        if ((uint(u_raycast.frame) - voxel_timestamp) > 60u) { return; }
        if (!clipmapWriteAndTestSubvoxel(clipmap_pos)) { return; }

        // Pack brick data
        g_data[0].xyz = floatBitsToUint(world_pos.xyz);
        g_data[0].w = uint(clipmap_pos.w);

        int result_idx = appendBrickToResultBuffer();
    }

    if (u_mode == RAYCAST_MODE_UPDATE_EXPERIMENTAL) {
        if ((gl_VertexID >> 3) >= u_result_buffer.count) { return; }

        // Fetch data from result buffer instead of brick cache
        int brick_idx = gl_VertexID >> 3;
        int sub_idx = gl_VertexID & 7;
        for (int i = 0; i < BRICK_SIZE; ++i) {
            g_data[i] = u_result_buffer.data[BRICK_SIZE * brick_idx + i];
        }

        // Unpack brick data
        vec4 world_pos = vec4(uintBitsToFloat(g_data[0].xyz), 1.0);
        int lod = int(g_data[0].w & 0xf);

        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz);
        float subvox_radius = clipmapSubvoxelRadius(clipmap_pos);
        float coverage = updateBrick(world_pos.xyz, subvox_radius, sub_idx);

        int coverage_bit = (coverage > 0.0 ? 1 : 0) << (8 + sub_idx);
        atomicOr(u_result_buffer.data[BRICK_SIZE * brick_idx].w, uint(coverage_bit));
        u_result_buffer.data[BRICK_SIZE * brick_idx + 1 + (sub_idx >> 2)][sub_idx & 3] =
            g_data[1 + (sub_idx >> 2)][sub_idx & 3];

        vec2 visibility = vec2(0.0);
        if (bool(u_raycast.num_shadow_rays) && coverage > 0.0) {
            visibility.x = computeShadowRay(world_pos.xyz, subvox_radius, sub_idx);
        }
        if (bool(u_raycast.num_occlusion_rays) && coverage > 0.0) {
            visibility.y = computeAORays(world_pos.xyz, subvox_radius, sub_idx);
        }
        u_result_buffer.data[BRICK_SIZE * brick_idx + 3 + (sub_idx >> 2)][sub_idx & 3] =
            packUnorm4x8(vec4(visibility, 0.0, 0.0));
    }

    if (u_mode == RAYCAST_MODE_CLEAR) {
        if (gl_VertexID >= u_brick_cache.count) { return; }

        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz);
        clipmapClearSubvoxel(clipmap_pos);

        // Save timestamp
        uint voxel_timestamp = timestampFetch(clipmap_pos);
        u_brick_cache.data[BRICK_SIZE * u_brick_cache.capacity + brick_idx][0] = voxel_timestamp;
    }

    if (u_mode == RAYCAST_MODE_INSERT) {
        if (gl_VertexID >= u_brick_cache.count) { return; }
        atomicAdd(u_result_buffer.total_count, 1);

        // Update LOD
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz);
        int lod_new = int(clipmap_pos.w);
        if (lod_new >= u_clipmap.dimensions.w) { return; }
        float subvox_radius = (0.5 / u_clipmap.subres) * exp2(lod) * u_clipmap.spacing;
        float subvox_radius_new = (0.5 / u_clipmap.subres) * exp2(lod_new) * u_clipmap.spacing;
        if (abs(subvox_radius - subvox_radius_new) < 1e-6) { subvox_radius_new = subvox_radius; }

        // Restore timestamp
        uint voxel_timestamp = u_brick_cache.data[BRICK_SIZE * u_brick_cache.capacity + brick_idx][0];
        timestampUpdate(clipmap_pos, voxel_timestamp);

        if (subvox_radius <= subvox_radius_new) {  // Re-use original brick
            if (!clipmapWriteAndTestSubvoxel(clipmap_pos)) { return; }

            world_pos = clipmapSubvoxelWorldPos(world_pos.xyz);

            // Pack brick data
            g_data[0].xyz = floatBitsToUint(world_pos.xyz);
            g_data[0].w = uint(clipmap_pos.w);

            int result_idx = appendBrickToResultBuffer();
        }
        else {  // Compute new sub-bricks from brick data
            uvec4 data_[BRICK_SIZE];
            for (int i = 0; i < BRICK_SIZE - 2; ++i) {
                data_[i] = g_data[i];
                g_data[i] = uvec4(0u);
            }

            for (int i = 0; i < 8; ++i) {
                // Unpack sub-point data
                uint packed0 = data_[i / 4 + 1][i % 4];
                if ((packed0 & 0xffu) == 0u) { continue; }
                float coverage = unpackUnorm4x8(packed0).x;
                vec3 world_normal = sphereToCart(unpackUnorm4x8(packed0).yz);

                // Compute position for new brick
                vec3 displacement = (coverage - 0.5) * world_normal;
                vec3 brick_pos = vec3(i & 1, (i >> 1) & 1, i >> 2) - 0.5;
                vec3 world_pos_new = world_pos.xyz + (brick_pos + displacement) * subvox_radius;
                vec4 clipmap_pos_new = clipmapFromWorld(world_pos_new);
                if (!clipmapWriteAndTestSubvoxel(clipmap_pos_new)) { continue; }

                // Pack brick data
                g_data[0].xyz = floatBitsToUint(world_pos_new);
                g_data[0].w = uint(clipmap_pos_new.w);
                g_data[1][0] = 0xffu | packed0;  // Flag brick to be rendered as single splat

                int result_idx = appendBrickToResultBuffer();
            }
        }
    }
}
