/// @file    gl_vr.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2018 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#ifdef _WIN32

#include "gl_vr.h"

#include <iostream>

bool cgHmdInit(CgHmd *hmd)
{
    vr::HmdError hmd_error;
    hmd->hmd = vr::VR_Init(&hmd_error, vr::VRApplication_Scene);
    if (!hmd->hmd) {
        std::cerr << vr::VR_GetVRInitErrorAsEnglishDescription(hmd_error) << std::endl;
        return false;
    }

    return true;
}

bool cgHmdShutdown(CgHmd *hmd)
{
	vr::VR_Shutdown();
    return true;
}

bool cgHmdConfigureRendering(CgHmd *hmd)
{
	hmd->hmd->GetRecommendedRenderTargetSize(&hmd->renderTargetSize[0], &hmd->renderTargetSize[1]);

    // Multiply width by two to obtain size for left + right eye combined
	hmd->renderTargetSize[0] *= 2;

    if (!vr::VRCompositor()) {
        std::cerr << "cgHmdConfigureRendering: Could not initialize VR compositor" << std::endl;
        return false;
    }

    return true;
}

bool cgHmdCreateRenderTarget(CgHmd *hmd)
{
    GLuint *color_texture = &hmd->renderTarget;
    GLuint *depth_texture = &hmd->depthTexture;
    GLuint *framebuffer = &hmd->framebuffer;
    glm::vec4 *viewports = &hmd->viewport[0];

    const auto width = hmd->renderTargetSize[0];
    const auto height = hmd->renderTargetSize[1];

    glCreateTextures(GL_TEXTURE_2D, 1, color_texture);
    glTextureParameteri(*color_texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*color_texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*color_texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTextureParameteri(*color_texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureStorage2D(*color_texture, 1, GL_SRGB8_ALPHA8, width, height);

    // Create depth + stencil texture in case we want to render something that
    // needs depth testing directly to the HMD framebuffer
    glCreateTextures(GL_TEXTURE_2D, 1, depth_texture);
    glTextureParameteri(*depth_texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*depth_texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*depth_texture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTextureParameteri(*depth_texture, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureStorage2D(*depth_texture, 1, GL_DEPTH24_STENCIL8, width, height);

    glCreateFramebuffers(1, framebuffer);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT0, *color_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_DEPTH_STENCIL_ATTACHMENT, *depth_texture, 0);
    GLenum status = glCheckNamedFramebufferStatus(*framebuffer, GL_DRAW_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "cgHmdCreateRenderTarget: Incomplete framebuffer\n";
    }

    // Compute left and right viewport rectangles
    viewports[0] = glm::vec4(0.0f, 0.0f, width * 0.5f, height);
    viewports[1] = glm::vec4(width * 0.5f, 0.0f, width * 0.5f, height);

    return true;
}

void cgHmdBeginFrame(CgHmd *hmd)
{
	// Begin HMD frame
	vr::VRCompositor()->WaitGetPoses(&hmd->poses[0], vr::k_unMaxTrackedDeviceCount, nullptr, 0);

	// Obtain pose matrices of tracked devices
    for (uint32_t di = 0; di < vr::k_unMaxTrackedDeviceCount; ++di) {
        vr::HmdMatrix34_t pose = hmd->poses[di].mDeviceToAbsoluteTracking;
        // Convert OpenVR row matrix to OpenGL column matrix
        hmd->poseMatrix[di] = glm::mat4();
	    for (uint32_t i = 0; i < 3; ++i) {
	    	for (uint32_t j = 0; j < 4; ++j) {
	    		hmd->poseMatrix[di][j][i] = pose.m[i][j];
	    	}
	    }
    }

    // Obtain projection matrices for left and right eye
    vr::HmdMatrix44_t projectionMatrix[2];
    projectionMatrix[0] = hmd->hmd->GetProjectionMatrix(vr::Eye_Left, 0.1f, 10.0f, vr::API_OpenGL);
    projectionMatrix[1] = hmd->hmd->GetProjectionMatrix(vr::Eye_Right, 0.1f, 10.0f, vr::API_OpenGL);
    hmd->projectionMatrix[0] = glm::transpose((glm::mat4 &)projectionMatrix[0]);
    hmd->projectionMatrix[1] = glm::transpose((glm::mat4 &)projectionMatrix[1]);

    // Compute view matrices for left and right eye
    vr::HmdMatrix34_t matEyeLeft = hmd->hmd->GetEyeToHeadTransform(vr::Eye_Left);
    vr::HmdMatrix34_t matEyeRight = hmd->hmd->GetEyeToHeadTransform(vr::Eye_Right);
    hmd->viewMatrix[0] = glm::inverse(glm::mat4(glm::transpose((glm::mat3x4 &)matEyeLeft))) *
                         glm::inverse(hmd->poseMatrix[vr::k_unTrackedDeviceIndex_Hmd]);
    hmd->viewMatrix[1] = glm::inverse(glm::mat4(glm::transpose((glm::mat3x4 &)matEyeRight))) *
                         glm::inverse(hmd->poseMatrix[vr::k_unTrackedDeviceIndex_Hmd]);
}

void cgHmdEndFrame(CgHmd *hmd)
{
	// Create descriptors for render targets
	vr::Texture_t eye_texture[2];
	vr::VRTextureBounds_t eye_bounds[2];
	eye_texture[0] = { (void *)(std::uintptr_t)hmd->renderTarget, vr::API_OpenGL, vr::ColorSpace_Gamma };
	eye_bounds[0] = { 0.0f, 0.0f, 0.5f, 1.0f };
	eye_texture[1] = { (void *)(std::uintptr_t)hmd->renderTarget, vr::API_OpenGL, vr::ColorSpace_Gamma };
	eye_bounds[1] = { 0.5f, 0.0f, 1.0f, 1.0f };

	// End HMD frame (present to display)
	vr::VRCompositor()->Submit(vr::Eye_Left, &eye_texture[0], &eye_bounds[0]);
	vr::VRCompositor()->Submit(vr::Eye_Right, &eye_texture[1], &eye_bounds[1]);
	vr::VRCompositor()->PostPresentHandoff();
}

void cgHmdProcessEvents(CgHmd *hmd, void *userdata)
{
    vr::VREvent_t event;
    while (hmd->hmd->PollNextEvent(&event, sizeof(event))) {
        // Handle event directly or call user-defined callback
        switch (event.eventType) {
        case vr::VREvent_ButtonPress:  // Fall through
        case vr::VREvent_ButtonUnpress:
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
            break;
        default:
            break;
        }
    }
}

void cgHmdLoadRenderModel(CgHmd *hmd, CgHmdRenderModel *dstRenderModel, int index)
{
    GLuint *vao = &dstRenderModel->vao;
    GLuint *vertex_buffer = &dstRenderModel->vertexBuffer;
    GLuint *index_buffer = &dstRenderModel->indexBuffer;
    GLuint *texture = &dstRenderModel->texture;

    // Obtain render model data for device index
    char str[80] = { 0 };
    hmd->hmd->GetStringTrackedDeviceProperty(index, vr::Prop_RenderModelName_String, str, 80, nullptr);
    vr::RenderModel_t *model = nullptr;
    while (vr::VRRenderModels()->LoadRenderModel_Async(str,
           &model) == vr::VRRenderModelError_Loading) {}

    // Obtain render model texture map
    vr::RenderModel_TextureMap_t *model_texture = nullptr;
    while (vr::VRRenderModels()->LoadTexture_Async(model->diffuseTextureId,
           &model_texture) == vr::VRRenderModelError_Loading) {}

	size_t num_vertex_bytes = model->unVertexCount * sizeof(model->rVertexData[0]);
    size_t vertex_stride = sizeof(model->rVertexData[0]);
	size_t num_index_bytes = (model->unTriangleCount * 3) * sizeof(model->rIndexData[0]);

    // Create GL objects for drawing the model

    glCreateBuffers(1, vertex_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *vertex_buffer);  // Workaround for DSA issue
	glNamedBufferData(*vertex_buffer, num_vertex_bytes, model->rVertexData, GL_STATIC_DRAW);

    glCreateBuffers(1, index_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *index_buffer);  // Workaround for DSA issue
	glNamedBufferData(*index_buffer, num_index_bytes, model->rIndexData, GL_STATIC_DRAW);

    glCreateVertexArrays(1, vao);
    glVertexArrayElementBuffer(*vao, *index_buffer);
    glVertexArrayVertexBuffer(*vao, 0, *vertex_buffer, 0, vertex_stride);
    glEnableVertexArrayAttrib(*vao, 0);
    glVertexArrayAttribFormat(*vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(*vao, 0, 0);
    glEnableVertexArrayAttrib(*vao, 1);
    glVertexArrayAttribFormat(*vao, 1, 3, GL_FLOAT, GL_FALSE, 12);
    glVertexArrayAttribBinding(*vao, 1, 0);
    glEnableVertexArrayAttrib(*vao, 2);
    glVertexArrayAttribFormat(*vao, 2, 2, GL_FLOAT, GL_FALSE, 24);
    glVertexArrayAttribBinding(*vao, 2, 0);

    glCreateTextures(GL_TEXTURE_2D, 1, texture);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTextureParameteri(*texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureStorage2D(*texture, 1, GL_RGBA8, model_texture->unWidth,model_texture->unHeight);
    glTextureSubImage2D(*texture, 0, 0, 0, model_texture->unWidth,model_texture->unHeight,
                        GL_RGBA, GL_UNSIGNED_BYTE, model_texture->rubTextureMapData);

    // Store additional information
    dstRenderModel->numVertices = model->unVertexCount;
    dstRenderModel->numIndices = model->unTriangleCount * 3;

    // Clean up
    vr::VRRenderModels()->FreeTexture(model_texture);
    vr::VRRenderModels()->FreeRenderModel(model);
}

#endif  // _WIN32
