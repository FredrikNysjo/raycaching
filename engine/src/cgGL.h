/// @file    cgGL.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"

#include <GL/glew.h>

#define CG_USE_EMULATED_DIRECT_STATE_ACCESS
#include "cgGLDirectStateAccess.h"

namespace cg {

// Typedefs for GL function pointers
typedef void (*cgGlGetv)(GLenum, void *);
typedef void (*cgGlSetv)(const void *);

/// Load GL API functions
CgStatus cgLoadGL();

/// @name String to GLenum lookup functions
///
/// Lookup GL enum from name string. If a lookup fails, the function will return
/// GL_INVALID_ENUM (not sure if this is the best way to handle it...)
///@{
GLenum cgGlBooleanFromString(const char *name);
GLenum cgGlFrontFaceFromString(const char *name);
GLenum cgGlShaderStageFromString(const char *name);
GLenum cgGlPrimitiveTypeFromString(const char *name);
GLenum cgGlBufferTargetFromString(const char *name);
GLenum cgGlBufferUsageFromString(const char *name);
GLenum cgGlFramebufferAttachmentFromString(const char *name);
GLenum cgGlTextureTargetFromString(const char *name);
GLenum cgGlTextureParameterFromString(const char *name);
GLenum cgGlTextureInternalFormatFromString(const char *name);
GLenum cgGlTextureFormatFromString(const char *name);
GLenum cgGlTextureTypeFromString(const char *name);
GLenum cgGlLogicOpModeFromString(const char *name);
GLenum cgGlBlendModeFromString(const char *name);
GLenum cgGlBlendFuncFromString(const char *name);
GLenum cgGlStateFromString(const char *name);
///@}

/// @name Custom GL explicit state setters
///
/// Set GL state explicitly, that is, set state queryable via glGet*. For example,
/// glSetPolygonMode will set the GL_POLYGON_MODE state, etc. Setters may call
/// glGet* internally to obtain other GL state when needed. For example,
/// glSetStencilRef needs the current GL_STENCIL_FUNC and GL_STENCIL_VALUE_MASK
/// to set the GL_STENCIL_REF state.
///
/// Dynamic state that should not trigger a shader recompilation (in Vulkan,
/// might not apply in GL) are GL_VIEWPORT, GL_SCISSOR_BOX,
/// GL_POLYGON_OFFSET_FACTOR, GL_POLYGON_OFFSET_UNITS, GL_LINE_WIDTH,
/// GL_DEPTH_RANGE, GL_STENCIL_[BACK_]REF, GL_STENCIL_[BACK_]WRITEMASK,
/// GL_STENCIL_[BACK_]VALUE_MASK, and GL_BLEND_COLOR.
///@{
void cgGlSetViewport(const void *value);
void cgGlSetScissorTest(const void *value);
void cgGlSetScissorBox(const void *value);
void cgGlSetDepthClamp(const void *value);
void cgGlSetRasterizerDiscard(const void *value);
void cgGlSetPolygonMode(const void *value);
void cgGlSetCullFace(const void *value);
void cgGlSetCullFaceMode(const void *value);
void cgGlSetFrontFace(const void *value);
void cgGlSetPolygonOffsetPoint(const void *value);
void cgGlSetPolygonOffsetLine(const void *value);
void cgGlSetPolygonOffsetFill(const void *value);
void cgGlSetPolygonOffsetFactor(const void *value);
void cgGlSetPolygonOffsetUnits(const void *value);
void cgGlSetLineWidth(const void *value);
void cgGlSetMultisample(const void *value);
void cgGlSetSampleShading(const void *value);
void cgGlSetMinSampleShadingValue(const void *value);
void cgGlSetSampleMask(const void *value);
void cgGlSetSampleMaskValue(const void *value);
void cgGlSetSampleAlphaToCoverage(const void *value);
void cgGlSetSampleAlphaToOne(const void *value);
void cgGlSetDepthTest(const void *value);
void cgGlSetDepthWritemask(const void *value);
void cgGlSetDepthFunc(const void *value);
void cgGlSetDepthRange(const void *value);
void cgGlSetStencilTest(const void *value);
void cgGlSetStencilFail(const void *value);
void cgGlSetStencilPassDepthFail(const void *value);
void cgGlSetStencilPassDepthPass(const void *value);
void cgGlSetStencilFunc(const void *value);
void cgGlSetStencilRef(const void *value);
void cgGlSetStencilWritemask(const void *value);
void cgGlSetStencilValueMask(const void *value);
void cgGlSetStencilBackFail(const void *value);
void cgGlSetStencilBackPassDepthFail(const void *value);
void cgGlSetStencilBackPassDepthPass(const void *value);
void cgGlSetStencilBackFunc(const void *value);
void cgGlSetStencilBackRef(const void *value);
void cgGlSetStencilBackWritemask(const void *value);
void cgGlSetStencilBackValueMask(const void *value);
void cgGlSetColorLogicOp(const void *value);
void cgGlSetLogicOpMode(const void *value);
void cgGlSetBlend(const void *value);
void cgGlSetBlendSrcRGB(const void *value);
void cgGlSetBlendDstRGB(const void *value);
void cgGlSetBlendEquationRGB(const void *value);
void cgGlSetBlendSrcAlpha(const void *value);
void cgGlSetBlendDstAlpha(const void *value);
void cgGlSetBlendEquationAlpha(const void *value);
void cgGlSetColorWritemask(const void *value);
void cgGlSetBlendColor(const void *value);
///@}

} // namespace cg
