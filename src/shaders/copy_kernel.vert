#version 450

#define COPY_MODE_INSERT 0
#define COPY_MODE_UPDATE 1

#define BRICK_SIZE 5

layout(location = 1) uniform int u_mode;

layout(std430, binding = 2) restrict buffer ResultBuffer {
    int count;
    int total_count;
    int pad[2];
    uvec4 data[];
} u_result_buffer;

layout(std430, binding = 4) restrict buffer BrickCache {
    int start;
    int count;
    int capacity;
    int pad;
    ivec4 cmd[2];  // DrawArraysInstancedCommand * 2
    uvec4 data[];
} u_bricks;

uvec4 g_data[BRICK_SIZE];

// Compute and update 8-bits occupancy mask for brick voxels
void updateBrickMask()
{
    uint mask = 0u;
    for (int i = 0; i < 8; ++i) {
        mask |= bool(g_data[1 + (i >> 2)][i & 3] & 0xffu) ? (1u << i) : 0u;
    }

    g_data[0].w &= ~0xff00u;
    g_data[0].w |= (mask << 8u);
}

void main()
{
    if (u_mode == COPY_MODE_INSERT) {
        if (gl_VertexID >= u_result_buffer.count) { return; }

        // Fetch brick data
        int result_idx = gl_VertexID;
        for (int i = 0; i < BRICK_SIZE; ++i) {
            g_data[i] = u_result_buffer.data[BRICK_SIZE * result_idx + i];
        }

        // Update brick mask and write brick to cache
        updateBrickMask();
        int brick_idx = ((u_bricks.start + u_bricks.count) + gl_VertexID) % u_bricks.capacity;
        for (int i = 0; i < BRICK_SIZE; ++i) {
            u_bricks.data[BRICK_SIZE * brick_idx + i] = g_data[i];
        }
    }

    if (u_mode == COPY_MODE_UPDATE) {
        // Update brick cache info
        if (u_result_buffer.total_count > 0) {
            u_bricks.start += u_result_buffer.total_count;
            u_bricks.start %= u_bricks.capacity;
            u_bricks.count -= u_result_buffer.total_count;
        }
        u_bricks.count += u_result_buffer.count;

        // Update indirect draw commands
        u_bricks.cmd[0][0] = 8 * u_bricks.count;
        u_bricks.cmd[1][0] = 1 * u_bricks.count;

        // Cleare counters
        u_result_buffer.count = 0;
        u_result_buffer.total_count = 0;
    }
}
