#version 450

#define TILE_SIZE_X 16
#define TILE_SIZE_Y 16
#define NUM_TILES (TILE_SIZE_X * TILE_SIZE_Y)

layout(location = 1) uniform vec4 u_background_color0 = vec4(0.0);
layout(location = 2) uniform vec4 u_background_color1 = vec4(1.0);
layout(location = 3) uniform int u_show_importance = 0;

layout(binding = 0, std430) restrict buffer TileBuffer {
    int data[];
} u_tile_buffer;

layout(location = 0) in vec2 v_uv;
layout(location = 0) out vec4 rt_color;

void main()
{
    vec4 color = vec4(0.0);
    color.rgb = mix(u_background_color0.rgb, u_background_color1.rgb, v_uv.y);

    if (bool(u_show_importance)) {
        vec2 uv_eye = fract(v_uv * vec2(2.0, 1.0));
        int tile_idx = int(uv_eye.y * TILE_SIZE_Y) * TILE_SIZE_X + int(uv_eye.x * TILE_SIZE_X);

        int count = 0;
        for (int i = 0; i < NUM_TILES; ++i) {
            int tile_idx2 = u_tile_buffer.data[3 * NUM_TILES + 1 + i];
            count += int(tile_idx2 > 0 && tile_idx2 == tile_idx);
        }
        color.rgb = vec3(pow(min(8.0, float(count)) / 8.0, 2.2));
    }

    rt_color = color;
}
