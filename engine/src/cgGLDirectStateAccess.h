/// @file    cgGLDirectStateAccess.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"

#ifdef CG_EMULATE_DIRECT_STATE_ACCESS
// Override GL API functions
#define glBindTexture cg::cgGlBindTexture
#define glDeleteTextures cg::cgGlDeleteTextures
#define glGetTextureParameterfv cg::cgGlGetTextureParameterfv
#define glGetTextureParameteriv cg::cgGlGetTextureParameteriv
#define glGetTextureParameterIiv cg::cgGlGetTextureParameterIiv
#define glGetTextureParameterIuiv cg::cgGlGetTextureParameterIuiv
#define glGetTextureLevelParameterfv cg::cgGlGetTextureLevelParameterfv
#define glGetTextureLevelParameteriv cg::cgGlGetTextureLevelParameteriv
#define glGetNamedBufferParameteriv cg::cgGlGetNamedBufferParameteriv
#define glGetNamedBufferParameteri64v cg::cgGlGetNamedBufferParameteri64v
#define glGetNamedFramebufferAttachmentParameteriv cg::cgGlGetNamedFramebufferAttachmentParameteriv
#endif // CG_EMULATE_DIRECT_STATE_ACCESS

namespace cg {

/// @name Custom GL functions for emulating direct state access (DSA)
///
/// Get GL resource parameter. Functions might call glGet* internally to obtain
/// other GL state when needed. When CG_EMULATE_DIRECT_STATE_ACCESS is defined,
/// the global GL function name will be overrided by the custom function.
///@{
void cgGlBindTexture(GLenum target, GLuint texture);
void cgGlDeleteTextures(GLsizei n, const GLuint *textures);
void cgGlGetTextureParameterfv(const GLuint texture,
                               const GLenum value, GLfloat *data);
void cgGlGetTextureParameteriv(const GLuint texture,
                               const GLenum value, GLint *data);
void cgGlGetTextureParameterIiv(const GLuint texture, const GLint lod,
                                const GLenum value, GLint *data);
void cgGlGetTextureParameterIuiv(const GLuint texture, const GLint lod,
                                 const GLenum value, GLuint *data);
void cgGlGetTextureLevelParameterfv(const GLuint texture, const GLint lod,
                                    const GLenum value, GLfloat *data);
void cgGlGetTextureLevelParameteriv(const GLuint texture, const GLint lod,
                                    const GLenum value, GLint *data);
void cgGlGetNamedBufferParameteriv(const GLuint buffer,
                                   const GLenum pname, GLint *params);
void cgGlGetNamedBufferParameteri64v(const GLuint buffer,
                                     const GLenum pname, GLint *params);
void cgGlGetNamedFramebufferAttachmentParameteriv(const GLuint framebuffer,
                                                  const GLenum attachment,
                                                  const GLenum pname,
                                                  GLint *params);
///@}

} // namespace cg
