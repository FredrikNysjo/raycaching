#version 450

#define MAX_SHADOW_STEPS 100
#define MAX_AO_STEPS 20
#define NUM_AO_RAYS 4
#define SHOW_SHADOWS_DEBUG 0
#define SHOW_AO_DEBUG 0

struct Ray { vec3 origin; vec3 dir; };
struct Hit { vec3 p; vec3 normal; float value; float t; };

layout(binding = 4, std140) uniform RaycastUniforms {
    mat4 projection[2];
    mat4 view[2];
    mat4 view_inv[2];
    int ao_enabled;
    int shadows_enabled;
    int max_steps;
    int raycasting_enabled;
    int num_importance_rays;
    float step_length;
} u_raycast;

layout(binding = 5, std140) uniform VolumeUniforms {
    mat4 image_to_world;
    mat4 world_to_image;
    vec4 spacing;
    vec4 clip[2];
    float threshold;
} u_volume;

layout(binding = 0) uniform sampler3D u_volume_texture;
layout(binding = 1) uniform samplerBuffer u_halton_texture;

layout(location = 0) in vec2 v_texcoord;
layout(location = 0) out vec4 rt_color;

bool intersect(vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    // Branchless ray/bbox intersection test adapted from
    // https://tavianator.com/fast-branchless-raybounding-box-intersections/
    vec3 t1 = (-0.5 - ray_origin) * ray_dir_inv;
    vec3 t2 = (0.5 - ray_origin) * ray_dir_inv;
    tmin = max(0.0, max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2]))));
    tmax = max(0.0, min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2]))));
    return bool((tmax - tmin) > 0.0);
}

vec3 reconstructViewPos(vec3 ndc_pos, mat4 projection)
{
    float a = projection[0][0];
    float b = projection[1][1];
    float c = projection[2][2];
    float d = projection[3][2];
    float e = projection[2][0];
    float f = projection[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);

    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

float imageLinear(sampler3D sampler, vec3 p)
{
    float value = textureLod(sampler, p, 0.0).r;
    return value;
}

vec3 imageGrad(sampler3D sampler, vec3 p, vec3 delta)
{
    vec3 grad = vec3(0.0);
    grad.x -= imageLinear(sampler, p - vec3(delta.x, 0, 0));
    grad.x += imageLinear(sampler, p + vec3(delta.x, 0, 0));
    grad.y -= imageLinear(sampler, p - vec3(0, delta.y, 0));
    grad.y += imageLinear(sampler, p + vec3(0, delta.y, 0));
    grad.z -= imageLinear(sampler, p - vec3(0, 0, delta.z));
    grad.z += imageLinear(sampler, p + vec3(0, 0, delta.z));
    return grad;
}

vec3 imageNormal(sampler3D sampler, vec3 p, vec3 spacing)
{
    vec3 delta = 1.0 / vec3(textureSize(sampler, 0));
    delta *= min(spacing.x, min(spacing.y, spacing.z)) / spacing;
    vec3 grad = imageGrad(sampler, p, delta);
    return -normalize(grad);
}

Hit imageRaycast(Ray ray, float step_length, int max_steps, float threshold, int num_refinements)
{
    vec3 p = ray.origin;
    float value = imageLinear(u_volume_texture, p);
    for (int i = 0; i < max_steps; ++i) {
        value = imageLinear(u_volume_texture, p);
        if (value >= threshold) { break; }
        p += (step_length * ray.dir);
    }

    if (value >= threshold) {
        vec3 q = p - (step_length * ray.dir);
        for (int i = 0; i < num_refinements; ++i) {
            vec3 midpoint = (q + p) * 0.5;
            value = imageLinear(u_volume_texture, midpoint);
            if (value >= threshold) { p = midpoint; }
            if (value < threshold) { q = midpoint; }
        }
        value = imageLinear(u_volume_texture, p);
    }

    vec3 normal;
    if (value >= threshold) {
        normal = imageNormal(u_volume_texture, p, u_volume.spacing.xyz);
    }

    return Hit(p, normal, value, -1.0);
}

float imageVisibility(Hit hit, vec3 image_light_dir, float step_length, int max_steps, float threshold)
{
    Ray ray = Ray(hit.p, image_light_dir);
    float jitter = fract(dot(gl_FragCoord.xy, vec2(sqrt(2.0), sqrt(3.0))));
    ray.origin += ray.dir * (1.0 + abs(jitter * 2.0 - 1.0)) * step_length;
    Hit hit_shadow = imageRaycast(ray, step_length, max_steps, max(hit.value, threshold), 0);
    float visibility = length(hit_shadow.p - ray.origin) / (step_length * max_steps);
    return visibility;
}

float diffuse_wrap(vec3 N, vec3 L, float wrap)
{
    return max(0.0, (dot(N, L) + wrap) / (1.0 + wrap));
}

float specular(vec3 N, vec3 L, vec3 V, float specular_power)
{
    vec3 H = normalize(L + V);
    float normalization = (8.0 + specular_power) / 8.0;
    return normalization * pow(max(0.0, dot(N, H)), specular_power);
}

vec3 hemisphere(vec3 N, vec3 up, vec3 ground, vec3 sky)
{
    return mix(ground, sky, dot(N, up) * 0.5 + 0.5);
}

vec3 computeLighting(vec3 N, vec3 V, vec3 L, float shadow, float ao)
{
    vec3 up = vec3(0.0, 1.0, 0.0);
    float specular_power = exp2(10.0 * 0.4);

    vec3 color = vec3(0.0);
    color.rgb += 0.3 * vec3(1.0) * diffuse_wrap(N, L, 0.0) * shadow;
    color.rgb += 0.3 * vec3(0.05) * specular(N, L, V, specular_power) * shadow;
    color.rgb += 0.75 * vec3(1.0) * hemisphere(N, up, vec3(0.0), vec3(1.0)) * ao;
    color.rgb = bool(SHOW_SHADOWS_DEBUG) ? vec3(shadow) : color.rgb;
    color.rgb = bool(SHOW_AO_DEBUG) ? vec3(ao) : color.rgb;
    return color;
}

vec3 linearToGamma(vec3 color)
{
    return pow(color, vec3(1.0 / 2.2));
}

void main()
{
    int eye_idx = v_texcoord.x > 0.5 ? 1 : 0;
    vec2 uv = vec2(v_texcoord.x * 2.0 - float(eye_idx), v_texcoord.y);

    vec3 ndc_pos = vec3(uv, 0.0) * 2.0 - 1.0;
    vec3 view_pos_ = reconstructViewPos(ndc_pos, u_raycast.projection[eye_idx]);
    mat4 image_from_view = u_volume.world_to_image * u_raycast.view_inv[eye_idx];

    Ray ray;
    ray.origin = vec3(image_from_view * vec4(view_pos_, 1.0));
    ray.dir = normalize(mat3(image_from_view) * view_pos_);

    vec4 color = vec4(0.0);
    float tmin, tmax;
    if (intersect(ray.origin - 0.5, 1.0 / ray.dir, tmin, tmax)) {
        float step_length = u_raycast.step_length;
        int num_steps = min(1024, int((tmax - tmin) / step_length));
        float jitter = fract(dot(gl_FragCoord.xy, vec2(sqrt(2.0), sqrt(3.0))));
        ray.origin += ray.dir * tmin;
        ray.origin -= (jitter * step_length) * ray.dir;

        float threshold = u_volume.threshold;
        Hit hit = imageRaycast(ray, step_length, num_steps, threshold, 8);
        if (hit.value >= threshold) {
            vec3 view_normal = mat3(u_raycast.view[eye_idx]) * hit.normal;
            vec4 world_pos = u_volume.image_to_world * vec4(hit.p, 1.0);
            vec3 view_pos = vec3(u_raycast.view[eye_idx] * world_pos);

            vec3 N = view_normal;
            vec3 V = -normalize(view_pos);
            vec3 L = normalize(vec3(1.0, 2.0, 3.0));
            float shadow = 1.0, ao = 1.0;
            if (bool(u_raycast.shadows_enabled)) {
                vec3 image_light_dir = normalize(mat3(image_from_view) * L);
                shadow = imageVisibility(hit, image_light_dir, step_length, MAX_SHADOW_STEPS, threshold);
            }
            if (bool(u_raycast.ao_enabled)) {
                ao = 0.0;
                for (int i = 0; i < NUM_AO_RAYS; ++i) {
                    vec3 rnd = fract(texelFetch(u_halton_texture, i).rgb + jitter);
                    vec3 image_ray_dir = normalize(hit.normal + normalize(rnd - 0.5));
                    ao += imageVisibility(hit, image_ray_dir, step_length, MAX_AO_STEPS, threshold);
                }
                ao /= float(NUM_AO_RAYS);
            }

            color.rgb = computeLighting(N, V, L, shadow, ao);
            color.a = 1.0;
        }
    }
    rt_color = color;
}
