/// @file    cgUtils.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgGL.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/constants.hpp>
#include <glm/gtx/quaternion.hpp>

#include <cstdint>
#include <string>
#include <vector>

namespace cg {

/// Struct for Wavefront (OBJ) triangle meshes that are indexed and has
/// per-vertex normals
struct CgOBJMesh {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<std::int32_t> indices;
};

/// Struct for Wavefront (OBJ) triangle meshes that are indexed and has
/// per-vertex normals and UV texture coordinates
struct CgOBJMeshUV {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec3> texcoords;
    std::vector<std::int32_t> indices;
};

/// Struct for representing a virtual 3D trackball that can be used for
/// object or camera rotation
struct CgTrackball {
    double radius;
    glm::vec2 center;
    bool tracking;
    glm::vec3 vStart;
    glm::quat qStart;
    glm::quat qCurrent;

    CgTrackball() 
    {
        radius = 1.0;
        center = glm::vec2(0.0f, 0.0f);
        tracking = false;
        vStart = glm::vec3(0.0f, 0.0f, 1.0f);
        qStart = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
        qCurrent = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
    }
};

/// Struct for storing transform feedback info, including:
///
/// - Output varyings (name, order)
/// - Buffer mode (interleaved or separate)
///
struct CgTransformFeedbackInfo {
    CgVector<const char *>::type varyings;
    GLenum bufferMode;

    CgTransformFeedbackInfo()
    {
        bufferMode = GL_INTERLEAVED_ATTRIBS;
    }

    CgTransformFeedbackInfo(CgVector<const char *>::type _varyings,
                            GLenum _bufferMode=GL_INTERLEAVED_ATTRIBS)
    {
        varyings = _varyings;
        bufferMode = _bufferMode;
    }
};

/// Struct for storing shader program info
struct CgShaderProgramInfo {
    CgVector<GLuint>::type shaders;
    CgTransformFeedbackInfo xfInfo;
};

/// Returns the value of an environment variable
const char *cgGetEnvSafe(const char *name);

/// Load and compile GLSL shader from file
GLuint cgLoadShader(const std::string &filename, GLenum shaderType);

/// Create GLSL program from compiled shaders and other program info
GLuint cgCreateShaderProgram(const CgShaderProgramInfo &programInfo);

/// Convenience function
GLuint cgLoadShaderProgram(const std::string &vsFilename,
                           const std::string &fsFilename);

/// Load RGBA8 2D texture from PNG file
GLuint cgLoad2DTexture(const std::string &filename);

/// Load cubemap texture and let OpenGL generate a mipmap chain
GLuint cgLoadCubemap(const std::string &dirname);

/// Load cubemap with pre-computed mipmap chain
GLuint cgLoadCubemapMipmap(const std::string &dirname);

/// Start trackball tracking
void cgTrackballStartTracking(CgTrackball &trackball, const glm::vec2 &point);

/// Stop trackball tracking
void cgTrackballStopTracking(CgTrackball &trackball);

/// Rotate trackball from, e.g., mouse movement
void cgTrackballMove(CgTrackball &trackball, const glm::vec2 &point);

/// Get trackball orientation in matrix form
glm::mat4 cgTrackballGetRotationMatrix(CgTrackball &trackball);

/// Read an OBJMesh from an .obj file
bool cgObjMeshLoad(CgOBJMesh &mesh, const std::string &filename);

/// Read an OBJMeshUV from an .obj file. This function can read texture
/// coordinates and/or normals, in addition to vertex positions.
bool cgObjMeshUVLoad(CgOBJMeshUV &mesh, const std::string &filename);

} // namespace cg
