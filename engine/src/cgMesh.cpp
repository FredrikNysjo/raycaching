/// @file    cgMesh.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "cgMesh.h"
#include "cgRenderer.h"

namespace cg {

void cgCreateMeshVAO(const cg::CgMesh &mesh, CgMeshVAO *meshVAO)
{
    // Save OpenGL state
    GLint prevVertexArrayBinding;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVertexArrayBinding);

    // Create VAO for drawing the mesh, and bind it
    glGenVertexArrays(1, &(meshVAO->vao));
    glBindVertexArray(meshVAO->vao);

    // Create buffer for vertex positions
    if (mesh.vertices.size()) {
        glGenBuffers(1, &(meshVAO->vertexVBO));
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->vertexVBO);
        size_t verticesNBytes = mesh.vertices.size() * sizeof(mesh.vertices[0]);
        glBufferData(GL_ARRAY_BUFFER, verticesNBytes, mesh.vertices.data(),
                     GL_STATIC_DRAW);
    }

    // Create buffer for vertex colors
    if (mesh.colors.size()) {
        glGenBuffers(1, &(meshVAO->colorVBO));
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->colorVBO);
        size_t colorsNBytes = mesh.colors.size() * sizeof(mesh.colors[0]);
        glBufferData(GL_ARRAY_BUFFER, colorsNBytes, mesh.colors.data(),
                     GL_STATIC_DRAW);
    }

    // Create buffer for vertex normals
    if (mesh.normals.size()) {
        glGenBuffers(1, &(meshVAO->normalVBO));
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->normalVBO);
        size_t normalsNBytes = mesh.normals.size() * sizeof(mesh.normals[0]);
        glBufferData(GL_ARRAY_BUFFER, normalsNBytes, mesh.normals.data(),
                     GL_STATIC_DRAW);
    }

    // Create buffer for vertex texture coordinates
    if (mesh.texcoords.size()) {
        glGenBuffers(1, &(meshVAO->texcoordVBO));
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->texcoordVBO);
        size_t texcoordsNBytes = mesh.texcoords.size() * sizeof(mesh.texcoords[0]);
        glBufferData(GL_ARRAY_BUFFER, texcoordsNBytes, mesh.texcoords.data(),
                     GL_STATIC_DRAW);
    }

    // Create buffer for element indices
    if (mesh.indices.size()) {
        glGenBuffers(1, &(meshVAO->indexVBO));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshVAO->indexVBO);
        size_t indicesNBytes = mesh.indices.size() * sizeof(mesh.indices[0]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesNBytes, mesh.indices.data(),
                     GL_STATIC_DRAW);
    }

    // Set bindings and vertex format for VAO
    if (mesh.vertices.size()) {
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->vertexVBO);
        glEnableVertexAttribArray(CG_POSITION);
        glVertexAttribPointer(CG_POSITION, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    }
    if (mesh.colors.size()) {
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->colorVBO);
        glEnableVertexAttribArray(CG_COLOR);
        glVertexAttribPointer(CG_COLOR, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    }
    if (mesh.normals.size()) {
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->normalVBO);
        glEnableVertexAttribArray(CG_NORMAL);
        glVertexAttribPointer(CG_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    }
    if (mesh.texcoords.size()) {
        glBindBuffer(GL_ARRAY_BUFFER, meshVAO->texcoordVBO);
        glEnableVertexAttribArray(CG_TEXCOORD0);
        glVertexAttribPointer(CG_TEXCOORD0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    }
    if (mesh.indices.size()) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshVAO->indexVBO);
    }

    // Store additional information
    meshVAO->numVertices = mesh.vertices.size();
    meshVAO->numIndices = mesh.indices.size();
    meshVAO->primitiveType = GL_TRIANGLES;
    meshVAO->indexType = GL_UNSIGNED_INT;

    // Restore OpenGL state
    glBindVertexArray(prevVertexArrayBinding);
}

} // namespace cg
