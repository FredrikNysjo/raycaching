#version 330
#extension GL_ARB_shading_language_420pack : require

layout(location = 0) in vec4 a_position;
//layout(location = 1) in vec3 a_color;
//layout(location = 2) in vec3 a_normal;
//layout(location = 3) in vec3 a_texcoord0;

out vec3 v_color;

//layout(binding = 1, std140) uniform SceneUniforms {
//    mat4 u_view;
//    mat4 u_projection;
//    float u_time;
//};

layout(binding = 3, std140) uniform DrawableUniforms {
    mat4 u_mvp;
    mat4 u_mv;
    mat4 u_model;
};

void main()
{
    v_color = a_position.xyz + 0.5;
    gl_Position = u_mvp * a_position;
}
