# RayCaching

Implementation of our isosurface rendering method proposed in the paper ["RayCaching: Amortized Isosurface Rendering for Virtual Reality"](https://onlinelibrary.wiley.com/doi/full/10.1111/cgf.13762) published in Computer Graphics Forum (2019). This work was also presented at the joint EuroGraphics/EuroVis 2020 conference, during the virtual session "Ray Tracing and Global Illumination". Video from the presentation is available [here](https://youtu.be/AoAbvd540I4?t=3320) (starts at around 55:20).


## Paper abstract

"Real-time virtual reality requires efficient rendering methods to deal with high-resolution stereoscopic displays and low latency head-tracking. Our proposed RayCaching method renders isosurfaces of large volume datasets by amortizing raycasting over several frames and caching primary rays as small bricks that can be efficiently rasterized. An occupancy map in form of a clipmap provides level of detail and ensures that only bricks corresponding to visible points on the isosurface are being cached and rendered. Hard shadows and ambient occlusion from secondary rays are also accumulated and stored in the cache. Our method supports real-time isosurface rendering with dynamic isovalue and allows stereoscopic visualization and exploration of large volume datasets at framerates suitable for virtual reality applications."


## Pipeline overview

![Pipeline overview](/pipeline_overview.png?raw=true "Pipeline overview")


## Example screenshot

![Screenshot](/screenshot.png?raw=true "Screenshot")

Note: This screenshot (also used in the paper) was captured from the version in the [vr_test](https://bitbucket.org/FredrikNysjo/raycaching/src/vr_test/) branch compiled with OpenVR support. The stag beetle dataset (832x832x494 voxels) is from [this page](https://www.cg.tuwien.ac.at/research/publications/2005/dataset-stagbeetle/), and was loaded as a .vtk file converted from the original raw volume data.


## Build instructions for Linux

You need to install the following tools, which should be available in the distributions package manager:

    gcc/g++, CMake 3.10 (or higher), make, git

In addition, you also need a few libraries from the package manager. On for example Ubuntu 18.04 and 20.04, you can install them by

    sudo apt install libgl1-mesa-dev libxmu-dev libxi-dev libxrandr-dev libxcursor-dev libxinerama-dev

Once the tools and dependencies are installed, set the environment variable `CG_ENGINE_ROOT` to the path where this `README.md` is located, and run the provided `build.sh` script to build and run the demo application. The resulting binary `raycaching` will be placed under `CG_ENGINE_ROOT/bin`.


## Build instructions for Windows

On Windows, you can install VSCode and the Microsof Visual C++ (MSVC) compiler via the instructions from https://code.visualstudio.com/docs/cpp/config-msvc , and then use CMake (3.14 or higher) from the command line or via the [VSCode Tools addon](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) to generate and build the demo application. Before running the application, you also need to set the environment variable `CG_ENGINE_ROOT` to the path where this `README.md` is located

The provided `build.sh` script can also be used from Git Bash on Windows, provided that you have CMake and the MSVC compiler installed. To change the CMake generator to match the MSVC version, you can use the `CG_GENERATOR` environment variable (by default, this variable is set to "Visual Studio 16 2019" in the script).


## Usage

Command line usage on Linux:

    ./raycaching [input_filename] [input_scale]

Command line usage on Windows:

    raycaching.exe [input_filename] [input_scale]


## License

The code is provided under the MIT license (see `LICENSE.md`).
