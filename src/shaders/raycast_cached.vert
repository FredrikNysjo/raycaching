#version 450
#extension GL_ARB_gpu_shader_int64 : enable
#extension GL_NV_shader_atomic_int64 : enable

#define RAYCAST_MODE_CREATE 0
#define RAYCAST_MODE_UPDATE 1
#define RAYCAST_MODE_CLEAR 2
#define RAYCAST_MODE_INSERT 3

#define BRICK_SIZE 5
#define TILE_SIZE_X 16
#define TILE_SIZE_Y 16
#define NUM_TILES (TILE_SIZE_X * TILE_SIZE_Y)

//#define USE_UINT64_CLIPMAP_VOXELS  // Requires support for 64-bits atomics
#ifdef USE_UINT64_CLIPMAP_VOXELS
#define CLIPMAP_VALUE_TYPE uint64_t
#else // USE_UINT64_CLIPMAP_VOXELS
#define CLIPMAP_VALUE_TYPE uint
#endif // USE_UINT64_CLIPMAP_VOXELS

layout(binding = 4, std140) uniform RaycastUniforms {
    mat4 projection[2];
    mat4 view[2];
    mat4 view_inv[2];
    int num_occlusion_rays;
    int num_shadow_rays;
    int max_steps;
    int raycasting_enabled;
    int num_importance_rays;
    float step_length;
    int use_normal_refinement;
    int num_rays_per_tile;
    int frame;
} u_raycast;

layout(binding = 5, std140) uniform VolumeUniforms {
    mat4 world_from_image;
    mat4 image_from_world;
    vec4 spacing;
    vec4 clip[2];
    float threshold;
} u_volume;

layout(binding = 6, std140) uniform ClipmapUniforms {
    mat4 world_from_clipmap;
    mat4 clipmap_from_world;
    vec4 center;
    ivec4 dimensions;
    float radius;
    float spacing;
    int subres;
} u_clipmap;

layout(location = 1) uniform int u_mode;
layout(location = 2) uniform int u_importance_samples = 0;

layout(binding = 0) uniform sampler3D u_volume_texture;
layout(binding = 2) uniform samplerBuffer u_halton;

layout(std430, binding = 2) restrict buffer ResultBuffer {
    int count;
    int total_count;
    int pad[2];
    uvec4 data[];
} u_result_buffer;

layout(std430, binding = 3) restrict buffer ClipmapBuffer {
    CLIPMAP_VALUE_TYPE data[];
} u_clipmap_buffer;

layout(std430, binding = 4) restrict buffer BrickCache {
    int start;
    int count;
    int capacity;
    int pad;
    ivec4 cmd[2];  // DrawArraysInstancedCommand
    uvec4 data[];
} u_brick_cache;

layout(std430, binding = 5) restrict buffer TimestampBuffer {
    uint data[];
} u_timestamp_buffer;

layout(std430, binding = 6) restrict buffer TileBuffer {
    int data[];
} u_tile_buffer;

uvec4 g_data[BRICK_SIZE];

float imageLinear(vec3 p)
{
    return textureLod(u_volume_texture, p, 0.0).r;
}

vec3 imageGradient(vec3 p, vec3 delta)
{
    vec3 grad = vec3(0.0);
    grad.x -= imageLinear(p - vec3(delta.x, 0.0, 0.0));
    grad.x += imageLinear(p + vec3(delta.x, 0.0, 0.0));
    grad.y -= imageLinear(p - vec3(0.0, delta.y, 0.0));
    grad.y += imageLinear(p + vec3(0.0, delta.y, 0.0));
    grad.z -= imageLinear(p - vec3(0.0, 0.0, delta.z));
    grad.z += imageLinear(p + vec3(0.0, 0.0, delta.z));

    return grad;
}

vec3 imageNormal(vec3 p, vec3 spacing)
{
    vec3 delta = 1.0 / vec3(textureSize(u_volume_texture, 0));
    delta *= min(spacing.x, min(spacing.y, spacing.z)) / spacing;
    vec3 grad = imageGradient(p, delta);

    return -normalize(grad);
}

vec4 imageRayCast(vec3 ray_origin, vec3 ray_dir, float step,
                  int max_steps, float threshold)
{
    vec3 p = ray_origin;
    float value = imageLinear(p);
    for (int i = 0; i < max_steps; ++i) {
        value = imageLinear(p);
        if (value >= threshold) { break; }
        p += (step * ray_dir);
    }

    int num_refinements = 4;  // Set to 0 to disable
    if (value >= threshold) {
        vec3 q = p - (step * ray_dir);
        for (int i = 0; i < num_refinements; ++i) {
            vec3 midpoint = (q + p) * 0.5;
            value = imageLinear(midpoint);
            if (value >= threshold) { p = midpoint; }
            if (value < threshold) { q = midpoint; }
        }
        value = imageLinear(p);
    }

    return vec4(p, value);
}

// Intersect image-space ray with 1-unit AABB centered at vec3(0.5), using
// method from Chapter 5.3 in "Real-Time Collision Detection" book
vec2 imageRayBoxIntersect(vec3 ray_origin, vec3 ray_dir)
{
    float tmin = 0.0;
    float tmax = 9999.0;
    for (int i = 0; i < 3; ++i) {
        float odd = 1.0 / (abs(ray_dir[i]) > 1e-5 ? ray_dir[i] : 1e-5);
        vec2 t = (vec2(0.0, 1.0) - ray_origin[i]) * odd;
        tmin = max(tmin, (t[0] > t[1] ? t[1] : t[0]));
        tmax = min(tmax, (t[0] > t[1] ? t[0] : t[1]));
    }
    if (tmin > tmax) { tmin = tmax = -1.0; }

    return vec2(tmin, tmax);
}

vec3 reconstructViewPos(vec3 ndc_pos, mat4 projection)
{
    float a = projection[0][0];
    float b = projection[1][1];
    float c = projection[2][2];
    float d = projection[3][2];
    float e = projection[2][0];
    float f = projection[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);

    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

vec4 clipmapFromWorld(vec3 p, int bias)
{
    vec3 q = (p - u_clipmap.center.xyz) / u_clipmap.radius;
    float lod = ceil(log2(max(1.0, max(abs(q.x), max(abs(q.y), abs(q.z)))))) + bias;
    vec3 pos = u_clipmap.dimensions[0] * ((q / exp2(lod)) * 0.5 + 0.5);

    return vec4(pos, lod);
}

vec4 worldFromClipmap(vec4 p)
{
    vec3 q = ((p.xyz / u_clipmap.dimensions[0]) - 0.5) * (exp2(p.w) / 0.5);
    vec3 pos = (q * u_clipmap.radius) + u_clipmap.center.xyz;

    return vec4(pos, 1.0);
}

int clipmapVoxelIndex(vec4 clipmap_pos, ivec4 clipmap_size)
{
    ivec4 p = ivec4(clipmap_pos);
    return clipmap_size.x * (clipmap_size.y * (clipmap_size.z * p.w + p.z) + p.y) + p.x;
}

CLIPMAP_VALUE_TYPE clipmapSubvoxelBit(vec4 clipmap_pos)
{
    vec3 ndc_pos = (clipmap_pos.xyz - (0.5 * u_clipmap.dimensions[0])) / (0.5 * u_clipmap.dimensions[0]);
    float dist = max(abs(ndc_pos.x), max(abs(ndc_pos.y), abs(ndc_pos.z)));

    CLIPMAP_VALUE_TYPE mask = 0;
    int sz = u_clipmap.subres;
    vec3 subvoxel = floor(sz * fract(clipmap_pos.xyz));
    mask = CLIPMAP_VALUE_TYPE(1) << uint(int(subvoxel.z) * (sz * sz) + int(subvoxel.y) * sz + int(subvoxel.x));

    return mask;
}

float clipmapSubvoxelRadius(vec4 clipmap_pos)
{
    return (0.5 / u_clipmap.subres) * exp2(clipmap_pos.w) * u_clipmap.spacing;
}

vec4 clipmapSubvoxelWorldPos(vec3 p, int bias)
{
    vec4 clipmap_pos = clipmapFromWorld(p, bias);
    int sz = u_clipmap.subres;
    vec4 clipmap_pos_new = vec4((floor(sz * clipmap_pos.xyz) + 0.5) / sz, clipmap_pos.w);

    return worldFromClipmap(clipmap_pos_new);
}

// Clear clipmap subvoxel using atomics
void clipmapClearSubvoxel(vec4 clipmap_pos)
{
    int voxel_idx = clipmapVoxelIndex(clipmap_pos, u_clipmap.dimensions);
    CLIPMAP_VALUE_TYPE subvoxel_bit = clipmapSubvoxelBit(clipmap_pos);
    atomicAnd(u_clipmap_buffer.data[voxel_idx], ~subvoxel_bit);
}

// Write and test clipmap subvoxel using atomics. Returns true if the test
// succeeds, false otherwise.
bool clipmapWriteAndTestSubvoxel(vec4 clipmap_pos)
{
     int voxel_idx = clipmapVoxelIndex(clipmap_pos, u_clipmap.dimensions);
     CLIPMAP_VALUE_TYPE subvoxel_bit = clipmapSubvoxelBit(clipmap_pos);
     CLIPMAP_VALUE_TYPE voxel_mask = atomicOr(u_clipmap_buffer.data[voxel_idx], subvoxel_bit);

     return (voxel_mask & subvoxel_bit) == CLIPMAP_VALUE_TYPE(0);
}

// Updates timestamp of clipmap voxel
void timestampUpdate(vec4 clipmap_pos, uint timestamp)
{
    ivec4 ts_pos = ivec4(clipmap_pos.xyz * 0.5, clipmap_pos.w);
    int ts_idx = 64 * (64 * (64 * ts_pos.w + ts_pos.z) + ts_pos.y) + ts_pos.x;

    u_timestamp_buffer.data[ts_idx] = timestamp;
}

// Fetches timestamp of clipmap voxel
uint timestampFetch(vec4 clipmap_pos)
{
    ivec4 ts_pos = ivec4(clipmap_pos.xyz * 0.5, clipmap_pos.w);
    int ts_idx = 64 * (64 * (64 * ts_pos.w + ts_pos.z) + ts_pos.y) + ts_pos.x;

    return u_timestamp_buffer.data[ts_idx];
}

// Convert Cartesian normal to spherical coordinate in range [0,1]
vec2 cartToSphere(vec3 c)
{
    return (vec2(atan(c.y, c.x), acos(c.z)) / 3.141592) * 0.5 + 0.5;
}

// Convert spherical coordinate in range [0,1] to Cartesian normal
vec3 sphereToCart(vec2 s)
{
    float theta = (s.x * 2.0 - 1.0) * 3.141592;
    float phi = (s.y * 2.0 - 1.0) * 3.141592;

    return vec3(cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi));
}

// Compute coverage and normal data for 2^3 brick. Brick position and radius
// shall be in world coordinates.
void updateBrick(vec3 world_pos, float subvox_radius)
{
    vec3 image_pos = vec3(u_volume.image_from_world * vec4(world_pos, 1.0));
    vec3 image_radius = mat3(u_volume.image_from_world) * vec3(subvox_radius);

    vec3 dir = abs(normalize(world_pos - u_clipmap.center.xyz));
    vec3 dir2 = normalize(world_pos - u_clipmap.center.xyz);

    for (int z = 0; z < 2; ++z) {
        for (int y = 0; y < 2; ++y) {
            for (int x = 0; x < 2; ++x) {
                vec3 p = image_pos + (vec3(x, y, z) - 0.5) * image_radius;

                float taps[9];
                taps[0] = imageLinear(p);
                for (int i = 0; i < 8; ++i) {
                    vec3 delta = vec3(i & 1, (i >> 1) & 1, (i >> 2)) - 0.5;
                    taps[i + 1] = imageLinear(p + delta * image_radius);
                }

                float coverage = 8.0 * step(u_volume.threshold, taps[0]);
                for (int i = 1; i < 9; ++i) {
                    coverage += step(u_volume.threshold, taps[i]);
                }
                coverage /= 16.0;

                vec3 opacity = vec3(0.0);
                opacity.z += step(u_volume.threshold, max(taps[1], taps[5]));
                opacity.z += step(u_volume.threshold, max(taps[2], taps[6]));
                opacity.z += step(u_volume.threshold, max(taps[3], taps[7]));
                opacity.z += step(u_volume.threshold, max(taps[4], taps[8]));
                opacity.y += step(u_volume.threshold, max(taps[1], taps[3]));
                opacity.y += step(u_volume.threshold, max(taps[2], taps[4]));
                opacity.y += step(u_volume.threshold, max(taps[5], taps[7]));
                opacity.y += step(u_volume.threshold, max(taps[6], taps[8]));
                opacity.x += step(u_volume.threshold, max(taps[1], taps[2]));
                opacity.x += step(u_volume.threshold, max(taps[3], taps[4]));
                opacity.x += step(u_volume.threshold, max(taps[5], taps[6]));
                opacity.x += step(u_volume.threshold, max(taps[7], taps[8]));
                opacity /= 4.0;

                vec3 grad_sobel = vec3(0.0);
                grad_sobel.x += (taps[2] + taps[4] + taps[6] + taps[8]);
                grad_sobel.x -= (taps[1] + taps[3] + taps[5] + taps[7]);
                grad_sobel.y += (taps[3] + taps[4] + taps[7] + taps[8]);
                grad_sobel.y -= (taps[1] + taps[2] + taps[5] + taps[6]);
                grad_sobel.z += (taps[5] + taps[6] + taps[7] + taps[8]);
                grad_sobel.z -= (taps[1] + taps[2] + taps[3] + taps[4]);
                vec3 normal = -normalize(grad_sobel + 1e-5);

                // Do one step of Newton-Rapson to estimate signed distance to surface
                float midpoint = taps[0];
                float t = 4.0 * (u_volume.threshold - midpoint) / length(grad_sobel);
                float d = clamp(-t, -0.5, 0.5) + 0.5;
                if (coverage > 0.01 && coverage < 0.99) { coverage = clamp(d, 0.02, 0.98); }

                if (bool(u_raycast.use_normal_refinement)) {
                    vec3 delta = (coverage - 0.5) * normal;
                    normal = imageNormal(p + delta * image_radius, u_volume.spacing.xyz);
                }
                if (dot(normal, -dir2) < -0.5) { coverage = 0.0; }

                float c = (coverage == 1.0) ? 0.0 : coverage;
                float o = min(0.99, opacity.x * dir.x + opacity.y * dir.y + opacity.z * dir.z);
                vec2 n = cartToSphere(normal);
                g_data[z + 1][y * 2 + x] = packUnorm4x8(vec4(c, n.x, n.y, o));
            }
        }
    }
}

// Append current brick to end of result buffer
int emitBrick()
{
    int result_idx = atomicAdd(u_result_buffer.count, 1);
    for (int i = 0; i < BRICK_SIZE; ++i) {
        u_result_buffer.data[BRICK_SIZE * result_idx + i] = g_data[i];
    }

    return result_idx;
}

// Compute 8-bit mask indicating which brick voxels that are occupied and
// needs to be rendered
uint brickMask()
{
    uint mask = 0u;
    for (int i = 0; i < 8; ++i) {
        mask |= bool(g_data[i / 4 + 1][i % 4] & 0xffu) ? (1u << i) : 0u;
    }
    return mask;
}

void main()
{
    if ((u_mode == RAYCAST_MODE_CREATE && gl_VertexID >= (u_brick_cache.capacity - u_brick_cache.count)) ||
        ((u_mode != RAYCAST_MODE_CREATE && gl_VertexID >= u_brick_cache.count))) { return; }

    // Fetch brick data (negative index used for fetching zeros)
    int brick_idx = (u_brick_cache.start + gl_VertexID) % u_brick_cache.capacity;
    brick_idx = (u_mode == RAYCAST_MODE_CREATE) ? -BRICK_SIZE : brick_idx;
    for (int i = 0; i < BRICK_SIZE; ++i) {
        g_data[i] = u_brick_cache.data[BRICK_SIZE * brick_idx + i];
    }

    // Unpack brick data
    vec4 world_pos = vec4(uintBitsToFloat(g_data[0].xyz), 1.0);
    int lod = int(g_data[0].w & 0xf);
    int hidden = int(g_data[0].w >> 31u) & 1;

    if (u_mode == RAYCAST_MODE_CREATE || u_mode == RAYCAST_MODE_UPDATE) {
        int eye_idx = gl_VertexID & 1;
        int tile_idx = gl_VertexID / u_raycast.num_rays_per_tile;

        if (u_mode == RAYCAST_MODE_CREATE) {
            // Compute UV for ray
            if (bool(u_importance_samples)) {
                tile_idx = u_tile_buffer.data[3 * NUM_TILES + 1 + tile_idx];
            }
            vec2 tile = vec2(tile_idx % TILE_SIZE_X, tile_idx / TILE_SIZE_X);
            int seed = atomicAdd(u_tile_buffer.data[tile_idx * 3 + eye_idx], 1) + (2048 * eye_idx);
            vec2 uv = (tile + texelFetch(u_halton, seed % 4096).rg) / vec2(TILE_SIZE_X, TILE_SIZE_Y);

            // Compute ray start and stop position
            vec3 ndc_pos = vec3(uv, 0.0) * 2.0 - 1.0;
            vec3 view_pos = reconstructViewPos(ndc_pos, u_raycast.projection[eye_idx]);
            mat4 image_from_view = u_volume.image_from_world * u_raycast.view_inv[eye_idx];
            vec3 image_pos = vec3(image_from_view * vec4(view_pos, 1.0));
            vec3 ray_dir = normalize(mat3(image_from_view) * view_pos);
            vec2 t = imageRayBoxIntersect(image_pos, ray_dir);
            vec3 ray_start = image_pos + ray_dir * t[0];
            vec3 ray_stop = image_pos + ray_dir * t[1];

            // Do raycasting
            float ray_length = length(ray_stop - ray_start);
            int num_steps = int(ray_length / u_raycast.step_length);
            float jitter = texelFetch(u_halton, gl_VertexID % 1024).r;
            ray_start -= (jitter * u_raycast.step_length) * ray_dir;
            vec4 hit = imageRayCast(ray_start, ray_dir, u_raycast.step_length,
                                    num_steps, u_volume.threshold);
            if (hit.w < u_volume.threshold) { return; }

            // Decide if brick generated for intersection should be hidden
            hidden = int((gl_VertexID % 5) == 0);

            // Check that intersection is inside clipmap
            world_pos = u_volume.world_from_image * vec4(hit.xyz, 1.0);
            vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
            if (int(clipmap_pos.w) >= u_clipmap.dimensions.w) { return; }

            timestampUpdate(clipmap_pos, uint(u_raycast.frame));
        }

        atomicAdd(u_result_buffer.total_count, int(u_mode == RAYCAST_MODE_UPDATE));

        // Snap to clipmap subvoxel
        world_pos = clipmapSubvoxelWorldPos(world_pos.xyz, hidden);
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);

        // Check if brick can be discarded
        uint voxel_timestamp = timestampFetch(clipmap_pos);
        if ((uint(u_raycast.frame) - voxel_timestamp) > 60u) { return; }
        if (u_mode == RAYCAST_MODE_UPDATE && brickMask() == 0u) { return; }
        if (!clipmapWriteAndTestSubvoxel(clipmap_pos)) { return; }  // Do this last!

        // Compute and update brick data
        float subvox_radius = clipmapSubvoxelRadius(clipmap_pos);
        updateBrick(world_pos.xyz, subvox_radius);

        // Write brick to result buffer
        g_data[0].xyz = floatBitsToUint(world_pos.xyz);
        g_data[0].w = uint(clipmap_pos.w) | (brickMask() << 8u) | uint(hidden << 31);
        emitBrick();

        if (!bool(u_importance_samples) && u_mode == RAYCAST_MODE_CREATE) {
            if (atomicMax(u_tile_buffer.data[tile_idx * 3 + 2], u_raycast.frame) != u_raycast.frame) {
                int count = atomicAdd(u_tile_buffer.data[3 * NUM_TILES], 1);
                u_tile_buffer.data[3 * NUM_TILES + 1 + (count % NUM_TILES)] = tile_idx;
            }
        }
    }

    if (u_mode == RAYCAST_MODE_CLEAR) {
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
        clipmapClearSubvoxel(clipmap_pos);

        // Save timestamp
        uint voxel_timestamp = timestampFetch(clipmap_pos);
        u_brick_cache.data[BRICK_SIZE * u_brick_cache.capacity + brick_idx][0] = voxel_timestamp;
    }

    if (u_mode == RAYCAST_MODE_INSERT) {
        atomicAdd(u_result_buffer.total_count, 1);

        // Update LOD
        vec4 clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden);
        int hidden_new = hidden;
        if (!bool(hidden) && (clipmap_pos.w > lod)) { return; }  // Discard if moving to higher LOD
        if (bool(hidden) && (clipmap_pos.w > lod)) { hidden_new = 0; }
        if (!bool(hidden) && (clipmap_pos.w < lod)) { hidden_new = 1; }
        bool scatter_brick = (clipmap_pos.w < lod);  // Need to compare with old value before we update
        clipmap_pos = clipmapFromWorld(world_pos.xyz, hidden_new);
        if (clipmap_pos.w >= u_clipmap.dimensions.w) { return; }

        // Restore timestamp
        uint voxel_timestamp = u_brick_cache.data[BRICK_SIZE * u_brick_cache.capacity + brick_idx][0];
        timestampUpdate(clipmap_pos, voxel_timestamp);

        if (clipmapWriteAndTestSubvoxel(clipmap_pos)) {
            // Write brick to result buffer
            g_data[0].xyz = floatBitsToUint(world_pos.xyz);
            g_data[0].w = uint(clipmap_pos.w) | (brickMask() << 8u) | uint(hidden_new << 31);
            emitBrick();
        }

        if (scatter_brick) {
            uvec4 data_[BRICK_SIZE];
            for (int i = 0; i < BRICK_SIZE; ++i) {
                data_[i] = g_data[i];
                g_data[i] = uvec4(0u);
            }

            for (int i = 0; i < 8; ++i) {
                // Unpack sub-point data
                uint packed0 = data_[1 + (i >> 2)][i & 3];
                if ((packed0 & 0xffu) == 0u) { continue; }
                float coverage = unpackUnorm4x8(packed0).x;
                vec3 world_normal = sphereToCart(unpackUnorm4x8(packed0).yz);

                // Compute location of new brick
                vec3 displacement = (coverage - 0.5) * world_normal;
                vec3 brick_pos = vec3(i & 1, (i >> 1) & 1, i >> 2) - 0.5;
                float subvox_radius = (0.5 / u_clipmap.subres) * exp2(lod) * u_clipmap.spacing;
                vec3 world_pos_new = world_pos.xyz + (brick_pos + displacement) * subvox_radius;
                vec4 clipmap_pos_new = clipmapFromWorld(world_pos_new, 0);

                timestampUpdate(clipmap_pos_new, voxel_timestamp);
                if (!clipmapWriteAndTestSubvoxel(clipmap_pos_new)) { continue; }

                // Write brick to result buffer
                g_data[0].xyz = floatBitsToUint(world_pos_new);
                g_data[0].w = uint(clipmap_pos_new.w) | (1u << 8u);
                g_data[1][0] = 0xffu | packed0;  // Flag brick for rendering as single splat
                emitBrick();
            }
        }
    }
}
