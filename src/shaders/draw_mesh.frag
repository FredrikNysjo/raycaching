#version 450

#define MAX_SHADOW_STEPS 100
#define MAX_AO_STEPS 20
#define NUM_AO_RAYS 4
#define SHOW_SHADOWS_DEBUG 0
#define SHOW_AO_DEBUG 0
#define COMPUTE_SHADOWS_AND_AO 1

struct Ray { vec3 origin; vec3 dir; };

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 2, std140) uniform RaycastUniforms {
    mat4 projection[2];
    mat4 view[2];
    mat4 view_inv[2];
    int ao_enabled;
    int shadows_enabled;
    int max_steps;
    int raycasting_enabled;
    int num_importance_rays;
    float step_length;
} u_raycast;

layout(binding = 3, std140) uniform VolumeUniforms {
    mat4 image_to_world;
    mat4 world_to_image;
    vec4 spacing;
    vec4 clip[2];
    float threshold;
} u_volume;

layout(location = 0) uniform int u_eye_idx = 0;
layout(binding = 1) uniform sampler3D u_volume_texture;
layout(binding = 2) uniform samplerBuffer u_halton_texture;

layout(location = 0) in vec3 v_view_pos;
layout(location = 0) out vec4 rt_color;

float imageLinear(sampler3D sampler, vec3 p)
{
    float value = textureLod(sampler, p, 0.0).r;
    return value;
}

vec4 imageRaycast(Ray ray, float step_length, int max_steps, float threshold)
{
    vec3 p = ray.origin;
    float value = imageLinear(u_volume_texture, p);
    for (int i = 0; i < max_steps; ++i) {
        value = imageLinear(u_volume_texture, p);
        if (value >= threshold) { break; }
        p += (step_length * ray.dir);
    }
    return vec4(p, value);
}

float imageVisibility(vec3 p, vec3 light_dir, float step_length, int max_steps, float threshold)
{
    float rnd = fract(dot(gl_FragCoord.xy, vec2(sqrt(2.0), sqrt(3.0))));

    Ray ray = Ray(p, light_dir);
    ray.origin += ray.dir * (1.0 + rnd) * step_length;
    float threshold_new = max(imageLinear(u_volume_texture, p), threshold);
    vec4 t = imageRaycast(ray, step_length, max_steps, threshold_new);
    return length(t.xyz - ray.origin) / (step_length * max_steps);
}

float diffuse_wrap(vec3 N, vec3 L, float wrap)
{
    return max(0.0, (dot(N, L) + wrap) / (1.0 + wrap));
}

float specular(vec3 N, vec3 L, vec3 V, float specular_power)
{
    vec3 H = normalize(L + V);
    float normalization = (8.0 + specular_power) / 8.0;
    return normalization * pow(max(0.0, dot(N, H)), specular_power);
}

vec3 hemisphere(vec3 N, vec3 up, vec3 ground, vec3 sky)
{
    return mix(ground, sky, dot(N, up) * 0.5 + 0.5);
}

vec3 computeLighting(vec3 N, vec3 V, vec3 L, float shadow, float ao)
{
    vec3 up = vec3(0.0, 1.0, 0.0);
    float specular_power = exp2(10.0 * 0.4);

    vec3 color = vec3(0.0);
    color.rgb += 0.3 * vec3(1.0) * diffuse_wrap(N, L, 0.0) * shadow;
    color.rgb += 0.3 * vec3(0.05) * specular(N, L, V, specular_power) * shadow;
    color.rgb += 0.75 * vec3(1.0) * hemisphere(N, up, vec3(0.0), vec3(1.0)) * ao;
    color.rgb = bool(SHOW_SHADOWS_DEBUG) ? vec3(shadow) : color.rgb;
    color.rgb = bool(SHOW_AO_DEBUG) ? vec3(ao) : color.rgb;
    return color;
}

void main()
{
    // Compute view normal from fragment group
    vec3 T = normalize(dFdx(v_view_pos));
    vec3 B = normalize(dFdy(v_view_pos));
    vec3 N = normalize(cross(T, B));

    vec4 color = vec4(0.0);
    {
        vec3 V = -normalize(v_view_pos);
        vec3 L = normalize(vec3(1.0, 2.0, 3.0));

        mat4 image_from_view = u_volume.world_to_image * u_raycast.view_inv[u_eye_idx];
        float step_length = u_raycast.step_length;
        float threshold = u_volume.threshold;
        float jitter = fract(dot(gl_FragCoord.xy, vec2(sqrt(2.0), sqrt(3.0))));

        float shadow = 1.0, ao = 1.0;
#if COMPUTE_SHADOWS_AND_AO
        if (bool(u_raycast.shadows_enabled)) {
            vec3 image_pos = vec3(image_from_view * vec4(v_view_pos, 1.0));
            vec3 image_light_dir = normalize(mat3(image_from_view) * L);
            shadow = imageVisibility(image_pos, image_light_dir, step_length, MAX_SHADOW_STEPS, threshold);
        }
        if (bool(u_raycast.ao_enabled)) {
            ao = 0.0;
            for (int i = 0; i < NUM_AO_RAYS; ++i) {
                vec3 rnd = fract(texelFetch(u_halton_texture, i).rgb + jitter);
                vec3 image_pos = vec3(image_from_view * vec4(v_view_pos, 1.0));
                vec3 image_normal = normalize(mat3(image_from_view) * N);
                vec3 image_ray_dir = normalize(image_normal + normalize(rnd - 0.5));
                ao += imageVisibility(image_pos, image_ray_dir, step_length, MAX_AO_STEPS, threshold);
            }
            ao /= float(NUM_AO_RAYS);
        }
#endif // COMPUTE_SHADOWS_AND_AO
        color.rgb = computeLighting(N, V, L, shadow, ao);
        color.a = 1.0;
    }

    rt_color = color;
}
