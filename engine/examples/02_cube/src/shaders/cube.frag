#version 330
#extension GL_ARB_shading_language_420pack : require

in vec3 v_color;

layout(location = 0) out vec4 frag_color;

//layout(binding = 1, std140) uniform SceneUniforms {
//    mat4 u_view;
//    mat4 u_projection;
//    float u_time;
//};

//layout(binding = 3, std140) uniform DrawableUniforms {
//    mat4 u_mvp;
//    mat4 u_mv;
//    mat4 u_model;
//};

void main()
{
    frag_color = vec4(v_color, 1.0);
}
