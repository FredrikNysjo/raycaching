/// @file    cgVr.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#ifndef CG_WITH_OVR_SDK

#include "cgVr.h"
#include "cgTypes.h"
#include "cgLogging.h"
#include "cgRenderer.h"

namespace cg {

CgStatus cgHmdInit(CgHmd *hmd)
{
    vr::HmdError hmd_error;
    hmd->hmd = vr::VR_Init(&hmd_error, vr::VRApplication_Scene);
    if (!hmd->hmd) {
        cgLogError("%s\n", vr::VR_GetVRInitErrorAsEnglishDescription(hmd_error));
        return CgStatus::Failure;
    }
    return CgStatus::Success;
}

CgStatus cgHmdShutdown(CgHmd *hmd)
{
	vr::VR_Shutdown();
    return CgStatus::Success;
}

CgStatus cgHmdConfigureRendering(CgHmd *hmd)
{
	hmd->hmd->GetRecommendedRenderTargetSize(&hmd->renderTargetSize[0], &hmd->renderTargetSize[1]);
	hmd->renderTargetSize[0] *= 2;  // Multiply width to obtain size for left + right eye 
    cgLogInfo("cgHmdConfigureRendering: HMD framebuffer size: %d %d\n", hmd->renderTargetSize[0], hmd->renderTargetSize[1]);

    if (!vr::VRCompositor()) {
        cgLogError("cgHmdConfigureRendering: Could not initialize VR compositor\n");
        return CgStatus::Failure;
    }
    return CgStatus::Success;
}

CgStatus cgHmdCreateRenderTarget(CgHmd *hmd)
{
    // Save OpenGL state
    GLint prevDrawFramebufferBinding;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &prevDrawFramebufferBinding);
    GLint prevTextureBinding2D;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTextureBinding2D);

    // Create GL texture for main render target (RGBA texture)
    GLuint renderTarget;
    glGenTextures(1, &renderTarget);
    glBindTexture(GL_TEXTURE_2D, renderTarget);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, hmd->renderTargetSize[0],
                 hmd->renderTargetSize[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    // Create GL depth + stencil texture for HMD. Only needed when we want to
    // draw something that needs depth testing directly to the HMD framebuffer)
    GLuint depthTexture;
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, hmd->renderTargetSize[0],
                 hmd->renderTargetSize[1], 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);

    // Create GL framebuffer for HMD
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, renderTarget, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                           GL_TEXTURE_2D, depthTexture, 0);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        cgLogError("cgHmdCreateRenderTarget: Incomplete framebuffer\n");
        return CgStatus::Failure;
    }

    // Store additional information
    hmd->renderTarget = renderTarget;
    hmd->depthTexture = depthTexture;
    hmd->framebuffer = framebuffer;
    hmd->viewport[0] = glm::vec4(0, 0, hmd->renderTargetSize[0] / 2, hmd->renderTargetSize[1]);
    hmd->viewport[1] = glm::vec4(hmd->renderTargetSize[0] / 2, 0, hmd->renderTargetSize[0] / 2, hmd->renderTargetSize[1]);

    // Restore OpenGL state
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, prevDrawFramebufferBinding);
    glBindTexture(GL_TEXTURE_2D, prevTextureBinding2D);

    return CgStatus::Success;
}

void cgHmdBeginFrame(CgHmd *hmd)
{
	// Begin HMD frame
	vr::VRCompositor()->WaitGetPoses(&hmd->poses[0], vr::k_unMaxTrackedDeviceCount, nullptr, 0);

	// Obtain pose matrices of tracked devices
    for (unsigned di = 0; di < vr::k_unMaxTrackedDeviceCount; ++di) {
        vr::HmdMatrix34_t pose = hmd->poses[di].mDeviceToAbsoluteTracking; 
        // Convert OpenVR row matrix to OpenGL column matrix
        hmd->poseMatrix[di] = glm::mat4();
	    for (unsigned i = 0; i < 3; ++i) {
	    	for (unsigned j = 0; j < 4; ++j) {
	    		hmd->poseMatrix[di][j][i] = pose.m[i][j];
	    	}
	    }
    }

	// Obtain projection matrices for left and right eye
	vr::HmdMatrix44_t projectionMatrix[2];
	projectionMatrix[0] = hmd->hmd->GetProjectionMatrix(vr::Eye_Left, 0.1f, 10.0f, vr::API_OpenGL);
    projectionMatrix[1] = hmd->hmd->GetProjectionMatrix(vr::Eye_Right, 0.1f, 10.0f, vr::API_OpenGL);
	hmd->projectionMatrix[0] = glm::transpose(*(glm::mat4 *)(&projectionMatrix[0].m[0][0]));
	hmd->projectionMatrix[1] = glm::transpose(*(glm::mat4 *)(&projectionMatrix[1].m[0][0]));

    // Compute view matrices for left and right eye
	vr::HmdMatrix34_t matEyeLeft = hmd->hmd->GetEyeToHeadTransform(vr::Eye_Left);
	vr::HmdMatrix34_t matEyeRight = hmd->hmd->GetEyeToHeadTransform(vr::Eye_Right);
	hmd->viewMatrix[0] = glm::translate(glm::mat4(), glm::vec3(0.0315f, 0.0f, 0.0f)) *
                         glm::inverse(hmd->poseMatrix[vr::k_unTrackedDeviceIndex_Hmd]);
	hmd->viewMatrix[1] = glm::translate(glm::mat4(), glm::vec3(-0.0315f, 0.0f, 0.0f)) *
                         glm::inverse(hmd->poseMatrix[vr::k_unTrackedDeviceIndex_Hmd]);
}

void cgHmdEndFrame(CgHmd *hmd)
{
	// Create descriptors for render targets
	vr::Texture_t eye_texture[2];
	vr::VRTextureBounds_t eye_bounds[2];
	eye_texture[0] = { (void *)(std::uintptr_t)hmd->renderTarget, vr::API_OpenGL, vr::ColorSpace_Gamma };
	eye_bounds[0] = { 0.0f, 0.0f, 0.5f, 1.0f };
	eye_texture[1] = { (void *)(std::uintptr_t)hmd->renderTarget, vr::API_OpenGL, vr::ColorSpace_Gamma };
	eye_bounds[1] = { 0.5f, 0.0f, 1.0f, 1.0f };

	// End HMD frame (present to display)
	vr::VRCompositor()->Submit(vr::Eye_Left, &eye_texture[0], &eye_bounds[0]);
	vr::VRCompositor()->Submit(vr::Eye_Right, &eye_texture[1], &eye_bounds[1]);

	vr::VRCompositor()->PostPresentHandoff();
}

void cgHmdProcessEvents(CgHmd *hmd, void *userdata)
{
    vr::VREvent_t event;
    while (hmd->hmd->PollNextEvent(&event, sizeof(event))) {
        // Handle event directly or call user-defined callback
        switch (event.eventType) {
        case vr::VREvent_ButtonPress:  // Fall through
        case vr::VREvent_ButtonUnpress:
            if (hmd->buttonCallback) { hmd->buttonCallback(hmd, &event, userdata); }
            break;
        default:
            break;
        }
    }
}

void cgHmdLoadRenderModel(CgHmd *hmd, CgHmdRenderModel *dstRenderModel, const int index)
{
    // Obtain render model for device index
    char str[80] = { 0 };
    hmd->hmd->GetStringTrackedDeviceProperty(index, vr::Prop_RenderModelName_String, str, 80, nullptr);
    vr::RenderModel_t *srcRenderModel = nullptr;
    while (vr::VRRenderModels()->LoadRenderModel_Async(str, &srcRenderModel) == vr::VRRenderModelError_Loading) {}

    // Obtain render model texture map
    vr::RenderModel_TextureMap_t *srcRenderModelTexture = nullptr;
    while (vr::VRRenderModels()->LoadTexture_Async(srcRenderModel->diffuseTextureId, &srcRenderModelTexture) == vr::VRRenderModelError_Loading) {}

    // Save OpenGL state
    GLint prevVertexArrayBinding;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &prevVertexArrayBinding);
    GLint prevTextureBinding2D;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTextureBinding2D);

    // Create VAO for drawing model mesh
    glGenVertexArrays(1, &dstRenderModel->vao);
    glBindVertexArray(dstRenderModel->vao);

    // Create GL buffer for model mesh vertices (stored in interleaved format)
    glGenBuffers(1, &dstRenderModel->vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, dstRenderModel->vertexBuffer);
	size_t verticesNBytes = srcRenderModel->unVertexCount * sizeof(srcRenderModel->rVertexData[0]);
    size_t vertexStride = sizeof(srcRenderModel->rVertexData[0]);
	glBufferData(GL_ARRAY_BUFFER, verticesNBytes, srcRenderModel->rVertexData, GL_STATIC_DRAW);
    glEnableVertexAttribArray(CG_POSITION);
    glVertexAttribPointer(CG_POSITION, 3, GL_FLOAT, GL_FALSE, vertexStride, 0);
    glEnableVertexAttribArray(CG_NORMAL);
    glVertexAttribPointer(CG_NORMAL, 3, GL_FLOAT, GL_FALSE, vertexStride, (void*)12);
    glEnableVertexAttribArray(CG_TEXCOORD0);
    glVertexAttribPointer(CG_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, vertexStride, (void*)24);

    // Create GL buffer for model mesh indices
    glGenBuffers(1, &dstRenderModel->indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dstRenderModel->indexBuffer);
	size_t indicesNBytes = (srcRenderModel->unTriangleCount * 3) * sizeof(srcRenderModel->rIndexData[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesNBytes, srcRenderModel->rIndexData, GL_STATIC_DRAW);

    // Create GL texture for model texture map
    glGenTextures(1, &dstRenderModel->texture);
    glBindTexture(GL_TEXTURE_2D, dstRenderModel->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
                 srcRenderModelTexture->unWidth,
                 srcRenderModelTexture->unHeight,
                 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 srcRenderModelTexture->rubTextureMapData);

    // Store additional information
    dstRenderModel->numVertices = srcRenderModel->unVertexCount;
    dstRenderModel->numIndices = srcRenderModel->unTriangleCount * 3;

    // Restore OpenGL state
    glBindVertexArray(prevVertexArrayBinding);
    glBindTexture(GL_TEXTURE_2D, prevTextureBinding2D);

    // Delete render model data
    vr::VRRenderModels()->FreeTexture(srcRenderModelTexture);
    vr::VRRenderModels()->FreeRenderModel(srcRenderModel);
}

} // namespace cg

#endif // CG_WITH_OVR_SDK
