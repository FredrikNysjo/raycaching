/// @file    cgRenderQueue.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2016 Fredrik Nysjö
/// 
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include "cgTypes.h"
#include "cgRenderer.h"

namespace cg {

/// Struct for render queue for submitting drawables for rendering
///
/// @remark Singleton
struct CgRenderQueue {
    CgVector<CgDrawable>::type drawables;
    CgArray<CgVector<CgDrawable>::type, 64>::type drawLists;

    /// Return CgRenderQueue instance
    ///
    /// @remark Thread-safe
    static CgRenderQueue *instance();
};

/// Submit drawable to render queue. A shallow copy of the drawable will be
/// stored on the queue.
///
/// @remark Thread-safe
void cgRenderQueueSubmitDrawable(CgRenderQueue *renderQueue,
                                 const CgDrawable &drawable);

/// Generate draw lists. A shallow copy of each drawable will be stored in each
/// draw list that corresponds to a 1 in the drawable's render mask.
///
/// @remark Thread-safe
void cgRenderQueueGenerateDrawLists(CgRenderQueue *renderQueue);

/// Get draw list for index corresponding to render pass
void cgRenderQueueGetDrawList(const CgRenderQueue *renderQueue,
                              const std::uint32_t renderPassIndex,
                              const CgDrawable **drawables,
                              size_t *drawablesCount);

/// Clear render queue drawables
///
/// @remark Thread-safe
void cgRenderQueueClear(CgRenderQueue *renderQueue);

} // namespace cg
