/// @file    raycaching.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2017 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "raycaching.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

void createClipmapBuffer(GLuint *buffer, GLuint *texture, const glm::ivec4 &dimensions)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = (dimensions.x * dimensions.y * dimensions.z * dimensions.w) * sizeof(std::uint32_t) * 2;
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_R32UI, *buffer);
}

void createBrickCache(GLuint *buffer, GLuint *texture, const BrickCacheInfo &buffer_info)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = sizeof(BrickCacheInfo) + buffer_info.capacity * (sizeof(Brick) + sizeof(glm::uvec4));
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glNamedBufferSubData(*buffer, 0, sizeof(BrickCacheInfo), &buffer_info);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RGBA32UI, *buffer);
}

void createIndexBuffer(GLuint *buffer, GLuint *texture, const BrickCacheInfo &buffer_info)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = 8 * buffer_info.capacity * sizeof(std::int32_t);
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32I, GL_RED_INTEGER, GL_INT, nullptr);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_R32I, *buffer);
}

void createResultBuffer(GLuint *buffer, const BrickCacheInfo &buffer_info)
{
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = 4 * 4 + buffer_info.capacity * sizeof(Brick);
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
}

void createTimestampBuffer(GLuint *buffer, const BrickCacheInfo &buffer_info)
{
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = 64 * 64 * 64 * 8 * sizeof(std::int32_t);  // (TODO)
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
}

void createTileBuffer(GLuint *buffer, const BrickCacheInfo &buffer_info)
{
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = 128 * 128 * 2 * sizeof(std::int32_t);
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
}

void createHaltonTexture(GLuint *buffer, GLuint *texture)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for Issue #1
    std::size_t num_bytes = 4096 * sizeof(glm::vec3);
    glNamedBufferData(*buffer, num_bytes, (void *)&halton3d_235[0], GL_STATIC_DRAW);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RGB32F, *buffer);
}

glm::mat4 predictHeadPose(const glm::mat4 &prev, const glm::mat4 &current, float num_frames)
{
    glm::vec3 p0 = glm::vec3(prev[3]);
    glm::vec3 p1 = glm::vec3(current[3]);
    glm::vec3 pos_velocity = p1 - p0;

    glm::quat q0 = glm::quat_cast(prev);
    glm::quat q1 = glm::quat_cast(current);
    if (glm::dot(q0, q1) < 0.0f) { q0 = -q0; }  // Do sign flip
    glm::quat q_diff = q1 * glm::conjugate(q0);

    glm::vec3 q_axis = glm::vec3(q_diff.x, q_diff.y, q_diff.z);
    float q_mag = glm::length(q_axis);
    float q_angle = 2.0f * glm::atan(q_mag, q_diff.w);
    q_axis = (q_mag > 1e-5f) ? (q_axis / q_mag) : glm::vec3(1.0f, 0.0f, 0.0f);

    // Compute predicted head pose
    glm::mat4 m;
    m = glm::mat4_cast(glm::angleAxis(q_angle * num_frames, q_axis) * q1);
    m[3] = glm::vec4(p1 + pos_velocity * num_frames, 1.0f);

    return m;
}
